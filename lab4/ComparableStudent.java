


public class ComparableStudent implements Comparable<ComparableStudent> {
	
	private String name;		// Student name
	private double prereqsMet;	// fraction of prerequisites met
	
	/**
	 * No-parameter constructor
	 */
	public ComparableStudent() {
		super();
		this.name = "Jane";
		this.prereqsMet = 0;
	}

	
	/**
	 * @param name
	 * @param meetsPrerequs
	 */
	public ComparableStudent(String name, double prereqsMet) {
		super();
		this.name = name;
		this.prereqsMet = prereqsMet;
	}
	
	/**
	 * @param name
	 * @param meetsPrerequs
	 */
	public ComparableStudent(double prereqsMet, String name) {
		this(name, prereqsMet);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the meetsPrerequs
	 */
	public double getPrereqsMet() {
		return prereqsMet;
	}
	/**
	 * @param meetsPrerequs the meetsPrerequs to set
	 */
	public void setPrereqsMet(double prereqsMet) {
		this.prereqsMet = prereqsMet;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", prereqsMet=" + prereqsMet + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComparableStudent other = (ComparableStudent) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	@Override
	public int compareTo(ComparableStudent o) {
		return this.getName().compareTo(o.getName());
	}
	
	
	
	
	
	

}
