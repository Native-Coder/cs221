
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import lab4.generic.ArraySortedList;
import lab4.generic.SortedListInterface;

public class ArraySortedListDriver {

	private static SortedListInterface<ComparableStudent> studentList = new ArraySortedList<ComparableStudent>(3);
	private static SortedListInterface<ComparableCarEvent> carEventList = new ArraySortedList<ComparableCarEvent>(3);
	private static SortedListInterface<ComparableCircle> circleList = new ArraySortedList<ComparableCircle>(3);

	private static SortedListInterface list;

	private static final String createNewList = "createNewList";
	private static final String insert = "insert";
	private static final String printList = "printList";
	private static final String delete = "delete";
	private static final String search = "search";
	private static final String size = "size";

	private static final String quit = "quit";

	private static String choiceString(String choice) {
		switch (choice) {
		case insert:
			return "\n* Testing add an element to the list";
		case delete:
			return "\n* Testing delete an element from the list";
		case search:
			return "\n* Testing search for an element in the list ";
		case size:
			return "\n* ListInfo: Display the size of the list ";
		case createNewList:
			return "\n* Setup for next testing phase: Create a new list";
		case printList:
			return "\n* ListInfo: Print the info in the list";
		case quit:
			return "quit";
		default:
			return "nothing";
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		// read data from re-directed file
		// Scanner inData = new Scanner(System.in);

		Scanner inData = new Scanner(new File("testSortedListMixedStudents.in"));
//		Scanner inData = new Scanner(new File("testSortedListMixedCarEvents.in"));
//		Scanner inData = new Scanner(new File("testSortedListMixedCircles.in"));
//		Scanner inData = new Scanner(new File("testListinsert.in"));
//		Scanner inData = new Scanner(new File("testListinsertTooMany.in"));
//		Scanner inData = new Scanner(new File("testListSearchFullList.in"));
//		Scanner inData = new Scanner(new File("testListSearchNotFullList.in"));
//		Scanner inData = new Scanner(new File("testListSearchEmptyList.in"));
//		Scanner inData = new Scanner(new File("testListDeleteNotFullList.in"));
//		Scanner inData = new Scanner(new File("testListDeleteFullList.in"));
//		Scanner inData = new Scanner(new File("testListDeleteEmptyList.in"));
//		while (inData.hasNext()) {
//			String line= inData.nextLine();
//			System.out.println("line = "+ line);
//		}
		// Scanner inData = new Scanner(new File("test5.in"));
		// Scanner inData = new Scanner(new File("test4.in"));
		// Scanner inData = new Scanner(new File("test3.in"));
		// Scanner inData = new Scanner(new File("test2.in"));
		// Scanner inData = new Scanner(new File("test1.in"));

		String type = setListToUse(inData);

		String choice;
		do {
			choice = getNextChoice(inData);
			String info = choiceString(choice);
			if (!info.equals(""))
				System.out.println(info);
			if (!choice.equals(quit))
				executeChoice(choice, inData, type);
		} while (!choice.equals(quit));

	}

	private static String setListToUse(Scanner inData) {
		String type = inData.nextLine().trim();
		switch (type) {
		case "circle":
			list = circleList;
			break;
		case "carevent":
			list = carEventList;
			break;
		case "student":
			list = studentList;
			break;
		default:
			list = null;
		}
		return type;
	}

	// ask the user for what s/he wants to do
	private static String getNextChoice(Scanner inData) {
		String choice = null;
		choice = inData.next();
		inData.nextLine();
		return choice;
	}

	// Determine what the user wants to do and execute the appropriate method
	static void executeChoice(String choice, Scanner inData, String type) {
		switch (choice) {
		case insert:
			executeInsert(inData, type);
			break;
		case delete:
			executeDelete(inData, type);
			break;
		case createNewList:
			executeCreate(inData, type);
			break;
		case search:
			executeSearch(inData, type);
			break;
		case size:
			executeSize();
			break;
		case printList:
			executePrint();
			break;

		}
		// executePrint(testA, size);
	}

	// perform the necessary update to complete the insert
	static void executeInsert(Scanner inData, String type) {
		// System.out.print("Enter the student info (fraction, name): ");
		Object temp =makeObjectFromScanner(inData, type);
		boolean res = list.insert(temp);
		String notStr = (res) ? "" : "not";
		System.out.println("Element " + temp + " was " + notStr + " inserted.");
	}

	// perform the necessary updates to perform the delete
	static void executeDelete(Scanner inData, String type) {
		// read student info from scanner and search for it in list
		Object target = makeObjectFromScanner(inData, type);
		int index = list.search(target);

		boolean res = list.delete(target);
		String notStr = (res) ? "" : "not";

		System.out.println("Element " + target + " was " + notStr + " deleted from index " + index + ".");
	}

	// perform the necessary updates to perform the delete
	static void executeSearch(Scanner inData, String type) {
		// System.out.print("Enter info of student to be deleted: ");
		Object target = makeObjectFromScanner(inData, type);

		int index = list.search(target);
		System.out.println("Search for " + target + " returns " + index);
	}
	
	private static Object makeObjectFromScanner(Scanner inData, String type) {
		switch (type) {
		case "circle":
			return makeCircleFromScanner(inData);
		case "carevent":
			return makeCarEventFromScanner(inData);
		case "student":
			return makeStudentFromScanner(inData);
		default:
			return  null;
		}
	}

	

	private static ComparableStudent makeStudentFromScanner(Scanner inData) {
		inData.nextDouble();
		String name = inData.nextLine().trim();
		ComparableStudent target = new ComparableStudent(name, 0);
		return target;
	}
	private static ComparableCircle makeCircleFromScanner(Scanner inData) {
		double radius = inData.nextDouble();
		double xCenter = inData.nextDouble();
		double yCenter = inData.nextDouble();
		String label = inData.nextLine().trim();

		ComparableCircle target = new ComparableCircle(radius, xCenter, yCenter, label);
		return target;
	}
	
	private static ComparableCarEvent makeCarEventFromScanner(Scanner inData) {
		int arrivalTime = inData.nextInt();
		int serviceTime = inData.nextInt();
		int finishTime = inData.nextInt();
		inData.hasNextLine();
		ComparableCarEvent target = new ComparableCarEvent(arrivalTime, serviceTime);
		target.setFinishTime(finishTime);
		return target;
	}


	// perform the necessary updates to perform the get operation
	static void executeSize() {
		// System.out.print("Enter subscript to be deleted: ");
		System.out.println("The list has " + list.size() + " elements.");
	}

	// perform the necessary updates to perform the get operation
	static void executeGet(Scanner inData) {
		// System.out.print("Enter subscript to be deleted: ");
		int index = inData.nextInt();
		inData.nextLine();
		System.out.println("get elem at index " + index + ": " + list.get(index));
	}

	// create a new rectangle array
	static void executeCreate(Scanner inData, String type) {
		// how much elements should the array hold?
		int capacity = inData.nextInt();
		inData.nextLine();
		switch (type) {
		case "circle":
			circleList = new ArraySortedList<ComparableCircle>(capacity);
			list = circleList;
			break;
		case "carevent":
			carEventList = new ArraySortedList<ComparableCarEvent>(capacity);
			list = carEventList;
			break;
		case "student":
			studentList = new ArraySortedList<ComparableStudent>(capacity);
			list = studentList;
			break;
		default:
			list = null;
		}

		System.out.println("Created list with " + capacity + " slots. \n ");

	}

	/**
	 * Prints the info about the list: first a message with the number of elements
	 * and then each element 
	 * @param list  The list to be printed
	 */
	public static void printList(SortedListInterface list) {
		System.out.println("The list has " + list.size() + " elements.");
		for (int i=0; i<list.size(); i++) {
			System.out.println(i+": " + list.get(i));
		}
		
	}
	

	// prints the list
	static void executePrint() {
		printList(list);
	}
	

}
