

import java.util.Arrays;


// A class to demonstrate how parameters are passed in Java
public class ComparableCircle implements Comparable<ComparableCircle> {
	
	private double radius;
	private double [] center = {0,0};
	private String label;
	private  int identifier;
	private static int counter = 1;
	
	// constructors   - function OVERLOADING - one name, several DIFFERENT signatures
	/**
	 * no-argument constructor - creates a circle of radius 1 centered at (0,0)
	 */
	public ComparableCircle () {
		this(1, 0, 0, "none"); // calls the appropriate constructor with 3 parameters
	}
	
	/**
	 *  Creates an object of type circle
	 *  @param radius  The radius of the circle. The center defaults to  (0,0)
	 */
	public ComparableCircle (double radius) {
		this(radius, 0, 0, "none"); // calls the appropriate constructor with 3 parameters
	}

	/**
	 *  Creates an object of type circle
	 *  @param radius  The radius of the circle. 
	 *  @param xCenter The x-coordinate of the center
	 *  @param yCenter The y-coordinate of the center
	 */
	public ComparableCircle(double radius, double xCenter, double yCenter, String label) {
//		super();
		this.radius = radius;
		this.center[0] = xCenter;
		this.center[1] = yCenter;
		this.label = label;
		identifier = counter++;


	}
	
	/**
	 *  Creates an object of type circle
	 *  @param radius  The radius of the circle. 
	 *  @param center The 2D coordinates of the center of the circle
	 */	public ComparableCircle(double radius, double[] center, String label) {
		this ( radius, center[0], center[1], label);
	}


	// transformer and observer methods for all instance variables


	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", center=" + Arrays.toString(center) + ", label=" + label + ", identifier="
				+ identifier + "]";
	}

	/**
	 * Determines whether this object is equal to a given object.
	 * @param obj The given object which may be equal (or not)
	 * @return True if obj is equal to this; false otherwise
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComparableCircle other = (ComparableCircle) obj;
		if (!Arrays.equals(center, other.center))
			return false;
		if (Double.doubleToLongBits(radius) != Double.doubleToLongBits(other.radius))
			return false;
		return true;
	}

	@Override
	public int compareTo(ComparableCircle o) {
	    if (Math.sqrt(Math.pow(center[0], 2)+ Math.pow(center[1], 2)) 
	    			< Math.sqrt(Math.pow(o.center[0], 2)+ Math.pow(o.center[1], 2)))
		return -1;
	    if (Math.sqrt(Math.pow(center[0], 2)+ Math.pow(center[1], 2)) 
    			> Math.sqrt(Math.pow(o.center[0], 2)+ Math.pow(o.center[1], 2)))
	    return 1;
	    return 0;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius to set
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * @return the center
	 */
	public double[] getCenter() {
		return center;
	}

	/**
	 * @param center the center to set
	 */
	public void setCenter(double[] center) {
		this.center = center;
	}
	
	

	/**
	 * @return the iD
	 */
	public int getID() {
		return identifier;
	}

	
	

}
