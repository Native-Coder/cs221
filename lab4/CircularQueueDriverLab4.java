

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


import lab4.circqueue.CircularQueue;


public class CircularQueueDriverLab4 {
	
	private final static String quit = "quit";
	private static final String noParamConstructor = "noParamConstructor";
	private static final String size = "size";
	private static final String enqueue = "enqueue";
	private static final String dequeue = "dequeue";
	private static final String isFull = "isFull";
	private static final String isEmpty = "isEmpty";
	private static final String nonStaticTest = "nonStaticTest";
	
	private static CircularQueue<Circle> [] testQueueArray = null;
	
	private static CircularQueue<Circle> testQueue;  
	
	private static String countsStr = "";

	private static String choiceString(String choice) {
		switch (choice) {
		case noParamConstructor:
			return "\n* Testing noParamConstructor";
		case enqueue:
			return "\n* Testing enqueue operation";
		case dequeue:
			return "\n* Testing dequeue operation";
		case isFull:
			return "\n* Testing isFull";
		case isEmpty:
			return "\n* Testing isEmpty";
		case size:
			return "\n* Testing size";
		case quit:
			return "quit";
		case nonStaticTest:
			return "\n* Testing that methods (and linked list) is not static";
		default:
			return "nothing";
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		// read data from re-directed file
		// Scanner inData = new Scanner(System.in);

//	    Scanner inData = new Scanner (new File ("cqLab3test1.in"));
//	    Scanner inData = new Scanner (new File ("cqLab3test2.in"));
//	    Scanner inData = new Scanner (new File ("cqLab3test3.in"));
	    Scanner inData = new Scanner (new File ("cqLab3test4.in"));
//	    Scanner inData = new Scanner (new File ("cqLab3test5.in"));

	    String name = inData.nextLine();
	    System.out.println("Test: " + name);

		String choice;
		do {
			choice = promptUserForChoice(inData);
			System.out.println(choiceString(choice));
			if (!choice.equals("quit")) {
				executeChoice(choice, inData);
				//driverPrint();
			}
		} while (!choice.equals("quit"));
		System.out.println("Count str = " + countsStr);
	}

	// ask the user for what s/he wants to do
	static String promptUserForChoice(Scanner inData) {
		String choice = null;
		// while (1 > choice || choice > 5) {
		// System.out.println("Select your choice:");
		// System.out.println(" 1 -- Load the array from a file");
		// System.out.println(" 2 -- Insert an element");
		// System.out.println(" 3 -- Delete an element");
		// System.out.println(" 4 -- Is the array full?");
		// System.out.println(" 5-- Quit");
		choice = inData.nextLine();
		//inData.nextLine();
		// }
		return choice;
	}

	// Determine what the user wants to do and execute the appropriate method
	static void  executeChoice(String choice, Scanner inData) {
		switch (choice) {
		case "noParamConstructor":
			testQueue = new CircularQueue<Circle>();
			break;
		case "enqueue":
			executeInsert(inData);
			break;
		case "dequeue":
			executeDelete(inData);
			break;
		case "nonStaticTest":
			executeNonStatic(inData);
			break;
		case "isFull":
			System.out.println("The queue is full: " + testQueue.isFull());
			break;
		case "isEmpty":
			System.out.println("The queue is empty: " + testQueue.isEmpty());
			break;
		case "size":
			System.out.println("The size of the queue is  " + testQueue.size());
			break;

		}
		// executePrint(testA, size);
	}

	// perform the necessary update to complete the insert
	static void executeInsert(Scanner inData) {
		// System.out.print("Enter the circle info (x, y, width, height): ");
		Circle temp = new Circle(inData.nextDouble(),  inData.nextDouble(), 
				inData.nextDouble(), inData.nextLine().trim());
		System.out.println(temp);
		testQueue.enqueue(temp);
		countsStr = countsStr + temp.getID() + " ";
	}

	// perform the necessary updates to perform the delete
	static void executeDelete(Scanner inData) {
		// System.out.print("Enter subscript to be deleted: ");
		Circle dequeued = testQueue.dequeue();
		System.out.println("Element dequeued " + dequeued);
		countsStr = countsStr + dequeued.getID() + " ";
	}


//	// using extension to print the results
//	static void driverPrint() {
//		if (testQueue == null)
//			return;
//		System.out.println("DriverPrint: Size of queue: " + testQueue.size());
//		int count = 0;
//		Iterator<Circle> itr = testQueue.iterator();
//		while (itr.hasNext() && count < 10) {
//			Circle elem = itr.next();
//			System.out.println(count + ": " + elem);
//			count++;
//		}
//		System.out.println("Number of elements printed: (cutoff at 10) " + count);
//	}
	
	// test that two different circular queues can be maintained
	static void executeNonStatic (Scanner inData) {
		
		testQueueArray = new CircularQueue [2];
		testQueueArray[0] = new CircularQueue<Circle>();
		testQueueArray[1] = new CircularQueue<Circle>();
		
		Circle temp = new Circle(inData.nextDouble(),  inData.nextDouble(), 
				inData.nextDouble(),inData.nextLine().trim());
		testQueueArray[0].enqueue(temp);
		temp = new Circle(inData.nextDouble(),  inData.nextDouble(), 
				inData.nextDouble(),inData.nextLine().trim());
		testQueueArray[0].enqueue(temp);
		temp = new Circle(inData.nextDouble(),  inData.nextDouble(), 
				inData.nextDouble(),inData.nextLine().trim());
		testQueueArray[0].enqueue(temp);

		temp = new Circle(inData.nextDouble(),  inData.nextDouble(), 
				inData.nextDouble(),inData.nextLine().trim());
		testQueueArray[1].enqueue(temp);
		temp = new Circle(inData.nextDouble(),  inData.nextDouble(), 
				inData.nextDouble(),inData.nextLine().trim());
		testQueueArray[1].enqueue(temp);
		temp =new Circle(inData.nextDouble(),  inData.nextDouble(), 
				inData.nextDouble(),inData.nextLine().trim());
		testQueueArray[1].enqueue(temp);
		temp = new Circle(inData.nextDouble(),  inData.nextDouble(), 
				inData.nextDouble(),inData.nextLine().trim());
		testQueueArray[1].enqueue(temp);
		
		temp = testQueueArray[0].dequeue();
		
		System.out.println("First queue:");
		while (!testQueueArray[0].isEmpty())
			System.out.println(testQueueArray[0].dequeue()); 
		
		System.out.println("\nSecond queue: Should be completely different");
		while (!testQueueArray[1].isEmpty())
			System.out.println(testQueueArray[1].dequeue()); 
	}

}
