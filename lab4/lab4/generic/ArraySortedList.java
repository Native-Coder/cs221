package lab4.generic;

public class ArraySortedList<T> implements SortedListInterface<T> {
	
	protected  T [] elemsArray = null;
	protected  int size;
	
//	public static void main (String [] args) {
//		
//		// create the array for the rectangles
//		// createArray(10);
//		
//		// test the methods e.g. 
//		// Rectangle rectngl1 = new Rectangle (....);
//		// insertAtEnd(rectngl1);
//		// printInfo();
//		
//	}
	
	/**
	 * Constructor with specified capacity
	 * @param capacity The size of the array to be created
	 */
	@SuppressWarnings("unchecked")
	public ArraySortedList (int capacity) {
		elemsArray = ( T[] ) new Object[ capacity ];
		size       = 0;
	}

	/**
	 * Add an element at its correct location in the list if it is not already in the list.
	 * if the array is full, the element is not added.
	 * @param The element to be added
	 * @return True if the element was added; false otherwise
	 */
	@SuppressWarnings("unchecked")
	public boolean insert(T element) {
		// is there space and is the element not already in the list?
		if (size != elemsArray.length && search(element) == -1) {
			// insert from the end...
			int index = size-1;
			// copy element at index one position to the right as long as
			// the element there is larger than the element to be inserted
			while (index >= 0 && ((Comparable<T>)elemsArray[index]).compareTo(element)> 0) {
					elemsArray[index+1] = elemsArray[index];
					index--;
			}
			// insert the element at location index +1			
			elemsArray[index+1] = element;
			size++;
			return true;
		}
		return false;
	}
	
	
	/**
	 * 
	 * @return  The number of elements in the list
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Search for the element in the list (using the compareTo function of T). 
	 * @param element
	 * @return The position (start counting at 0) of the element in the list (if found); -1 otherwise; 
	 * Note: Since the list is sorted, a binary search is used 
	 */
	public int search (T element) {
		int first = 0; 
		int last = size-1;
		while (first <= last) {
			// find the index of the element in the middle of the array to be 
			// searched
			int mid = (first+last)/2;
			@SuppressWarnings("unchecked")
			Comparable<T> midElem= (Comparable<T>) elemsArray[mid];
			if (midElem.compareTo(element)< 0) 
				// element is larger than element in middle
				// ==> search top half of array
				first = mid+1;
			else if (midElem.compareTo(element)> 0) 
				// element is smaller than element in middle ==>
				// search bottom half
				last = mid-1;
			else 
				// element must be here 
				return mid;
		}
		// not returned yet? ==> element is not in the list
		
		// not found
		return -1;
		
	}
	
	
	/**
	 * @ returns True if the list is not full; false otherwise
	 */
	public boolean isFull( ) {
		return (size >= elemsArray.length) ;
	}
	
	/**
	 * Gets the  element - 
	 * @param The element to be returned
	 * @return The element in the list; 
	 * null if no element in the list is equal to the given element
	 */
	public T get (T element) {
		int index = search(element);
		if (index != -1)
			return elemsArray[index];
		// not found
		return null;
	}

	/**
	 * Gets the element at the specified position - 
	 * @param position The position of the elem in the list 
	 * (Note: position must be between 0 and size-1 (inclusive))
	 * @return The element at the specified position
	 */
	public T get (int  position) {
		return elemsArray[position];
	}


	/**
	 * Delete the element from the list but keep the list sorted.
	 * @param The element to be deleted
	 * @return true - if the element was deleted; false if the element was not found in the list
	 */
	public boolean delete (T element) {
		int index = search(element);
		if (index == -1)
			return false;
		// move values at index+1 to size-1 to left by one position
		moveThem(index);
		size--;
		return true;
	}
	
	// the element startPos must be replaced by the elem at startPos +1, 
	// which must be replaced by the element at startPos+2, ....
	private void moveThem(int startPos) {
		for (int i=startPos; i< size-1; i++)
			elemsArray[i] = elemsArray[i+1];
	}
}
