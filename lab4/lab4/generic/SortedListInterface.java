package lab4.generic;

// This sorted list does not allow duplicates
public interface SortedListInterface<T> {
	
	/**
	 * 
	 * @return  The number of elements in the list
	 */
	int size();
	
	/**
	 * Search for the element in the list (using the compareTo function of T). 
	 * @param element
	 * @return The position (start counting at 0) of the element in the list (if found); -1 otherwise; 
	 */
	int search (T element);
	
	/**
	 * Add an element at its correct location in the list if it is not already in the list.
	 * if the array is full, the element is not added.
	 * @param The element to be added
	 * @return True if the element was added; false otherwise
	 */
	boolean insert(T element);
	
	
	/**
	 * Delete the element from the list but keep the list sorted.
	 * @param The element to be deleted
	 * @return true - if the element was deleted; false if the element was not found in the list
	 */
	boolean delete (T element) ;
	
	/**
	 * Gets the element - 
	 * @param The element to be returned - with the identifying info included
	 * @return The element in the list with the given identifying information; 
	 * null if no element in the list is equal to the given element
	 */
	T get (T element) ;

	/**
	 * Gets the element at the specified position - 
	 * @param position The position of the elem in the list 
	 * (Note: position must be between 0 and size-1 (inclusive))
	 * @return The element at the specified position
	 */
	T get (int  position) ;

	/**
	 * @ returns True if the list is not full; false otherwise
	 */
	boolean isFull();

}
