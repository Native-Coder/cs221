package lab4.circqueue;
import java.util.NoSuchElementException;

public class CircularQueue<T> implements QueueInterface<T>{
	// REQUIRED: Specify fields here
	Integer size; 
	LinkedNode rear;
	
	// REQUIRED: add a constructor without any parameters
	public CircularQueue(){
		this.size = 0;
		this.rear = null;
	}
	
	/**
	 * @return true if the queue is empty, false otherwise.
	 */
	public boolean isEmpty() {
		return ( rear == null );
	}

	/**
	 * @return the number of elements in the queue.
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Put the element at the rear of the queue if it is not full. Throws
	 * QueueOverflowException if the queue is full.
	 * @param element  The value to be enqueued.
	 */
	public void enqueue( T val ){
		
		this.size++;
		LinkedNode newRear;
		// Edge-case
		if( this.rear == null ) {
			newRear = new LinkedNode( val );
			newRear.setNext( newRear );
		} else {
			newRear = new LinkedNode( val, this.rear.getNext() );
			this.rear.setNext( newRear );
		}
		this.rear = newRear;
	}

	/**
	 * Removes the head element and returns it if the queue is not empty.
	 * 
	 * @return the head if the queue is not empty.
	 * @throws NoSuchElementException when the queue is empty.
	 */
	public T dequeue() {
		if( this.rear == null ) throw new NoSuchElementException();
		LinkedNode next = this.rear.getNext();
		
		if( next.equals( this.rear ) )
			this.rear = null;
		else
			this.rear.setNext( next.getNext() );
		
		this.size--;
		return next.getVal();
	}
	
	// DO NOT MODIFY THE LinkedNode class given here.
	class LinkedNode {		
		// value stored in each element
		private T val;
		// link to the next node in the
		// linked list
		private LinkedNode next;
		
		/**
		 * @param val
		 * @param next
		 */
		public LinkedNode( T val, LinkedNode next ) {
			super();
			this.val  = val;
			this.next = next;
		}
		
		/**
		 * @param val
		 */
		public LinkedNode( T val ) {
			super();
			this.val = val;
		}

		/**
		 * @return the val
		 */
		public T getVal() {
			return val;
		}

		/**
		 * @param val the val to set
		 */
		public void setVal( T val ) {
			this.val = val;
		}

		/**
		 * @return the next
		 */
		public LinkedNode getNext() {
			return next;
		}

		/**
		 * @param next the next to set
		 */
		public void setNext( LinkedNode next ) {
			this.next = next;
		}
	}
}
