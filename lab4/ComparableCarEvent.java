
public class ComparableCarEvent implements Comparable<ComparableCarEvent> {
	protected int arrivalTime;
	protected int serviceTime;
	protected int finishTime;

	public ComparableCarEvent(int arrivalTime, int serviceTime) {
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}

	@Override
	public int compareTo(ComparableCarEvent o) {
		if (this.getArrivalTime() - o.getArrivalTime() > 0)
			return 1;
		if (this.getArrivalTime() - o.getArrivalTime() < 0)
			return -1;

		return 0;
	}

	public void setFinishTime(int time) {
		finishTime = time;
	}

	public int getFinishTime() {
		return finishTime;
	}

	// @Override
	public String toString() {
		return "CarEvent [arrivalTime=" + arrivalTime + ", " + "serviceTime=" + serviceTime + ", " + "finishTime="
				+ finishTime + "]";
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int time) {
		serviceTime = time;
	}

	public int getWaitTime() {
		return (finishTime - arrivalTime - serviceTime);
	}
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComparableCarEvent other = (ComparableCarEvent) obj;
		if (arrivalTime != other.arrivalTime)
			return false;
		if (serviceTime != other.serviceTime)
			return false;
		return true;
	}

}
