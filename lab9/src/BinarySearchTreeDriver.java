
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

import lab9.Member;
import lab9.CreateBalancedBST;
import lab9.bst.BinarySearchTree;
import lab9.bst.BinarySearchTreeExtended;

public class BinarySearchTreeDriver {

	private static final String createNewTree = "createNewTree";
	private static final String add = "add";
	private static final String addSequence = "addSequence";
	private static final String printTree = "printTree";
	private static final String minimum = "minimum";
	private static final String singleChildCount = "singleChildCount";
	private static final String leafCount = "leafCount";
	private static final String isBST = "isBST";
	private static final String preload = "preload";
	private static final String preloadSequence = "preloadSequence";
	private static final String createBalanced = "createBalanced";

	// private static final String printList = "printList";
	// private static final String getPointer = "getPointer";
	// private static final String find = "find";
	// private static final String insert = "insert";
	// private static final String deleteItem = "deleteItem";
	private static final String quit = "quit";

	static Comparator<Integer> compareMembers = new Comparator<Integer>() {
		public int compare(Integer one, Integer two) {
			return one - two;
		}
	};

	private static BinarySearchTreeExtended<Integer, Member> tree; // =
																			// new
																			// BinarySearchTree<Integer,
																			// Member>
																			// (compareMembers);
	private static Integer [] keys;
	private static Member []  elems;
	
	private static String choiceString(String choice) {
		switch (choice) {
		case createNewTree:
			return "\n Create a new tree";
		case add:
			return "\n add elem to tree";
		case preload:
			return "\n Preloading the tree";
		case preloadSequence:
			return "\n Preloading the tree (not in BST manner) ";
		case minimum:
			return "\nTESTING findMinimum";
		case singleChildCount:
			return "\nTESTING singleChildCount";
		case leafCount:
			return "\nTESTING leafcount";
		case isBST:
			return "\nTESTING isBST";
		case createBalanced:
			return "\nTESTING createBalanced";
		case printTree:
			return "\n Printing the tree";
		case addSequence:
			return "\n add elems to tree in non BST order";
		case quit:
			return "quit";
		default:
			return "nothing";
		}
	}

	/**
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException {

		// read data from re-directed file
		 //Scanner inData = new Scanner(System.in);


//		Scanner inData = new Scanner(new File("testSingleChildCount.in"));
//		Scanner inData = new Scanner(new File("testMinimum.in"));
//		Scanner inData = new Scanner(new File("testisBSTIncorrect.in"));
//		Scanner inData = new Scanner(new File("testisBSTCorrect.in"));
		Scanner inData = new Scanner(new File("testCreateBalanced.in"));
		

		String testName = inData.next();
		System.out.println(testName);
		
		String choice;
		do {
			choice = getNextChoice(inData);
			String info = choiceString(choice);
			if (!info.equals(""))
				System.out.println(info);
			if (!choice.equals(quit)) {
				executeChoice(choice, inData);

				// display(editor, 5);
			}
		} while (!choice.equals(quit));

	}

	// ask the user for what s/he wants to do
	private static String getNextChoice(Scanner inData) {
		String choice = null;
		choice = inData.next();
		inData.nextLine();
		return choice;
	}

	// Determine what the user wants to do and execute the appropriate method
	private static void executeChoice(String choice, Scanner inData) {
		switch (choice) {
		case createNewTree:
			tree = new BinarySearchTreeExtended<Integer, Member>(compareMembers);
			break;
		case add:
			executeAdd(inData);
			break;
		case addSequence:
			executeAddSequence(inData);
			break;
		case preload:
			executePreload(inData);
			break;
		case preloadSequence:
			executePreloadSequence(inData);
			break;
		case printTree:
			executePrintTree();
			break;
		case minimum:
			executeFindMinimum();
			break;
		case singleChildCount:
			executeSingleChildCount();
			break;
//		case leafCount:
//			executeLeafCount();
//			break;
		case isBST:
			executeIsBST();
			break;
		case createBalanced:
			executecreateBalanced(inData);
			break;

		// case insert:
		// executeInsert(inData);
		// break;
		// case deleteItem:
		// executeDeleteItem(inData);
		// break;
		}
		// executePrint(testA, size);
	}

	static void executePrintTree() {
		System.out.println("There are " + tree.size() + " nodes in the tree.");
		System.out.println(tree.inOrder());

	}

//	static private void executeLeafCount() {
//		System.out.println("Number of leaf nodes: " + tree.leafCount());
//	}

	static private void executeSingleChildCount() {
		System.out.println("Number of single parent nodes: " + tree.singleChildCount());
	}

	static private void executeFindMinimum() {
		System.out.println("Minimum value in the tree: " + tree.findMinimum());
	}

	static private void executeIsBST() {
		System.out.println("The tree is a BST is a  " + tree.isBST() + " statement.");
	}
	
	// execute createBalanced
	static void executecreateBalanced(Scanner inData) {
		// generate a new empty tree
		BinarySearchTree<Integer, Member> treeBal = new BinarySearchTreeExtended<Integer, Member>(compareMembers);
		preloadArrays(inData);
		CreateBalancedBST.createBalanced(treeBal, keys, elems);
		BinarySearchTreeExtended<Integer,Member> useThisTree =(BinarySearchTreeExtended<Integer,Member>)treeBal;
		System.out.println("Is the tree balanced? "+ useThisTree.isBST());
		System.out.println("Balanced tree has height " + useThisTree.height());
		System.out.println("Balanced tree has preOrder sequence\n" + treeBal.preOrder());
		
	}

	private static void  preloadArrays(Scanner inData) {
		int howMany = inData.nextInt();
		keys = new Integer[howMany];
		elems = new Member[howMany];
		Member temp;
		for (int i=0; i<howMany; i++) {
			temp = new Member(inData.nextInt(), inData.nextLine());
			keys[i] = temp.getKey();
			elems[i] = temp;
		}
		System.out.println("keys to add (in order): " + Arrays.toString(keys));
	}

	// execute preload
	static int executePreload(Scanner inData) {
		// generate a new tree
		tree = new BinarySearchTreeExtended<Integer, Member>(compareMembers);
		System.out.print("Starting with empty tree and adding elements ");

		int howMany = inData.nextInt();
		Member temp;
		for (int i = 0; i < howMany; i++) {
			temp = new Member(inData.nextInt(), inData.nextLine());
			System.out.print(temp.getKey() + " ");
			tree.insert(temp.getKey(), temp);
		}
		System.out.println();
		return howMany;
	}
	
	// execute preload
	static int executePreloadSequence(Scanner inData) {
		// generate a new tree
		tree = new BinarySearchTreeExtended<Integer, Member>(compareMembers);
		System.out.print("Starting with empty tree and adding elements (not BSTway) ");

		int howMany = inData.nextInt();
		Member temp;
		for (int i = 0; i < howMany; i++) {
			temp = new Member(inData.nextInt(), inData.nextLine());
			String seq = inData.nextLine().trim();
			System.out.print(temp.getKey() + " <"+seq+ "> ");
			tree.insertSeq(temp.getKey(), temp, seq);
		}
		System.out.println();
		return howMany;
	}


	static void executeAdd(Scanner inData) {
		Member toAdd = new Member(inData.nextInt(), inData.nextLine());
		System.out.println("Adding element " + toAdd.getKey());
		tree.insert(toAdd.getKey(), toAdd);
	}

	static void executeAddSequence(Scanner inData) {
		// generate a new tree
		tree = new BinarySearchTreeExtended<Integer, Member>(compareMembers);
		System.out.print("Starting with empty tree and adding (not in BST way) elements");

		int howMany = inData.nextInt();
		inData.nextLine();
		Member temp;
		for (int i = 0; i < howMany; i++) {
			temp = new Member(inData.nextInt(), inData.next());
			String sequence = inData.nextLine();
			tree.insertSeq(temp.getKey(), temp, sequence);
		}
	}

}
