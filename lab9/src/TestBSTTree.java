

import lab9.CreateBalancedBST;
import lab9.bst.BinarySearchTree;
//import lab9.solution.bst.BinarySearchTreeExtendedSolution;

public class TestBSTTree {

	public static void main (String [] args) {
		
		testCreateBST();
		
		testOtherMethods();
		
	}
	private static void testOtherMethods() {
//		BinarySearchTreeExtendedSolution<String, Integer> tree = new BinarySearchTreeExtendedSolution<String, Integer>();
//		tree.insert( "four", 4);
//		tree.insert( "eight", 8);
//		tree.insert( "two",2);
//		tree.insert( "five",5);
//		
//		System.out.println(tree.singleChildCount());
//		System.out.println(tree.findMinimum());
//		System.out.println(tree.isBST());
//		tree.insertSeq("one", 999, "LL");
//		System.out.println(tree.isBST());

		
	}

	private static void testCreateBST() {
		String [] keys = {   "anymore", "home","is", "like", "no", "place","there", "whatsoever"};
		Integer [] elems = {1, 2, 3, 4, 5, 6, 7,8};
		 
		BinarySearchTree<String, Integer> tree = new BinarySearchTree<String, Integer>();
		CreateBalancedBST.createBalanced(tree, keys, elems );
		System.out.println(tree.preOrder());
	}
}
