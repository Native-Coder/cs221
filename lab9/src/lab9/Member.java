package lab9;

public class Member  {

	private int key;
	private String name;
	
	
	public Member (int key, String name) {
		this.key = key;
		this.name = name;
	}
	
	public int getKey () {
		return key;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Member [key=" + key +  ", name=" + name + "]";
	}


}
