package lab9.bst;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import queue.UnboundedArrayQueue;

public class BinarySearchTree< K, T > implements BSTInterface< K, T > {

	/**
	 * The internal representation of a BST node
	 */
	class BSTNode {
		K key;
		T info;
		BSTNode left, right;

		public T getInfo() {
			return info;
		}

		public void setInfo( T info ) {
			this.info = info;
		}

		public K getKey() {
			return key;
		}

		public void setKey( K key ) {
			this.key = key;
		}

		public void setKeyAndInfo( K key, T info ) {
			setKey( key );
			setInfo( info );
		}

		public BSTNode getLeft() {
			return left;
		}

		public void setLeft( BSTNode left ) {
			this.left = left;
		}

		public BSTNode getRight() {
			return right;
		}

		public void setRight( BSTNode right ) {
			this.right = right;
		}

		public BSTNode( K key, T info, BinarySearchTree< K, T >.BSTNode left, BinarySearchTree< K, T >.BSTNode right ) {
			this.key = key;
			this.info = info;
			this.left = left;
			this.right = right;
		}

		@Override
		public String toString() {
			return key.toString() + " " + info.toString();
		}

	}

	// root of the binary search tree
	protected BSTNode root;
	// the comparator that determines the relation between two elements
	// in a binary search tree
	protected Comparator< K > comp;

	protected int size;

	public BinarySearchTree() {
		this.root = null;

		this.comp = new Comparator< K >() {
			@Override
			public int compare( K arg0, K arg1 ) {
				return ( (Comparable) arg0 ).compareTo( arg1 );
			}
		};
	}

	public BinarySearchTree( Comparator< K > comp ) {
		this.root = null;
		this.comp = comp;
	}

	/**
	 * Recursively insert element into the subtree root at node if the key does not
	 * already exist in the tree
	 */
	private BSTNode insertAux( BSTNode node, K target, T element ) {
		if ( node == null ) {
			// If node is null, create a new BSTNode and initialize the info
			// with element. Returns the newly created node.
			// increment the size of the tree
			size++;
			return new BSTNode( target, element, null, null );
		} else {
			int cmp = comp.compare( target, node.getKey() );

			// return the node if the key is found in the tree - nothing to add
			if ( cmp == 0 )
				return node;

			// recursively insert to the left if element is smaller
			// insert to the fight if element is larger.
			if ( cmp < 0 ) {
				node.setLeft( insertAux( node.getLeft(), target, element ) );
			} else if ( cmp > 0 ) {
				node.setRight( insertAux( node.getRight(), target, element ) );
			}

			return node;
		}

	}

	@Override
	public void insert( K key, T element ) {
		if ( element == null || key == null )
			throw new IllegalArgumentException( "Null parameter" );

		root = insertAux( root, key, element );

	}

	/**
	 * Recursively find the target in the subtree rooted at node.
	 * 
	 * If the element at node is equal, return the node. larger, recursively find in
	 * the left subtree smaller, recursively find in the right subtree
	 */
	private BSTNode findAux( BSTNode node, K target ) {
		// search fails
		if ( node == null ) {
			return null;
		}

		int cmp = comp.compare( target, node.getKey() );

		if ( cmp == 0 )
			return node;
		else if ( cmp < 0 ) {
			return findAux( node.getLeft(), target );
		} else if ( cmp > 0 ) {
			return findAux( node.getRight(), target );
		}

		return null;
	}

	@Override
	public T find( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		// return the info - if the key was found
		BSTNode result = findAux( root, key );
		if ( result == null )
			return null;
		return result.getInfo();

	}

	/**
	 * Determine whether the specified key is in the binary search tree and return
	 * true or false
	 * 
	 */
	public boolean contains( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		return findAux( root, key ) != null;
	}

	private T lastDeleted;

	/**
	 * Recursively remove target from the subtree rooted at node.
	 */
	private BSTNode removeAux( BSTNode node, K target ) {
		// if cannot continue, the search fails.
		if ( node == null )
			return null;

		int cmp = comp.compare( target, node.getKey() );
		if ( cmp == 0 ) {
			if ( lastDeleted == null )
				lastDeleted = node.getInfo();
			if ( node.getLeft() == null )
				return node.getRight();

			if ( node.getRight() == null )
				return node.getLeft();

			// find replacement and recursively remove the replacement
			BSTNode largestOnLeft = findLargest( node.getLeft() );
			// do the replacement and recursively remove the replacement from
			// the right subtree
			node.setKeyAndInfo( largestOnLeft.getKey(), largestOnLeft.getInfo() );
			node.setLeft( removeAux( node.getLeft(), largestOnLeft.getKey() ) );

		} else if ( cmp < 0 ) {
			// recursively remove from the left
			node.setLeft( removeAux( node.getLeft(), target ) );
		} else {
			// recursively remove from the right
			node.setRight( removeAux( node.getRight(), target ) );
		}

		return node;

	}

	private BSTNode findLargest( BSTNode node ) {
		while ( node.getRight() != null )
			node = node.getRight();

		return node;
	}

	/**
	 * Returns true if the tree follows the shape property
	 * 
	 * @return
	 */
	public boolean isBST() {
		return isBSTAux( root, null, null );
	}

	/**
	 * Auxillary function of isBST(). Recursively determines if each key is in a
	 * valid place in the tree
	 * 
	 * @param node  The root of the tree
	 * @param lower The lower bounds of the node being checked
	 * @param upper The upper bounds of the node being checked
	 * @return
	 */
	private boolean isBSTAux( BSTNode node, K lower, K upper ) {
		if ( node == null ) // Null is a valid BST node
			return true;

		return  isBSTAux( node.getLeft(), lower, node.getKey() ) && // Check the left
				isBetween( node, lower, upper ) &&                  // Check the current
				isBSTAux( node.getRight(), node.getKey(), upper );  // Check the right

	}

	/**
	 * Returns true if the tree rooted at node follows the shape property of a
	 * binary search tree.
	 * 
	 * @param node
	 * @param lower
	 * @param upper
	 * @return
	 */
	private boolean isBetween( BSTNode node, K lower, K upper ) {
		// This will only occur at the root of the tree. And since null is a valid
		// value, we return true
		if ( lower == null && upper == null )
			return true;
		// Null is a valid value for a BST node. therefore, we only care about the upper
		// bounds in this case
		else if ( lower == null )
			return comp.compare( node.getKey(), upper ) < 0;
		// Null is a valid value for a BST node. therefore, we only care about the lower
		// bounds in this case
		else if ( upper == null )
			return comp.compare( lower, node.getKey() ) < 0;
		// If both left and right are populated, we care about both the upper AND lower
		// bounds
		else
			return ( comp.compare( lower, node.key ) < 0 && comp.compare( node.key, upper ) < 0 );
	}

	@Override
	public T remove( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		lastDeleted = null;
		root = removeAux( root, key );

		// reduce number of elements in the tree on a successful delete
		if ( lastDeleted != null )
			size--;

		return lastDeleted;
	}

	public String inOrder() {
		return inOrderAux( root );
	}

	private String inOrderAux( BSTNode node ) {
		if ( node == null )
			return "";

		String left = inOrderAux( node.getLeft() );

		String right = inOrderAux( node.getRight() );

		return left + "\n" + node.toString() + "\n" + right;
	}

	public String preOrder() {
		return preOrderAux( root );
	}

	private String preOrderAux( BSTNode node ) {
		if ( node == null )
			return "";

		String left = preOrderAux( node.getLeft() );

		String right = preOrderAux( node.getRight() );

		// remove unnecessary empty lines...
		if ( !left.equals( "" ) )
			left = "\n" + left;
		if ( !right.equals( "" ) )
			right = "\n" + right;
		return node.toString() + " " + left + " " + right;
	}

	@Override
	public int size() {
		return size;
	}

	/**
	 * Finds the smallest value in the BST
	 * 
	 * @return
	 */
	public T findMinimum() {
		BSTNode result = findMinimumAux( root );
		return ( root == null ? null : result.getInfo() );
	}

	/**
	 * Finds the smallest BSTNode in the tree and returns the entire node
	 * 
	 * @param node
	 * @return
	 */
	private BSTNode findMinimumAux( BSTNode node ) {
		if( node == null)
			return null;
		if ( node.getLeft() == null )
			return node;
		return findMinimumAux( node.getLeft() );
	}

	/**
	 * counts the number of nodes in the tree that have only 1 of their two children
	 * set
	 * 
	 * @return
	 */
	public int singleChildCount() {
		return singleChildCountAux( root );
	}

	/**
	 * Returns the number of nodes that have only one of their two children
	 * instantiated
	 * 
	 * @param node The root of the tree we want to count
	 * @return
	 */
	private int singleChildCountAux( BSTNode node ) {
		// If there is no node, the count is 0
		if ( node == null )
			return 0;
		// If the left has a value, but the right does not, then add 1 to the count and
		// continue to traverse the left tree
		else if ( node.getLeft() != null && node.getRight() == null )
			return 1 + singleChildCountAux( node.getLeft() );
		// If the right has a value, but the left does not, then add 1 to the count and
		// continue to traverse the right tree
		else if ( node.getLeft() == null && node.getRight() != null )
			return 1 + singleChildCountAux( node.getRight() );
		// If neither are null, traverse both the left and right tree and sum the
		// returned values
		else
			return singleChildCountAux( node.getLeft() ) + singleChildCountAux( node.getRight() );
	}

	private void inOrderVisit( BSTNode node, UnboundedArrayQueue< T > queue ) {
		if ( node != null ) {
			inOrderVisit( node.getLeft(), queue );
			queue.enqueue( node.getInfo() );
			inOrderVisit( node.getRight(), queue );
		}
	}

	public Iterator< T > iterator() {
		return new Iterator< T >() {

			UnboundedArrayQueue< T > queue = new UnboundedArrayQueue< T >();
			{
				inOrderVisit( root, queue );
			}

			@Override
			public boolean hasNext() {
				return !queue.isEmpty();
			}

			@Override
			public T next() {
				if ( hasNext() ) {
					return queue.dequeue();
				} else
					throw new NoSuchElementException();
			}
		};
	}

}
;