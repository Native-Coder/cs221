package lab9.bst;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import lab9.bst.BSTInterface;
import queue.UnboundedArrayQueue;

public class BinarySearchTreeExtended< K, T > extends BinarySearchTree< K, T > {

	public BinarySearchTreeExtended() {
		super();
	}

	public BinarySearchTreeExtended( Comparator< K > comp ) {
		super( comp );
	}

	// determine the height of the tree
	public int height() {
		return recComputeHeight( root );
	}

	/**
	 * recursive helper method to determine the height
	 * 
	 * @param node The root of the subtree whose height is determined
	 */
	private int recComputeHeight( BSTNode node ) {
		// base case
		if ( node == null )
			return -1;

		// general case
		int leftHeight = recComputeHeight( node.getLeft() );
		int rightHeight = recComputeHeight( node.getRight() );

		// larger of these counts
		if ( leftHeight > rightHeight )
			return leftHeight + 1;

		return rightHeight + 1;
	}

	// lab 9 stuff for testing isBST
	/**
	 * Method to insert a node incorrectly into the tree
	 * 
	 * @param key      The keyvalue of the node to be added
	 * @param element  The element value of the node to be aded
	 * @param sequence The directions of moves (from root of tree) for where the
	 *                 node is to be added.
	 */
	public void insertSeq( K key, T element, String sequence ) {
		if ( element == null || key == null )
			throw new IllegalArgumentException( "Null parameter" );

		root = insertSeqAux( root, key, element, sequence, 0 );

	}

	/**
	 * Insert the element based on the given sequence; if sequence too short always
	 * move to the left
	 */
	private BSTNode insertSeqAux( BSTNode node, K target, T element, String sequence, int index ) {
		if ( node == null ) {
			// If node is null, create a new BSTNode and initialize the info
			// with element. Returns the newly created node.
			// increment the size of the tree
			size++;
			return new BSTNode( target, element, null, null );
		} else {
			// randomly determine in which subtree to insert the item
			if ( index >= sequence.length() || sequence.charAt( index ) == 'L' ) {
				node.setLeft( insertSeqAux( node.getLeft(), target, element, sequence, index + 1 ) );
			} else {
				node.setRight( insertSeqAux( node.getRight(), target, element, sequence, index + 1 ) );
			}
		}

		return node;
	}

}
