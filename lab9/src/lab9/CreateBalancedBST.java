package lab9;

import lab9.bst.BinarySearchTree;

/**
 * 
 * @author Ty Meador [Native-Coder]
 *
 */
public class CreateBalancedBST {

	/**
	 * Creates a balanced tree from the given arrays
	 * 
	 * @param <K>       data type used for keys
	 * @param <T>       data type used for elements
	 * @param tree      The tree to populate
	 * @param keyArray  an array of keys
	 * @param elemArray an array of elements that corresponds to the array of keys
	 */
	public static < K, T > void createBalanced( BinarySearchTree< K, T > tree, K[] keys, T[] elements ) {
		createBalancedAux( tree, keys, elements, 0, elements.length - 1 );
	}

	/**
	 * Creates a balanced tree from a given array set
	 * 
	 * @param <K>       data type used for keys
	 * @param <T>       data type used for elements
	 * @param tree      The tree to populate
	 * @param keyArray  an array of keys
	 * @param elemArray an array of elements that corresponds to the array of keys
	 * @param left      The starting index of this iteration
	 * @param right     The ending index of this iteration
	 */
	private static < K, T > void createBalancedAux(
		BinarySearchTree< K, T > tree,
		K[] keyArray,
		T[] elemArray,
		int left, int right
	) {
		// If left is greater than right, we are done.
		if ( left > right )
			return;
		
		// Integer division to find our mid point
		int mid = ( left + right ) / 2;
		// Insert the middle node as the root of the tree
		tree.insert( keyArray[ mid ], elemArray[ mid ] ); // Insert the middle value
		createBalancedAux( tree, keyArray, elemArray, left, mid - 1 );  // recurse left, don't include middle again
		createBalancedAux( tree, keyArray, elemArray, mid + 1, right ); // recurse right, don't include middle again
	}

}
