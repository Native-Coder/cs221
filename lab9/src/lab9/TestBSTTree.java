package lab9;

import lab9.bst.BinarySearchTree;
import lab9.bst.BinarySearchTreeExtended;

import java.lang.reflect.Array;

import lab9.CreateBalancedBST;

public class TestBSTTree {
	public static void main( String[] args ) {
		testSingleChildCount();
		testFindMinimum();
		testIsBST();
		testCreateBalanced();
	}

	public static void testIsBST() {
		System.out.println( "testing isBST():" );
		BinarySearchTree< String, String > bst = spawnBST();

		// Test with an empty tree
		System.out.println( "\tTesting with an empty tree: " + ( bst.isBST() ? "PASS" : "FAIL" ) );

		// Test with a single node
		bst.insert( "A", "A" );
		System.out.println( "\tTesting with a single node: " + ( bst.isBST() ? "PASS" : "FAIL" ) );

		// Testing with a balanced tree
		bst = spawnBST();
		bst.insert( "D", "D" );
		bst.insert( "B", "B" );
		bst.insert( "F", "F" );
		bst.insert( "A", "A" );
		bst.insert( "C", "C" );
		bst.insert( "E", "E" );
		bst.insert( "G", "G" );
		System.out.println( "\tTesting with a balanced tree of 7 nodes: " + ( bst.isBST() ? "PASS" : "FAIL" ) );

		// Testing with a malformed tree
		BinarySearchTreeExtended< String, String > ebst = spawnEBST();
		ebst.insert( "A", "A" );
		ebst.insertSeq( "B", "B", "L" );
		System.out.println(
				"\tTesting with a malformed tree; 'B' is left child of 'A': " + ( !ebst.isBST() ? "PASS" : "FAIL" ) );

		// Createa a balanced tree, then make a semi-colon the rightmost-value
		ebst = spawnEBST();
		ebst.insert( "D", "D" );
		ebst.insert( "B", "B" );
		ebst.insert( "F", "F" );
		ebst.insert( "A", "A" );
		ebst.insert( "C", "C" );
		ebst.insert( "E", "E" );
		ebst.insert( "G", "G" );
		ebst.insertSeq( ":", ":", "RRR" );
		System.out.println( "\tForcing semicolon as right-most node : " + ( !ebst.isBST() ? "PASS" : "FAIL" ) );

	}

	public static void testFindMinimum() {
		System.out.println( "\ntesting findMinimum() : " );

		// Test an empty tree
		BinarySearchTree< String, String > bst = spawnBST();
		System.out.println( "\tTesting empyt tree: " + ( bst.findMinimum() == null ? "PASS" : "FAIL" ) );

		// Test a skewed tree with 3 nodes
		bst.insert( "A", "A" );
		bst.insert( "B", "B" );
		bst.insert( "C", "C" );
		System.out.println( "\tTesting skewed tree with 3 nodes: " + ( bst.findMinimum() == "A" ? "PASS" : "FAIL" ) );

		// Test a balanced tree with 7 nodes
		bst = spawnBST();
		bst.insert( "D", "D" );
		bst.insert( "B", "B" );
		bst.insert( "F", "F" );
		bst.insert( "A", "A" );
		bst.insert( "C", "C" );
		bst.insert( "E", "E" );
		bst.insert( "G", "G" );
		System.out.println( "\tTesting balanced tree with 7 nodes: " + ( bst.findMinimum() == "A" ? "PASS" : "FAIL" ) );

		// Test a tree with one node
		bst = spawnBST();
		bst.insert( "A", "A" );
		System.out.println( "\tTesting a single-node tree: " + ( bst.findMinimum() == "A" ? "PASS" : "FAIL" ) );

		// Testing a tree with a random order. contains special characters
		bst = spawnBST();
		bst.insert( "T", "T" );
		bst.insert( "B", "B" );
		bst.insert( "F", "F" );
		bst.insert( "A", "A" );
		bst.insert( "P", "P" );
		bst.insert( ":", ":" );
		bst.insert( "&", "&" );
		bst.insert( "X", "X" );
		bst.insert( "Q", "Q" );
		bst.insert( "K", "K" );
		bst.insert( "G", "G" );
		System.out.println( "\tTesting special characters: " + ( bst.findMinimum() == "&" ? "PASS" : "FAIL" ) );

	}

	public static void testSingleChildCount() {
		System.out.println( "testSingleChildCount : " );

		// Tests an empty tree
		BinarySearchTree< String, String > bst = spawnBST();
		System.out.println( "\tTesting empty tree: " + ( ( bst.singleChildCount() == 0 ) ? "PASS" : "FAIL" ) );

		// Tests a skewed tree with 2 single children
		bst.insert( "A", "A" );
		bst.insert( "B", "B" );
		bst.insert( "C", "C" );
		System.out.println(
				"\tTesting skewed tree with 3 elements: " + ( ( bst.singleChildCount() == 2 ) ? "PASS" : "FAIL" ) );

		// Tests a balanced tree with no single children
		bst = spawnBST();
		bst.insert( "B", "B" );
		bst.insert( "A", "A" );
		bst.insert( "C", "C" );
		System.out.println(
				"\tTesting balanced tree with 3 elements: " + ( ( bst.singleChildCount() == 0 ) ? "PASS" : "FAIL" ) );

		// Tests a bush tree that has 2 single child nodes
		bst = spawnBST();
		bst.insert( "B", "B" );
		bst.insert( "A", "A" );
		bst.insert( "F", "F" );
		bst.insert( "C", "C" );
		bst.insert( "D", "D" );
		System.out.println( "\tTesting bushy tree with 2 singleChildeNodes: "
				+ ( ( bst.singleChildCount() == 2 ) ? "PASS" : "FAIL" ) );

		// Tests a single node
		bst = spawnBST();
		bst.insert( "A", "A" );
		System.out.println( "\tTesting with a single node: " + ( bst.singleChildCount() == 0 ? "PASS" : "FAIL" ) );

	}

	public static void testCreateBalanced() {
		BinarySearchTree< String, String > bst = spawnBST();
		CreateBalancedBST factory = new CreateBalancedBST();

		// This would normally produce a skewed tree
		String[] inOrder = {
				"A", "B", "C"
		};

		// This will always produce a balanced tree
		String[] preOrder = {
				"B", "A", "C"
		};

		// The postOrder traversal of the preOrder
		String[] postOrder = {
				"B", "C", "A"
		};

		// Don't care what values are stored. only how the keys are behaving
		String[] values = {
				"", "", ""
		};

		// This should create a full, balanced tree
		String[] full = {
				"A", "B", "C", "D", "E", "F", "G"
		};

		// Don't care what values are stored. only how the keys are behaving
		String[] fullVals = {
				"", "", "", "", "", "", ""
		};


		// Testing an empty tree
		bst = spawnBST();
		System.out.println( "Testing an empty tree..." );
		System.out.println( "\tChecking pre-order sequence..." + ( isBalanced( bst, "" ) ? "PASS" : "FAIL" ) );

		// Testing 3 nodes that are in order
		factory.createBalanced( bst, inOrder, values );
		System.out.println( "Testing balanced tree with three nodes, all of which are in order..." );
		System.out.println( "\tChecking pre-order sequence..." + ( isBalanced( bst, "BAC" ) ? "PASS" : "FAIL" ) );

		// Testing 3 nodes in preOrder sequence
		bst = spawnBST();
		factory.createBalanced( bst, preOrder, values );
		System.out.println( "Testing balanced keys in preOrder..." );
		System.out.println( "\tChecking if pre-order matches..." + ( isBalanced( bst, "ABC" ) ? "PASS" : "FAIL" ) );

		// Test 3 nodes in postOrder sequence
		bst = spawnBST();
		factory.createBalanced( bst, postOrder, values );
		System.out.println( "Testing balanced keys in postOrder..." );
		System.out.println( "\tChecking if pre-order matches..." + ( isBalanced( bst, "CBA" ) ? "PASS" : "FAIL" ) );

		// Testing a full tree
		bst = spawnBST();
		factory.createBalanced( bst, full, fullVals );
		System.out.println( "Testing a full tree..." );
		System.out.println(
				"\tChecking if pre-order matches.." + ( isBalanced( bst, "DBACFEG" ) ? "PASS" : "FAIL" ) );
	}

	private static boolean isBalanced( BinarySearchTree< String, String > tree, String expectedOutput ) {
		return tree.preOrder().replace( "\n", "" ).replace( " ", "" ).equals( expectedOutput );
	}

	private static BinarySearchTreeExtended< String, String > spawnEBST() {
		return new BinarySearchTreeExtended< String, String >();
	}

	private static BinarySearchTree< String, String > spawnBST() {
		return new BinarySearchTree< String, String >();
	}
}
