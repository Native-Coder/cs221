package lab8;

import lab8.bst.BinarySearchTree;

public class WordCount {
	private static int wordCount = 0; // The size of the tree (not entirely necessary, but fun! :) 
	private int count;
	private String value;
	
	/**
	 * Basic instantiation constructor
	 */
	public WordCount() {
		this( "" );
	}
	
	/**
	 * Initialization constructor
	 * @param word 
	 */
	public WordCount( String word ) {
		wordCount++;
		this.value = word;
		this.count = 1;
	}
	
	/**
	 * Copy constructor
	 * @param copy The WordCount object to copy
	 */
	public WordCount( WordCount copy ) {
		this.value = copy.getValue(); // Copy the value
		this.count = copy.count;      // Copy count
	}
	
	public void inc() {
		count++;
	}
	
	public String getValue() {
		return value;
	}
	
	public int getCount() {
		return count;
	}

	public int getDistinctWordCount() {
		return wordCount;
	}
	
	@Override
	public String toString() {
		return "WordCount [str=" + value + ", count=" + count + "]";
	}
	
	
}
