package lab8;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;

import lab8.WordCount;
import lab8.bst.BinarySearchTree;
import lab8.list.AArrayList;
import lab8.list.ListInterface;

public class DocumentWordCount {

	static Comparator <String> ignoreCase = ( String o1, String o2 ) -> o1.toLowerCase().compareTo( o2.toLowerCase() );

	static BinarySearchTree <String, WordCount> tree = new BinarySearchTree <String, WordCount>( ignoreCase );

	public void query( String key ) {
		System.out.println( tree.find( key ) );
	}

	/**
	 * prints the word to be checked and how often it occurred in the document. If
	 * the word does not occur in the document, then the count is 0
	 * 
	 * @param wordsToCheck
	 */
	public void checkWords( Scanner wordsToCheck ) {
		String next;
		WordCount node;
		int count = 0;
		// Check all the words
		while ( wordsToCheck.hasNext() ) {
			next = wordsToCheck.next(); // Grab the next word to be checked
			node = tree.find( next ); // find a node with the next word as its key
			// If the word wasn't found in the tree, then it didn't occur in the document
			if ( node == null )
				count = 0;
			else
				count = node.getCount();

			System.out.println( String.format( "Checking next word: %5s %4d ", next, count ) );
		}

	}

	/**
	 * Returns the word that occurred the most times
	 * 
	 * @return
	 */
	public WordCount findMostFrequent() {

		Iterator <WordCount> itr = tree.iterator(); // Get an iterator from the BST
		WordCount max = null; // default to null if the tree is empty

		// Iterate through each node in the tree
		while ( itr.hasNext() ) {

			WordCount word = itr.next(); // Next BST node

			// Set the max
			if ( max == null || word.getCount() > max.getCount() )
				max = word;
		}

		// Return the max that we found
		return max;
	}

	/**
	 * Returns an array list containing the longest words in the BST
	 * 
	 * @return AArrayList<WordCount>
	 */
	public ListInterface <WordCount> findMaxlength() {

		AArrayList <WordCount> list = null; // The list to store our longest words
		WordCount word = null; // To store the WordCount returned by itr.next()
		Iterator <WordCount> itr = tree.iterator(); // An iterator to iterate over the BST
		int currentSize = 0; // The size of the longest word found so far

		// Iterate over the tree
		while ( itr.hasNext() ) {

			word = itr.next(); // Get the next word

			// If we found a word that is longer than the words in our list....
			if ( word.getValue().length() > currentSize ) {

				list = new AArrayList <WordCount>(); // Create a new, empty list
				list.add( word ); // Add the new longest word
				currentSize = word.getValue().length(); // Save the size of the new longest word

				// If we found another word of the same length as our longest words
			} else if ( word.getValue().length() == currentSize ) {

				list.add( word ); // just add it to the list

			}
		}
		// Return the list we generated
		return list;

	}

	/**
	 * 
	 * @param inData
	 * @param minWordSize
	 * @param maxWordSize
	 * @return
	 */
	public int loadTreeFromScanner( Scanner inData, int minWordSize, int maxWordSize ) {
		String next; // Temporary storage
		// read each word
		while ( inData.hasNext() ) {
			next = inData.next();
			// If the words is between min and max (inclusive)
			if ( next.length() >= minWordSize && next.length() <= maxWordSize )
				upsert( next );// upsert
		}
		return tree.size();

	}

	/**
	 * Updates the word count if the key exists, creates a new node with the given
	 * key if it doesn't
	 * 
	 * @param key The key to insert/update
	 * @return the word count of the given key
	 */
	private int upsert( String key ) {
		WordCount node = tree.find( key );

		if ( node != null ) {
			node.inc();
			return node.getCount();
		} else {
			tree.insert( key, new WordCount( key ) );
			return 1;
		}
	}

}