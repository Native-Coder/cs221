package lab8.bst;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import lab8.queue.UnboundedArrayQueue;

public class BinarySearchTree<K, T> implements BSTInterface <K, T> {

	/**
	 * The internal representation of a BST node
	 */
	class BSTNode {
		K key;
		T info;
		BSTNode left, right;

		public T getInfo() {
			return info;
		}

		public void setInfo( T info ) {
			this.info = info;
		}

		public K getKey() {
			return key;
		}

		public void setKey( K key ) {
			this.key = key;
		}

		public void setKeyAndInfo( K key, T info ) {
			setKey( key );
			setInfo( info );
		}

		public BSTNode getLeft() {
			return left;
		}

		public void setLeft( BSTNode left ) {
			this.left = left;
		}

		public BSTNode getRight() {
			return right;
		}

		public void setRight( BSTNode right ) {
			this.right = right;
		}

		public BSTNode( K key, T info, BinarySearchTree <K, T>.BSTNode left, BinarySearchTree <K, T>.BSTNode right ) {
			this.key = key;
			this.info = info;
			this.left = left;
			this.right = right;
		}

		@Override
		public String toString() {
			return key.toString() + " " + info.toString();
		}

	}

	// root of the binary search tree
	private BSTNode root;
	// the comparator that determines the relation between two elements
	// in a binary search tree
	private Comparator <K> comp;

	private int size;

	public BinarySearchTree() {
		this.root = null;

		this.comp = new Comparator <K>() {
			@Override
			public int compare( K arg0, K arg1 ) {
				return ( (Comparable) arg0 ).compareTo( arg1 );
			}
		};
	}

	public BinarySearchTree( Comparator <K> comp ) {
		this.root = null;
		this.comp = comp;
	}

	/**
	 * Recursively insert element into the subtree root at node if the key does not
	 * already exist in the tree
	 */
	private BSTNode insertAux( BSTNode node, K target, T element ) {
		if ( node == null ) {
			// If node is null, create a new BSTNode and initialize the info
			// with element. Returns the newly created node.
			// increment the size of the tree
			size++;
			return new BSTNode( target, element, null, null );
		} else {
			int cmp = comp.compare( target, node.getKey() );

			// return the node if the key is found in the tree - nothing to add
			if ( cmp == 0 )
				return node;

			// recursively insert to the left if element is smaller
			// insert to the fight if element is larger.
			if ( cmp < 0 ) {
				node.setLeft( insertAux( node.getLeft(), target, element ) );
			} else if ( cmp > 0 ) {
				node.setRight( insertAux( node.getRight(), target, element ) );
			}

			return node;
		}

	}

	@Override
	public void insert( K key, T element ) {
		if ( element == null || key == null )
			throw new IllegalArgumentException( "Null parameter" );

		root = insertAux( root, key, element );

	}

	/**
	 * Recursively find the target in the subtree rooted at node.
	 * 
	 * If the element at node is equal, return the node. larger, recursively find in
	 * the left subtree smaller, recursively find in the right subtree
	 */
	private BSTNode findAux( BSTNode node, K target ) {
		// search fails
		if ( node == null ) {
			return null;
		}

		int cmp = comp.compare( target, node.getKey() );

		if ( cmp == 0 )
			return node;
		else if ( cmp < 0 ) {
			return findAux( node.getLeft(), target );
		} else if ( cmp > 0 ) {
			return findAux( node.getRight(), target );
		}

		return null;
	}

	@Override
	public T find( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		// return the info - if the key was found
		BSTNode result = findAux( root, key );
		if ( result == null )
			return null;
		return result.getInfo();

	}

	/**
	 * Determine whether the specified key is in the binary search tree and return
	 * true or false
	 * 
	 */
	public boolean contains( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		return findAux( root, key ) != null;
	}

	private T lastDeleted;

	/**
	 * Recursively remove target from the subtree rooted at node.
	 */
	private BSTNode removeAux( BSTNode node, K target ) {
		// if cannot continue, the search fails.
		if ( node == null )
			return null;

		int cmp = comp.compare( target, node.getKey() );
		if ( cmp == 0 ) {
			if ( lastDeleted == null )
				lastDeleted = node.getInfo();
			if ( node.getLeft() == null )
				return node.getRight();

			if ( node.getRight() == null )
				return node.getLeft();

			// find replacement and recursively remove the replacement
			BSTNode largestOnLeft = findLargest( node.getLeft() );
			// do the replacement and recursively remove the replacement from
			// the right subtree
			node.setKeyAndInfo( largestOnLeft.getKey(), largestOnLeft.getInfo() );
			node.setLeft( removeAux( node.getLeft(), largestOnLeft.getKey() ) );

		} else if ( cmp < 0 ) {
			// recursively remove from the left
			node.setLeft( removeAux( node.getLeft(), target ) );
		} else {
			// recursively remove from the right
			node.setRight( removeAux( node.getRight(), target ) );
		}

		return node;

	}

	private BSTNode findLargest( BSTNode node ) {
		while ( node.getRight() != null )
			node = node.getRight();

		return node;
	}

	@Override
	public T remove( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		lastDeleted = null;
		root = removeAux( root, key );

		// reduce number of elements in the tree on a successful delete
		if ( lastDeleted != null )
			size++;

		return lastDeleted;
	}

	public String inOrder() {
		return inOrderAux( root );
	}

	private String inOrderAux( BSTNode node ) {
		if ( node == null )
			return "";

		String left = inOrderAux( node.getLeft() );

		String right = inOrderAux( node.getRight() );

		return left + "\n" + node.toString() + "\n" + right;
	}

	public String preOrder() {
		return preOrderAux( root );
	}

	private String preOrderAux( BSTNode node ) {
		if ( node == null )
			return "";

		String left = preOrderAux( node.getLeft() );

		String right = preOrderAux( node.getRight() );

		return node.toString() + "\t" + left + "\t" + right;
	}

	@Override
	public int size() {
		return size;
	}

	private void inOrderVisit( BSTNode node, UnboundedArrayQueue <T> queue ) {
		if ( node != null ) {
			inOrderVisit( node.getLeft(), queue );
			queue.enqueue( node.getInfo() );
			inOrderVisit( node.getRight(), queue );
		}
	}

	public Iterator <T> iterator() {
		return new Iterator <T>() {

			UnboundedArrayQueue <T> queue = new UnboundedArrayQueue <T>();
			{
				inOrderVisit( root, queue );
			}

			@Override
			public boolean hasNext() {
				return !queue.isEmpty();
			}

			@Override
			public T next() {
				if ( hasNext() ) {
					return queue.dequeue();
				} else
					throw new NoSuchElementException();
			}
		};
	}

}
