package lab8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import lab8.WordCount;
import lab8.list.ListInterface;
import lab8.DocumentWordCount;

public class DocumentWordCountApp {
	static String[] testFiles = {
			"big.txt",
			/**
			 * Lyrics to a Tech N9ne song. reason chosen: it has a lot of words and it was
			 * fun. 547 unique words Word counts to check (verified from wordcounttools.com)
			 * the (78) i (52) a (50) and (38) to (34) ruler (1) lorem (0) ipsum (0)
			 */
			"./speedom.in",
			/**
			 * The 8 first words of lorem ipsum reason chosen: it was easy to count by hand
			 * and verify. (also used as a control to make sure that wordcounttools.com was
			 * worth using to verify my other results. 8 unique words word counts to check
			 * (verified by hand, used as control for wordcounttools.com) the (0) i (0) a
			 * (0) and (0) to (0) ruler (0) lorem (1) ipsum (1)
			 */
			"./simpleIpsum.in",

	};

	public static void main( String[] args ) {
		try {
			DocumentWordCount dwc = new DocumentWordCount();
			for ( String file : testFiles ) {
				Scanner inData = new Scanner( new File( file ) );
				System.out.println( "Loaded file " + file );
				Scanner input = new Scanner( System.in );
				dwc.loadTreeFromScanner( inData, 0, Integer.MAX_VALUE );
				System.out.println( "Longest words in the file are" );
				for( WordCount word : dwc.findMaxlength() ) {
					System.out.println( "\t" + word);
				}
				System.out.println( "Most refrequently used word was " + dwc.findMostFrequent() );
				System.out.println( "Total word count: " + dwc.findMostFrequent().getDistinctWordCount());
				
				dwc.checkWords( input );
			}

		} catch ( FileNotFoundException e ) {
			System.out.println( "Couldn't find the file" );
		}

	}
}
