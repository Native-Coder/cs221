package lab8.queue;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IterableArrayQueue<T> extends BoundedArrayQueue<T> implements Iterable<T> {

   public IterableArrayQueue(int size) {
      super(size);
   }
   /**
    * returns an instance from a anonymous inner class.
    */ 
   public Iterator<T> iterator() {
      return new Iterator<T>() {
         private int index = 0;

         public T next() {
            T res;
            
            if (!hasNext())
               throw new NoSuchElementException("No next element available!");
            res = elements[(index + front) % elements.length];
            index++;
            
            return res;
         }

         public boolean hasNext() {
            return index < size();
         }

         public void remove() {
            throw new UnsupportedOperationException("remove operation not supported!");
         }
      };
   }

}