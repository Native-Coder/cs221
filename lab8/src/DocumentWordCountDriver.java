

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import lab8.WordCount;
import lab8.list.ListInterface;
import lab8.DocumentWordCount;

public class DocumentWordCountDriver{


	
	  public static void main(String[] args) throws IOException 
	  {
		    // Set up command line reading
//		    Scanner inData = new Scanner(System.in);

		    Scanner inData = new Scanner (new File ("test2.in"));
		    // Set up file reading
		    String fn;
		    fn = inData.next();
		    Scanner wordsIn = new Scanner(new FileReader(fn));
		    wordsIn.useDelimiter("[^a-zA-Z']");  // delimiters are nonletters,'

		    // get min and max
		    int min = inData.nextInt();
		    int max = inData.nextInt();
		    DocumentWordCount dwc = new DocumentWordCount();
		    System.out.println("Reading document " + fn + "\nOnly include words with length between " + min + 
		    				" and " + max + " inclusive.");
		    System.out.println("Different words stored in the tree: " + dwc.loadTreeFromScanner(wordsIn,min,max));
		    
		    
		    wordsIn.close();
		    

		    dwc.checkWords(inData);
		    
		    
		    System.out.println("The most frequent word is found in <" + dwc.findMostFrequent() + " >");
		    System.out.println("The longest word(s) is/are: ");
		    ListInterface<WordCount> longestWords = dwc.findMaxlength();
		    for (WordCount  word : longestWords) {
		    	System.out.println(word);
		    }
		    		    
		    System.out.println("Program completed.");

	  }

}
