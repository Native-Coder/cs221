package lab2;


public class ArrayOfCircles implements SimpleListInterface {
	
	protected  Circle [] circArray = null;
	protected  int size;
	
	
	/**
	 * Constructor with specified capacity
	 * @param capacity The size of the array to be created
	 */
	public ArrayOfCircles ( int capacity ) {
		circArray = new Circle[ capacity ];
		size      = 0;
	}	
	
	/**
	 * 
	 * @return  The number of elements in the list
	 */
	public int size() {
		return size;
	}
	
	/**
	 * @ returns True if the list is not full; false otherwise
	 */
	public boolean isFull( ) {
		return ( size >= circArray.length );
	}
	
	/**
	 * Gets the circle element - 
	 * @param The element to be returned
	 * @return The circle element in the list (with the lowest position if more than one); 
	 * null if no element in the list is equal to the given element
	 */
	public Circle get ( Circle element ) {
		int index = search( element );
		if ( index != -1 )
			return circArray[ index ];
		// not found
		return null;
	}

	/**
	 * Gets the circle at the specified position - 
	 * @param position The position of the elem in the list 
	 * (Note: position must be between 0 and size-1 (inclusive))
	 * @return The circle at the specified position
	 */
	public Circle get ( int  position ) {
		return circArray[ position ];
	}

	
	/**
	 * Search for the element in the list (using the equals function of T)
	 * @param element
	 * @return The position (start counting at 0) of the element in the list (if found); -1 otherwise
	 */
	public int search( Circle Element ) {
		for( int i = 0; i < circArray.length; i++ ) {
			if( Element.equals( circArray[ i ] ) )
				return i;
		}
		return -1;
	}
	
	/**
	 * Add an element to the end of the list; if the array is full, the element is 
	 * not added.
	 * Note: The 'end' is not at index array.length-1 but at size-1
	 * @param The element to be added
	 * @return True if the element was added; false otherwise
	 */
	public boolean insert( Circle Element ) {
		if( this.isFull() ) return false;
		
		this.circArray[ this.size ] = Element;
		this.size++;
		return true;
	}
	
	/**
	 * Delete the element from the list without changing the order 
	 * of the remaining elements in the list. If there is more than one element 
	 * equal to the one to be deleted, then the one with the lowest position is deleted.
	 * @param The element to be deleted
	 * @return true - if the element was deleted; false if the element was not found in the list
	 */
	public boolean delete ( Circle Element ) {
		//Search for the element
		for( int i = 0; i < this.size; i++ ) {
			// If we find the element
			if( this.circArray[ i ].equals( Element ) ) {
				// Take everything below the element and shift it up by one
				for( int i2 = i; i2 < this.size - 1; i2++ ) {
					this.circArray[ i2 ] = this.circArray[ i2 + 1 ];
				}
				// Decrement size
				this.size--;
				// Set the end to null
				this.circArray[ this.size ] = null;

				return true;
			}
		}
		return false;
	}
}



































