package lab2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ArrayOfCirclesApp {

	static SimpleListInterface circLst = new ArrayOfCircles(5);

	public static void main(String[] args) {

		// testing insert
		circLst = new ArrayOfCircles(3);

		circLst.insert(new Circle(1, 2, 3, "four"));
		circLst.insert(new Circle(3, 4, 5, "three"));
		printCirles();

		// testing readFromFile
		circLst = new ArrayOfCircles(5);
		readFromFile("circleLoadtesting.txt");
		printCirles();

	}

	/**
	 * Loads the circLst with the circle info from the given file
	 * @param fileName The file with the circle information
	 */
	public static void readFromFile( String fileName ) {
		try {
			Scanner reader = new Scanner( new File( fileName ) );
			while( reader.hasNextDouble() ) {
				/* I'm not sure if this is considered "smelly code" in Java */
				circLst.insert( new Circle(
					reader.nextDouble(),
					reader.nextDouble(),
					reader.nextDouble(),
					reader.next()
				));
				reader.nextLine();
			}
			reader.close();
		} catch ( FileNotFoundException e ) {
			e.printStackTrace();
		}
	}

	/**
	 * Prints the info about the list: first a message with the number of elements
	 * and then each element
	 * 
	 * @param list The list to be printed
	 */
	public static void printCirles(SimpleListInterface list) {
		System.out.println("The list has " + list.size() + " elements.");
		for (int i = 0; i < list.size(); i++) {
			System.out.println( i + ": " + list.get( i ) );
		}
	}

	/**
	 * Prints the info about the current list: first a message with the number of
	 * elements and then each element
	 */
	public static void printCirles() {
		printCirles(circLst);
	}

}
