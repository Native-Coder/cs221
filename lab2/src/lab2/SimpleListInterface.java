package lab2;

// Note: Usually this should be a generic interface - but for the purpose of lab2 we fix the 
//  type to Circle - we will later look into how to update this to a generic interface and a 
//  generic implementation
public interface SimpleListInterface {
	
	/**
	 * 
	 * @return  The number of elements in the list
	 */
	int size();
	
	/**
	 * Search for the element in the list (using the equals function of T)
	 * @param element
	 * @return The position (start counting at 0) of the element in the list (if found); -1 otherwise
	 */
	int search (Circle element);
	
	/**
	 * Add an element to the end of the list; if the array is full, the element is 
	 * not added.
	 * Note: The 'end' is not at index array.length-1 but at size-1
	 * @param The element to be added
	 * @return True if the element was added; false otherwise
	 */
	boolean insert(Circle element);
	
	
	/**
	 * Delete the element from the list without changing the order 
	 * of the remaining elements in the list. If there is more than one element 
	 * equal to the one to be deleted, then the one with the lowest position is deleted.
	 * @param The element to be deleted
	 * @return true - if the element was deleted; false if the element was not found in the list
	 */
	boolean delete (Circle element) ;
	
	/**
	 * Gets the circle element - 
	 * @param The element to be returned
	 * @return The circle element in the list (with the lowest position if more than one); 
	 * null if no element in the list is equal to the given element
	 */
	Circle get (Circle element) ;

	/**
	 * Gets the circle at the specified position - 
	 * @param position The position of the elem in the list 
	 * (Note: position must be between 0 and size-1 (inclusive))
	 * @return The circle at the specified position
	 */
	Circle get (int  position) ;

	/**
	 * @ returns True if the list is not full; false otherwise
	 */
	boolean isFull();

}
