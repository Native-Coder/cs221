
import lab2.ArrayOfCircles;
import lab2.ArrayOfCirclesApp;
import lab2.Circle;
import lab2.SimpleListInterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ArrayOfCircleDriver {

	private static SimpleListInterface list;

	private static final String createNewList = "createNewList";
	private static final String insert = "insert";
	private static final String printList = "printList";
	private static final String delete = "delete";
	private static final String search = "search";
	private static final String size = "size";

	private static final String quit = "quit";

	private static String choiceString(String choice) {
		switch (choice) {
		case insert:
			return "\n* Testing add an element to the list";
		case delete:
			return "\n* Testing delete an element from the list";
		case search:
			return "\n* Testing search for an element in the list ";
		case size:
			return "\n* ListInfo: Display the size of the list ";
		case createNewList:
			return "\n* Setup for next testing phase: Create a new list";
		case printList:
			return "\n* ListInfo: Print the info in the list";
		case quit:
			return "quit";
		default:
			return "nothing";
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		// read data from re-directed file
		// Scanner inData = new Scanner(System.in);

//		Scanner inData = new Scanner(new File("testListMixed.in"));
//		Scanner inData = new Scanner(new File("testListinsert.in"));
//		Scanner inData = new Scanner(new File("testListinsertTooMany.in"));
//		Scanner inData = new Scanner(new File("testListSearchFullList.in"));
//		Scanner inData = new Scanner(new File("testListSearchNotFullList.in"));
//		Scanner inData = new Scanner(new File("testListSearchEmptyList.in"));
		Scanner inData = new Scanner(new File("testListDeleteNotFullList.in"));
//		Scanner inData = new Scanner(new File("testListDeleteFullList.in"));
//		Scanner inData = new Scanner(new File("testListDeleteEmptyList.in"));
//		while (inData.hasNext()) {
//			String line= inData.nextLine();
//			System.out.println("line = "+ line);
//		}
		// Scanner inData = new Scanner(new File("test5.in"));
		// Scanner inData = new Scanner(new File("test4.in"));
		// Scanner inData = new Scanner(new File("test3.in"));
		// Scanner inData = new Scanner(new File("test2.in"));
		// Scanner inData = new Scanner(new File("test1.in"));

		String choice;
		do {
			choice = getNextChoice(inData);
			String info = choiceString(choice);
			if (!info.equals(""))
				System.out.println(info);
			if (!choice.equals(quit)) 
				executeChoice(choice, inData);
		} while (!choice.equals(quit));

	}
	
	// ask the user for what s/he wants to do
	private static String getNextChoice(Scanner inData) {
		String choice = null;
		choice = inData.next();
		inData.nextLine();
		return choice;
	}


	// Determine what the user wants to do and execute the appropriate method
	static void executeChoice(String choice, Scanner inData) {
		switch (choice) {
		case insert:
			executeInsert(inData);
			break;
		case delete:
			executeDelete(inData);
			break;
		case createNewList:
			executeCreate(inData);
			break;
		case search:
			executeSearch(inData);
			break;
		case size:
			executeSize();
			break;
		case printList:
			executePrint();
			break;

		}
		// executePrint(testA, size);
	}


	// perform the necessary update to complete the insert
	static void executeInsert(Scanner inData) {
		// System.out.print("Enter the circle info (x, y, width, height): ");
		Circle temp = new Circle(inData.nextDouble(), inData.nextDouble(), inData.nextDouble(), inData.nextLine());
		boolean res = list.insert(temp);
		String notStr = (res)? "" : "not";
		System.out.println("Element " + temp+ " was " + notStr + " inserted.");
	}

	// perform the necessary updates to perform the delete
	static void executeDelete(Scanner inData) {
		// read circle info from scanner and search for it in list
		Circle target = makeCircleFromScanner(inData);
		int index = list.search(target);
		
		boolean res = list.delete(target);
		String notStr = (res)? "" : "not";

		System.out.println("Element " + target + " was " + notStr + " deleted from index " + index + ".");
	}

	// perform the necessary updates to perform the delete
	static void executeSearch(Scanner inData) {
		// System.out.print("Enter info of circle to be deleted: ");
		Circle target = makeCircleFromScanner(inData);
		
		int index = list.search(target);
		System.out.println("Search for " + target + " returns " + index);
	}

	private static Circle makeCircleFromScanner(Scanner inData) {
		double radius = inData.nextDouble();
		double centerX = inData.nextDouble();
		double centerY = inData.nextDouble();
		Circle target = new Circle (radius, centerX, centerY, "");
		inData.nextLine();
		return target;
	}
	
	// perform the necessary updates to perform the get operation
	static void executeSize() {
		// System.out.print("Enter subscript to be deleted: ");
		System.out.println("The list has " + list.size() + " elements.");
	}
	

	// perform the necessary updates to perform the get operation
	static void executeGet(Scanner inData) {
		// System.out.print("Enter subscript to be deleted: ");
		int index = inData.nextInt();
		inData.nextLine();
		System.out.println("get elem at index " + index + ": " + list.get(index));
	}

	// create a new rectangle array
	static void executeCreate(Scanner inData) {
		// how much elements should the array hold?
		int capacity = inData.nextInt();
		inData.nextLine();
		list = new ArrayOfCircles(capacity);

		System.out.println("Created list with " + capacity + " slots. \n ");

	}

	// prints the list
	static void executePrint() {
		ArrayOfCirclesApp.printCirles(list);
	}

}
