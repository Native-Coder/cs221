
import lab2.ArrayOfCirclesApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ArrayOfCircleAppDriver {

	private static final String load = "load";
	private static final String quit = "quit";

	private static String choiceString(String choice) {
		switch (choice) {
		case load:
			return "\n Loading a list from a file";
		case quit:
			return "quit";
		default:
			return "nothing";
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		// read data from re-directed file
		// Scanner inData = new Scanner(System.in);

		Scanner inData = new Scanner(new File("testListAppLoadNotFull.in"));
//		Scanner inData = new Scanner(new File("testListAppLoadFull.in"));
//		Scanner inData = new Scanner(new File("testListAppLoadTooMany.in"));

		String choice;
		do {
			choice = getNextChoice(inData);
			String info = choiceString(choice);
			if (!info.equals(""))
				System.out.println(info);
			if (!choice.equals(quit)) 
				executeChoice(choice, inData);
		} while (!choice.equals(quit));

	}
	
	// ask the user for what s/he wants to do
	private static String getNextChoice(Scanner inData) {
		String choice = null;
		choice = inData.next();
		inData.nextLine();
		return choice;
	}


	// Determine what the user wants to do and execute the appropriate method
	static void executeChoice(String choice, Scanner inData) {
		switch (choice) {
		case load:
			executeLoad(inData);
			break;
		}
	}

	// perform the necessary updates to complete the load
	static void executeLoad(Scanner inData) {
		// System.out.print("Enter the name of the textfile: ");
		String fileName = inData.nextLine();
		System.out.println("File used for loading: " + fileName);
		ArrayOfCirclesApp.readFromFile(fileName);
		
		// print result
		System.out.println("The loaded circles are printed.");
		ArrayOfCirclesApp.printCirles();
	}

//	// perform the necessary update to complete the insert
//	static void executeInsert(Scanner inData) {
//		// System.out.print("Enter the circle info (x, y, width, height): ");
//		Circle temp = new Circle(inData.nextDouble(), inData.nextDouble(), inData.nextDouble(), inData.nextLine());
//		boolean res = list.insert(temp);
//		String notStr = (res)? "" : "not";
//		System.out.println("Element " + temp+ " was " + notStr + " inserted.");
//	}
//
//	// perform the necessary updates to perform the delete
//	static void executeDelete(Scanner inData) {
//		// read circle info from scanner and search for it in list
//		Circle target = makeCircleFromScanner(inData);
//		int index = list.search(target);
//		
//		boolean res = list.delete(target);
//		String notStr = (res)? "" : "not";
//
//		System.out.println("Element " + target + " was " + notStr + " deleted from index " + index + ".");
//	}
//
//	// perform the necessary updates to perform the delete
//	static void executeSearch(Scanner inData) {
//		// System.out.print("Enter info of circle to be deleted: ");
//		Circle target = makeCircleFromScanner(inData);
//		
//		int index = list.search(target);
//		System.out.println("Search for " + target + " returns " + index);
//	}
//
//	private static Circle makeCircleFromScanner(Scanner inData) {
//		double radius = inData.nextDouble();
//		double centerX = inData.nextDouble();
//		double centerY = inData.nextDouble();
//		Circle target = new Circle (radius, centerX, centerY, "");
//		inData.nextLine();
//		return target;
//	}
//	
//	
//
//	// perform the necessary updates to perform the get operation
//	static void executeGet(Scanner inData) {
//		// System.out.print("Enter subscript to be deleted: ");
//		int index = inData.nextInt();
//		inData.nextLine();
//		System.out.println("get elem at index " + index + ": " + list.get(index));
//	}
//
//	// create a new rectangle array
//	static void executeCreate(Scanner inData) {
//		// how much elements should the array hold?
//		int capacity = inData.nextInt();
//		inData.nextLine();
//		list = new ArrayOfCircles(capacity);
//
//		System.out.println("Created list with " + capacity + " slots. \n ");
//
//	}
//
//	// prints the list
//	static void executePrint() {
//		ArrayOfCirclesApp.printCirles(list);
//	}

}
