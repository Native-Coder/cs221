package chapter2;

import java.util.EmptyStackException;

/**
 * The linked-list based implementation of a stack. 
 */
public class LinkedStack<T> implements StackInterface<T> {

   static class LinkedNode<T> {
      T val;
      LinkedNode<T> next;

      public LinkedNode() {
         super();
      }

      public LinkedNode(T val, LinkedNode<T> next) {
         super();
         this.val = val;
         this.next = next;
      }

      public T getVal() {
         return val;
      }

      public void setVal(T val) {
         this.val = val;
      }

      public LinkedNode<T> getNext() {
         return next;
      }

      public void setNext(LinkedNode<T> next) {
         this.next = next;
      }
   }

   private LinkedNode<T> top;
   private int numOfElements;

   public LinkedStack() {
      top = null;
      numOfElements = 0;
   }

   @Override
   public void push(T element) {
      LinkedNode<T> newTop = new LinkedNode<T>();
      newTop.setVal(element);
      newTop.setNext(top);

      top = newTop;
      numOfElements++;
   }

   @Override
   public T pop() throws EmptyStackException {
      if (!isEmpty()) {
         T res = top.getVal();
         top = top.getNext();
         numOfElements--;
         return res;
      } else
         throw new EmptyStackException();
   }

   @Override
   public T top() throws EmptyStackException {
      if (!isEmpty()) {
         T res = top.getVal();

         return res;
      } else
         throw new EmptyStackException();

   }

   @Override
   public boolean isEmpty() {
      return top == null;
   }

   @Override
   public boolean isFull() {
      return false;
   }
   
   public int size() {
      return numOfElements;
   }
}
