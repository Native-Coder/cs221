package chapter2;

import java.util.EmptyStackException;

/**
 * Represents a last-in-first-out (LIFO) stack of objects.
 *
 */
public interface StackInterface<T> {
   /**
    * Push the element to the stack.
    * @param element the element to be pushed onto the stack.
    */
   void push(T element);

   /**
    * Removes the top element and returns it if the stack is not empty. 
    * @return the top if the stack is not empty.
    * @throws EmptyStackException when the stack is empty.
    */
   T pop() throws EmptyStackException;

   /**
    * Looks at the object at the top of this stack without removing it.
    * @return the top if the stack is not empty.
    * @throws EmptyStackException when the stack is empty.
    */
   T top() throws EmptyStackException;

   /**
    * @return true if the stack is empty, false otherwise.
    */
   boolean isEmpty();

   /**
    * Optional
    * @return true if the stack is full, false otherwise.
    */
   default boolean isFull() {
      return false;
   }
}
