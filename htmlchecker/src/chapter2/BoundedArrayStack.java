
package chapter2;

import java.util.EmptyStackException;

/**
 * The array based implementation of stack. 
 */
public class BoundedArrayStack<T> implements StackInterface<T> {
   protected static final int DEFAULT_CAPACITY = 4;
   protected T[] stack;
   protected int topIndex;

   public BoundedArrayStack() {
      this(DEFAULT_CAPACITY);
   }

   public BoundedArrayStack(int capacity) {
      topIndex = -1;
      stack = (T[]) new Object[capacity];
   }

   @Override
   public void push(T element) throws StackOverflowException {
      if (!isFull()) {
         topIndex++;
         stack[topIndex] = element;
      } else {
         throw new StackOverflowException("Stack is full");
      }
   }

   @Override
   public T pop() throws EmptyStackException {
      if (!isEmpty()) {
         T res = stack[topIndex];
         // null the entry
         stack[topIndex--] = null;

         return res;
      } else {
         throw new EmptyStackException();
      }
   }

   @Override
   public T top() throws EmptyStackException {
      if (!isEmpty()) {
         return stack[topIndex];
      } else
         throw new EmptyStackException();
   }

   @Override
   public boolean isEmpty() {
      return topIndex == -1;
   }

   @Override
   public boolean isFull() {
      return topIndex == stack.length - 1;
   }
   
   public int size() {
      return topIndex + 1;
   }
}
