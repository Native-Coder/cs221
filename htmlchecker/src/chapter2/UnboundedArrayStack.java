package chapter2;

public class UnboundedArrayStack<T> extends BoundedArrayStack<T> {

   /**
    *  Adds the specified element to the top of the stack, expanding
    *  the capacity of the array if necessary.
    */
   @Override
   public void push(T element) {
      if (isFull()) {
         expandCapacity();
      }
      topIndex++;
      stack[topIndex] = element;
   }

   private void expandCapacity() {
      // allocate a larger array
      T[] larger = (T[]) (new Object[stack.length * 2]);

      // copy the elements from the old array to the larger array
      for (int index = 0; index < stack.length; index++)
         larger[index] = stack[index];

      stack = larger;
   }
}