package htmlstuff;

import chapter2.LinkedStack;
import java.util.Scanner;

enum TagType{
	OPEN, CLOSE, NOTAG
}

/**
 * @author ty
 */
public class HTMLTest {
	
	int totalCount,
		openCount,
		closeCount,
		nonTagCount;
	
	LinkedStack<String> stack;
	
	/**
	 * constructor
	 */
	public HTMLTest(){
		this.totalCount  = 0; 
		this.openCount   = 0;
		this.closeCount  = 0;
		this.nonTagCount = 0;
		this.stack       = new LinkedStack<String>();
	}

	/**
	 * Checks an HTML string token by token and validates it
	 * @param html
	 * @return string
	 */
	public String checkHTML( String html ) {
		reset();
		Scanner reader = new Scanner( html );
		String token   = "";
		
		while( reader.hasNext() ) {

			// Increment our count as well as our string iterator
			totalCount++;
			token = reader.next();
			
			switch( tagType( token ) ) {
				case OPEN:
					stack.push( token );
					openCount++;
				break;
				
				case CLOSE:
					closeCount++;
					if( stack.isEmpty() )
						return "Error: Closing tag encountered and stack empty. " + this;
					
					if( match( token, stack.top() ) ) {
						stack.pop();
					} else {
						return "Error: Closing tag does not match opening tag. " + this;
					}
				break;
				
				case NOTAG:
					nonTagCount++;
				break;
			}
		}
		reader.close();
		
		if( stack.isEmpty() )
			return "Success. " + this;
		
		return "Error: Opening tag(s) left of stack. " + this;
	}
	
	/**
	 * determines if two tags match or not
	 * @param open An opening tag
	 * @param close a closing tag
	 * @return true of open is the correct corresponding closing tag to close
	 */
	public boolean match( String open, String close ) {
		return stripDelims( open ).toLowerCase().equals( stripDelims( close ).toLowerCase() );
	}
	

	/**
	 * Resets all the counters back to zero
	 */
	public void reset() {
		this.totalCount  = 0; 
		this.openCount   = 0;
		this.closeCount  = 0;
		this.nonTagCount = 0;
		this.stack       = new LinkedStack<String>();
	}
	

	/**
	 * strips the HTML tag delimiters from the input string
	 * @param tag - A string that has been verified as a tag using the isTag method
	 * @return The input string without the HTML tag delimiters
	 */
	public String stripDelims( String tag ){
		if( !isTag( tag ) ) return tag; 
		tag = tag.substring( 1 , tag.length() - 1 ); // Strip brackets 
		return ( tag.charAt( 0 ) == '/' ) ? tag.substring( 1 ) : tag ; // Strip end-tag delimiter '/'
	}
	
	/**
	 * Will return true if the input string in an opening or a closing html tag. 
	 * @param tag - any string
	 * @return True if the string is a "valid" HTML tag
	 */
	public boolean isTag(String str ) {
		// If the first character is a '<' and the last is a > then it is a "valid" HTML tag
		return ( str.length() > 2 && ( str.charAt( 0 ) == '<' && str.charAt( str.length() - 1 ) == '>' ) );
	}
	
	/**
	 * Determines if the string is an opening or a closing tag. Returns 0 for an opening tag, 1 for a closing tag, and -1 if it is not a tag
	 * @param tag - any string
	 * @return TagType Enumeration
	 */
	public TagType tagType( String str ) {
		if( isTag( str ) ) return ( str.substring( 0, 2 ).equals( "</" ) ) ? TagType.CLOSE : TagType.OPEN;
		return TagType.NOTAG;
	}
	
	/**
	 * Gives you the total number of tokens processed,
	 * followed by the number of, open tags, closing tags, and non tags; In that order.
	 * @see java.lang.Object#toString()
	 * @return A space-delimited string containing the number of tokens processed
	 */
	@Override
	public String toString() {
		return String.valueOf( totalCount  ) + ' ' +
			   String.valueOf( openCount   ) + ' ' +
			   String.valueOf( closeCount  ) + ' ' +
			   String.valueOf( nonTagCount ); 
	}
}



























