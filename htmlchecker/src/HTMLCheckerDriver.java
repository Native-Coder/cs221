
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import htmlstuff.HTMLTest;

public class HTMLCheckerDriver {
	
	public static void main(String[] args) throws FileNotFoundException {

	//	Scanner inData = new Scanner (System.in);
		
//		Scanner inData = new Scanner(new File("lab3advancedtester.in"));
		
//    	Scanner inData = new Scanner(new File("lab3htmlopeningtester.in"));
//		Scanner inData = new Scanner(new File("lab3htmlclosingtester.in"));
//		Scanner inData = new Scanner(new File("lab3htmlcorrecttester.in"));
//		Scanner inData = new Scanner(new File("lab3htmlmismatchtester.in"));
		Scanner inData = new Scanner(new File("lab3htmltester.in"));
//		Scanner inData = new Scanner(new File("lab3htmllargerthanlessthan.in"));


		HTMLTest test = new HTMLTest();

		while (inData.hasNext()) {
			// each line is one complete expression to be checked
			String page = inData.nextLine();
			String res = test.checkHTML(page);
			System.out.println( res);
		}
		
		
		inData.close();
	}





}
