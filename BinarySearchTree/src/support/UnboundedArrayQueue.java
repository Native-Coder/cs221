package support;

public class UnboundedArrayQueue<T> extends BoundedArrayQueue<T> {

	public UnboundedArrayQueue() {
		super( DEFAULT_CAPACITY );
	}

	public UnboundedArrayQueue( int size ) {
		super( size );
	}

	@Override
	public void enqueue( T element ) throws QueueOverflowException {
		if (isFull()) {
			expand();
		}
		rear = ( rear + 1 ) % elements.length;
		elements[rear] = element;
		numOfElements++;
	}

	private void expand() {
		T[] newArray = (T[]) new Object[numOfElements * 2];

		// copy all the elements to the correct location
		for (int i = 0; i < numOfElements; i++)
			newArray[i] = elements[( front + i ) % numOfElements];

		elements = newArray;
		front = 0;
		rear = numOfElements - 1;
	}

}
