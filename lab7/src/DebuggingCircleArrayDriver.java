import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import circlesupport.Circle;
import circlesupport.CircleComp;
import lab7.CircleArrayCode;
import lab7.InsertionSort;

public class DebuggingCircleArrayDriver {


	public static void main(String[] args) throws FileNotFoundException {
		
//		Scanner inData = new Scanner (System.in);
		
//		Scanner inData = new Scanner (new File ( "circlefile1.txt"));
//		Scanner inData = new Scanner (new File ( "circlefile2.txt"));
//		Scanner inData = new Scanner (new File ( "circlefile3.txt"));
		Scanner inData = new Scanner (new File ( "circlefile4.txt"));
//		Scanner inData = new Scanner (new File ( "circlefile5.txt"));
//		Scanner inData = new Scanner (new File ( "circlefile6.txt"));
//		Scanner inData = new Scanner (new File ( "circlefile7.txt"));
		
		testCircleCode(inData);
	}

	
	public static void testCircleCode(Scanner inData) {

		CircleComp[] test = new CircleComp[7];

		// load the array
		int numOfElements = CircleArrayCode.loadCircleArray(test, inData);

		// print the array
		System.out.println("The loaded array:");
		CircleArrayCode.printCircleArray(test, numOfElements);

		// find the circle with a given label
		System.out.println("Trying to find the circle with label \"circ5\"");
		String label = "circ5";
		Circle temp = CircleArrayCode.findLabeledCircle(test, numOfElements, label);
		System.out.println("\n TEST 1a: Circle with label " + label + " is " + temp);

		// sort the array
		System.out.println("\n TEST 2: sorting based on radius (from small to large)");
		InsertionSort.sort(test, numOfElements);

		// print it again - now sorted
		CircleArrayCode.printCircleArray(test, numOfElements);
		
		// find the circle with a given label
		System.out.println("Trying to find the circle with label \"circ5\" again");
		label = "circ5";
		temp = CircleArrayCode.findLabeledCircle(test,numOfElements, label);
		System.out.println("\n TEST 1b: Circle with label " + label + " is " + temp);

		numOfElements= testRemove (test, numOfElements);
	}
	
	// remove two elements from the array and print to check results
	private static int testRemove(CircleComp [] array, int size ) {
		// remove an element
		System.out.println("Trying to remove the circle with label \"circX\" ");
		String label = "circX";
		boolean res = CircleArrayCode.removeLabeledCircle(array,size, label);
		if (res)
			size--;
		System.out.println("\n TEST 3a: Circle with label " + label + " is removed :" + res);

		// print it again - now without circX
		CircleArrayCode.printCircleArray(array, size);

		
		// remove another element
		System.out.println("Trying to remove the circle with label \"circY\" ");
		 label = "circY";
		 res = CircleArrayCode.removeLabeledCircle(array,size, label);
		if (res)
			size--;
		System.out.println("\n TEST 3b: Circle with label " + label + " is removed :" + res);
		
		// print it again - now without circX
		CircleArrayCode.printCircleArray(array, size);
		
		return size;
	}


}