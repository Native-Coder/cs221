package circlesupport;

import java.util.Arrays;

public class Circle {



	protected double radius;
	protected String label;
	protected double[] center;
	protected int ID;
	static int counter=0;

	/**
	 * @param radius
	 * @param label
	 * @param center
	 */
	public Circle(double radius, String label, double[] center) {
		super();
		this.radius = radius;
		this.label = label;
		this.center = center;
		ID = counter++;
	}

	public Circle(double radius, String label, double xPt, double yPt) {
		super();
		this.radius = radius;
		this.label = label;
		this.center = new double[2];
		this.center[0] = xPt;
		this.center[1] = yPt;
		ID =counter++;
	}

	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * @return the center
	 */
	public double[] getCenter() {
		return center;
	}

	/**
	 * @param center
	 *            the center to set
	 */
	public void setCenter(double[] center) {
		this.center = center;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	
	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", label=" + label + ", center=" + Arrays.toString(center) + ", ID=" + ID
				+ "]";
	}

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Circle other = (Circle) obj;
		if (Double.doubleToLongBits(radius) != Double.doubleToLongBits(other.radius))
			return false;
		return true;
	}


}
