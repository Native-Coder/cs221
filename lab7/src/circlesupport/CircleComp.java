/**
 * 
 */
package circlesupport;

import java.util.Random;

/**
 * @author utz46243
 *
 */
public class CircleComp extends Circle implements Comparable<CircleComp> {

	static Random rand = new Random();

	/**
	 * @param radius
	 * @param label
	 * @param center
	 */
	public CircleComp( double radius, String label, double[] center ) {
		super( radius, label, center );
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param radius
	 * @param label
	 * @param xPt
	 * @param yPt
	 */
	public CircleComp( double radius, String label, double xPt, double yPt ) {
		super( radius, label, xPt, yPt );
		// TODO Auto-generated constructor stub
	}

	public int compareTo( CircleComp other ) {
		if ( other == null )
			return Integer.MIN_VALUE;

		return (int)( Math.round( this.getRadius() ) - Math.round( other.getRadius() ) );
	}

}
