package lab7;

public class InsertionSort {

	/**
	 * sorts the given array
	 * @param elements the array to sort
	 * @param size the number of elements in the array
	 */
	public static <T extends Comparable<T>> void sort( T[] elements, int size ) {
		// start from the second element, insert each element
		for (int i = 1; i < elements.length; i++) {
			insert( elements, i );
		}
	}

	/**
	 * inserts the ith element at the correct position
	 * @param elements array of elements
	 * @param i index representing the first value in the unsorted portion of the array
	 */
	private static <T extends Comparable<T>> void insert( T[] elements, int i ) {
		// No need to process null elements
		if( elements[ i ] == null )
			return;
		// store our target for later
		T target = elements[i];
		i--;
		// as long as the target element is smaller
		while ( i >= 0 && target.compareTo( elements[i] ) < 0 ) {
			// move the element to the right			
			elements[ i + 1 ] = elements[ i ];
			i--;
		}

		// i + 1 is the location for insertion
		elements[i + 1] = target;
	}
}
