package lab7;

import java.util.Scanner;

import circlesupport.Circle;
import circlesupport.CircleComp;

public class CircleArrayCode {

	/**
	 * takes an array of circles and prints it
	 * 
	 * @param circles the array of circles
	 * @param size the size of the array (not necessary)
	 */
	static public void printCircleArray( Circle[] circles, int size ) {
		for ( int i = 0; i < circles.length && circles[ i ] != null; i++ ) {
			System.out.print( i + ": " );
			System.out.println( circles[ i ] );
		}
	}

	/**
	 * finds the circle in the given array with the given label and returns its
	 * index
	 * 
	 * @param array the array to search
	 * @param size the number of elements in the array(no longer necessary)
	 * @param label the label to search for
	 * @return
	 */
	static public int findCircleIndex( Circle[] array, int size, String label ) {
		// Assume that the circle is not in the array
		int index = -1;

		/*
		 * Attempt to loop through every element in the array. I think it's safe to stop
		 * the first time we encounter a null element because arrays are expected to be
		 * contiguous
		 */
		for ( int i = 0; i < array.length && array[ i ] != null; i++ ) {
			// If we find the element...
			if ( array[ i ].getLabel().equals( label ) ) {
				// Overwrite the index with the value of i, then break the loop, we are done
				index = i;
				break;
			}
		}

		// return the correct index
		return index;

	}

	/**
	 * attempts to find a circle in the given array with the given label
	 * 
	 * @param array the array to search
	 * @param size The number of elements in the array (no longer necessary)
	 * @param label the label to search for
	 * @return The first circle with the given array, or null if it wasn't found
	 */
	static public Circle findLabeledCircle( Circle[] array, int size, String label ) {
		int index = findCircleIndex( array, size, label );
		return ( index == -1 ) ? null : array[ index ];

	}

	/**
	 * Attempts to find the circle with the given label in the given array and
	 * remove it
	 * 
	 * @param array the array to search
	 * @param size the number of elements in the array (no longer necessary)
	 * @param label the label to search for
	 * @return true if the circle was found and removed, false otherwise
	 */
	static public boolean removeLabeledCircle( Circle[] array, int size, String label ) {
		int index = findCircleIndex( array, size, label );
		// If we can't find the circle then we can't remove it
		if ( index == -1 )
			return false;

		// Start at the index of the circle to be removed...
		int i = index;
		// move all elements it it's right, over by one. This will overwrite the value
		while ( ( i + 1 ) < size ) {
			array[ i ] = array[ ++i ];
		}
		/*
		 * Make sure to set the last index that we manipulated to null, otherwise we
		 * will just duplicate the last value in the array
		 */
		array[ i ] = null;

		return true;
	}

	/**
	 * loads a circle array from the provided file scanner
	 * 
	 * @param circleArray the array in which to store the circles
	 * @param inData the scanner to use to read the input file
	 * @return an array of circles created with data from the file pointed to by
	 *         inData
	 */
	static public int loadCircleArray( CircleComp[] circleArray, Scanner inData ) {
		// get info about number of circles to be used

		// load each circle
		int size = 0;
		while ( inData.hasNext() && size < circleArray.length ) {
			// Create a new Circle and add it to the array
			circleArray[ size++ ] = new CircleComp(
				inData.nextDouble(), // radius
				inData.next(),	     // label
				inData.nextDouble(), // x-center
				inData.nextDouble()  // y-center
			);
		}

		return size;
	}

}