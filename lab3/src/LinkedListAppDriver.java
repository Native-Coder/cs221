import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import lab3.LinkedListApp;
import lab3.LinkedListApp.LNode;

public class LinkedListAppDriver {

	private static LinkedListApp.LNode head = null;

	private static final String createNewList = "createNewList";
	private static final String insertFrist = "insertFirst";
	private static final String printList = "printList";
	private static final String removeFirst = "removeFirst";
	private static final String getFirst = "getFirst";
	private static final String sumTraversal = "sumTraversal";
	private static final String avgTraversal = "avgTraversal";

	private static final String quit = "quit";

	private static String choiceString(String choice) {
		switch (choice) {
		case avgTraversal:
			return "\n* Testing computeAverageTraversal";
		case insertFrist:
			return "\n* Testing insertFirst";
		case removeFirst:
			return "\n* Testing removeFirst";
		case getFirst:
			return "\n* Testing getFirst ";
		case sumTraversal:
			return "\n* Calling computeSumTraversal ";
		case createNewList:
			return "\n* Setup for next testing phase: Create a new list";
		case printList:
			return "\n* ListInfo: calling printTraversal";
		case quit:
			return "quit";
		default:
			return "nothing";
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		// read data from re-directed file
		// Scanner inData = new Scanner(System.in);

//		Scanner inData = new Scanner(new File("testLinkedinsert.in"));
//		Scanner inData = new Scanner(new File("testLinkedremove.in"));
		Scanner inData = new Scanner(new File("testLinkedget.in"));
//		Scanner inData = new Scanner(new File("testLinkedavg.in"));

		String choice;
		do {
			choice = getNextChoice(inData);
			String info = choiceString(choice);
			if (!info.equals(""))
				System.out.println(info);
			if (!choice.equals(quit)) 
				executeChoice(choice, inData);
		} while (!choice.equals(quit));

	}
	
	// ask the user for what s/he wants to do
	private static String getNextChoice(Scanner inData) {
		String choice = null;
		choice = inData.next();
		inData.nextLine();
		return choice;
	}
	// Determine what the user wants to do and execute the appropriate method
	static void executeChoice(String choice, Scanner inData) {
		switch (choice) {
		case insertFrist:
			executeInsert(inData);
			break;
		case removeFirst:
			executeDelete();
			break;
		case createNewList:
			executeCreate();
			break;
		case getFirst:
			executeGet();
			break;
		case sumTraversal:
			executeSumTraversal();
			break;
		case avgTraversal:
			executeAvgTraversal();
			break;
		case printList:
			executePrint();
			break;

		}
		// executePrint(testA, size);
	}


	// perform the necessary update to complete the insert first
	static void executeInsert(Scanner inData) {
		LinkedListApp.LNode keepHead = head;
		int temp = inData.nextInt();
		head = LinkedListApp.insertFirst(head,temp);
		String notStr = (head != keepHead)? "" : "not";
		System.out.println("Element " + temp+ " was " + notStr + " inserted.");
	}

	// perform the necessary updates to perform the delete
	static void executeDelete() {
		LinkedListApp.LNode keepHead = head;
		head = LinkedListApp.removeFirst(head);
		if (keepHead == null && head == null) {
			System.out.println("Empty list - nothing was removed");
			return;
		}
		String notStr = (head != keepHead)? "" : "not";
		System.out.println("Element " + keepHead.getVal()+ " was " + notStr + " removed.");
	}
	
	// perform the necessary updates to perform the delete
	static void executeGet() {
		LinkedListApp.LNode keepHead = head;
		Integer res = LinkedListApp.getFirst(head);
		if (keepHead == null && res == null) {
			System.out.println("Empty list - nothing was get");
			return;
		}
		System.out.println("Element " + res+ " was  returned.");
	}


	// call the computeSumTraversal method and display the result
	static void executeSumTraversal() {
		
		int sum = LinkedListApp.computeSumTraversal(head);
		System.out.println("computeSumTraversal returns " + sum);
	}
	
	// call the computeAvgTraversal method and display the result
	static void executeAvgTraversal() {
		double average = LinkedListApp.computeAverageTraversal(head);
		System.out.println("computeAvgTraversal returns " + average);
	}


	// create a new rectangle array
	static void executeCreate() {
		// how much elements should the array hold?
		head = null;

		System.out.println("Created empty linked list. \n ");

	}

	// prints the list
	static void executePrint() {
		LinkedListApp.printTraversal(head);
	}

}
