package lab3;

// this is a demo for the LLNode class - you can see what happens using 
// the visualization tool on Vlab (right next to the test cases button)
public class LinkedListApp {

	public static class LNode {
		private Integer val; // information stored in each node
		private LNode next; // the link to the next node

		// constructor
		public LNode(Integer info) {
			this(info, null);
		}
		
		// constructor
		public LNode() {
			this(0, null);
		}
		
		// constructor
		public LNode(Integer val, LNode next) {
			super();
			this.val = val;
			this.next = next;
			next = null;
		}

		// transformer for info
		public void setVal(Integer info)
		// Sets info of this LLStringNode.
		{
			this.val = info;
		}

		// observer for info
		public Integer getVal()
		// Returns info of this LLStringNode.
		{
			return val;
		}

		// transformer for link
		public void setNext(LNode link)
		// Sets link of this LLStringNode.
		{
			this.next = link;
		}

		// observer for link
		public LNode getNext()
		// Returns link of this LLStringNode.
		{
			return next;
		}
	}

	// short program to demo traveral of a list
	public static void main(String arg[]) {
		// creating a linked list with three elements
		LNode list = null;
		list = insertFirst(list, 20);
		list = insertFirst(list, 30);
		list = insertFirst(list, 40);

		// display the list - from first to last element
		printTraversal(list);

		// sum the value in the linked list and display it
		int sum = computeSumTraversal(list);
		System.out.println("The sum of the elements in the list is " + sum + ".");

		// average the positive values in the linked list and display it
		double avg = computeAverageTraversal(list);
		System.out.println("The average of the positive elements in the list is " + avg + ".");

		list = insertFirst(list, -20);
		list = insertFirst(list, 52);
		list = insertFirst(list, -40);

		// display the list - from first to last element
		printTraversal(list);

		// sum the value in the linked list and display it
		sum = computeSumTraversal(list);
		System.out.println("The sum of the elements in the list is " + sum + ".");

		// average the positive values in the linked list and display it
		avg = computeAverageTraversal(list);
		System.out.println("The average of the positive elements in the list is " + avg + ".");

	}

	/**
	 * add a new element at the beginning of the linked list
	 * 
	 * @param head    The pointer to the first element in the linked list
	 * @param element The element to be added
	 * @return The pointer to the first element of the updated linked list.
	 */
	public static LNode insertFirst(LNode head, Integer element) {
		LNode newHead = new LNode( element );
		newHead.setNext( head );
		return newHead;
	}

	/**
	 * Remove the element at the beginning of the linked list. If the list is empty
	 * the function returns null.
	 * 
	 * @param head The pointer to the first element in the linked list
	 * @return The pointer to the first element of the updated linked list.
	 */
	public static LNode removeFirst(LNode head) {
		return ( head == null ) ? null : head.getNext();
	}

	/**
	 * Get the value in the first element of the linked list. If the list is empty
	 * the function returns null.
	 * 
	 * @param head The pointer to the first element in the linked list
	 * @return The pointer to the first element of the updated linked list.
	 */
	public static Integer getFirst(LNode head) {
		return ( head == null ) ? null : head.getVal();
	}

	/**
	 * Traverse the linked list and compute the average of the positive elements in
	 * the list
	 * 
	 * @param numberLst The pointer to the first element in the linked list
	 * @return The average of the values of the positive elements in the linked
	 *         list.
	 * 
	 */
	public static double computeAverageTraversal( LNode link ) {
		if( link == null ) return 0;
		// traversing the linked list - to create the average of the ints
		double avg = 0;
		int count  = 0;
		while( link != null ) {
			if( link.getVal() >= 0 ) {
				count++;
				avg += link.getVal();
			}
			link = link.getNext();
		}
		return avg / count;
	}

	/**
	 * Traverse the linked list and compute the sum of the elements
	 * 
	 * @param numberLst The pointer to the first element in the linked list
	 * @return The sum of the values of all the elements in the linked list.
	 * 
	 */
	public static Integer computeSumTraversal( LNode link ) {
		// traversing the linked list - to create the sum of the ints
		LNode temp = link;
		Integer sum = 0;
		 while ( temp != null ){
			sum += temp.getVal();
			temp = temp.getNext();
		};
		return sum;
	
	}

	/**
	 * Traverse the linked list and print each element
	 * 
	 * @param numberLst The pointer to the first element in the linked list
	 */
	public static void printTraversal(LNode numberLst) {
		System.out.println("The info in the list is as follows:");
		// traversing the linked list
		LNode temp = numberLst;
		while (temp != null) {
			int info = temp.getVal();
			System.out.println(info);
			temp = temp.getNext();
		}
		System.out.println("Done");

	}

}
