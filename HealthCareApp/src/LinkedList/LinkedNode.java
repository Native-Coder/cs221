package LinkedList;

public class LinkedNode<T> {		
	// value stored in each element
	private T val;
	// link to the next node in the
	// linked list
	private LinkedNode next;
	
	/**
	 * @param val
	 * @param next
	 */
	public LinkedNode( T val, LinkedNode next ) {
		super();
		this.val  = val;
		this.next = next;
	}
	
	/**
	 * @param val
	 */
	public LinkedNode( T val ) {
		super();
		this.val = val;
	}

	/**
	 * @return the val
	 */
	public T getVal() {
		return val;
	}

	/**
	 * @param val the val to set
	 */
	public void setVal( T val ) {
		this.val = val;
	}

	/**
	 * @return the next
	 */
	public LinkedNode getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext( LinkedNode next ) {
		this.next = next;
	}
}
