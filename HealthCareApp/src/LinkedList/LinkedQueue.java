package LinkedList;

import java.util.NoSuchElementException;

/**
 * 
 * @author Ty Meador <Native-Coder>
 *
 * @param <T>
 */
public class LinkedQueue<T> implements QueueInterface<T> {
	Integer size; 
	LinkedNode<T> front, rear;
	
	// REQUIRED: add a constructor without any parameters
	public LinkedQueue(){
		this.size = 0;
		this.front = null;
	}
	
	/**
	 * @return true if the queue is empty, false otherwise.
	 */
	public boolean isEmpty() {
		return ( front == null );
	}

	/**
	 * @return the number of elements in the queue.
	 */
	public int size() {
		return this.size;
	}
	
	public void insert( T val ) {
		enqueue( val );
	}
	
	/**
	 * Put the element at the rear of the queue if it is not full. Throws
	 * QueueOverflowException if the queue is full.
	 * @param element  The value to be enqueued.
	 */
	public void enqueue( T val ){
		
		this.size++;
		LinkedNode<T> newFront;
		// If the list is empty...
		if( this.front == null ) {
			// Create a new front node
			newFront = new LinkedNode<T>( val );
			newFront.setNext( newFront );
			// Set the rear node
			this.rear = newFront;
		} else {
			newFront = new LinkedNode<T>( val, this.front.getNext() );
			this.front.setNext( newFront );
		}
		this.front = newFront;
	}

	/**
	 * Removes the head element and returns it if the queue is not empty.
	 * 
	 * @return the head if the queue is not empty.
	 * @throws NoSuchElementException when the queue is empty.
	 */
	public T dequeue() {
		if( this.front == null ) throw new NoSuchElementException();
		LinkedNode<T> next = this.front.getNext();
		
		if( next.equals( this.front ) )
			this.front = null;
		else
			this.front.setNext( next.getNext() );
		
		this.size--;
		return next.getVal();
	}
	
	
	/**
	 * Returns the first element in the queue, without dequeueing it
	 * @return T
	 */
	public LinkedNode<T> peekFirst() {
		return this.front;
	}
	
	/**
	 * 
	 * Returns the last element in the queue, without dequeueing it
	 * @return T
	 */
	public T peekLast() {
		return this.rear.getVal();
	}
	
}
