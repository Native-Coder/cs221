package HospitalDatabase;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import LinkedList.*;

/**
 * 
 * @author Ty Meador <Native-Coder>
 *
 */
public class HospitalDatabase {
	// The datasets we need
	private LinkedQueue < Diagnosis > diagnoses;
	private LinkedQueue < BillingRecord > records;

	// Diagnosis indexes
	private DatabaseSearchTree < String, LinkedNode < Diagnosis > > diagnosesByName;
	private DatabaseSearchTree < Double, LinkedNode < Diagnosis > > diagnosesByCost;
	private DatabaseSearchTree < Double, LinkedNode < Diagnosis > > diagnosesByDiscount;

	// Record indexes
	private DatabaseSearchTree < Double, LinkedNode < BillingRecord > > recordsByCost;
	private DatabaseSearchTree < String, LinkedNode < BillingRecord > > recordsByPatient;
	private DatabaseSearchTree < String, LinkedNode < BillingRecord > > recordsByHospital;
	private DatabaseSearchTree < String, LinkedNode < BillingRecord > > recordsByAdmission;
	private DatabaseSearchTree < Diagnosis, LinkedNode < BillingRecord > > recordsByDiagnosis;
	private DatabaseSearchTree < Integer, LinkedNode < BillingRecord > > recordsByLengthOfStay;

	public ArrayList < Diagnosis > listDiagnoses() {
		return extractDiagnoses( diagnosesByName );
	}

	/**
	 * Default constructor
	 */
	HospitalDatabase() {

		reset();

	}

	/**
	 * Scanner constructor. You can use one scanner, passing null for the other. Or
	 * use both scanners at the same time
	 * 
	 * @param inDiagnoses
	 * @param inRecords
	 * @throws Exception
	 */
	public HospitalDatabase( Scanner inDiagnoses, Scanner inRecords ) {

		reset();
		if ( inDiagnoses != null )
			loadDiagnosesFromScanner( inDiagnoses );
		if ( inRecords != null )
			loadRecordsFromScanner( inRecords );

	}

	/**
	 * Removes everything from the database
	 */
	public void dropAll() {

		reset();

	}

	/**
	 * Loads defined diagnoses from the provided scanner
	 * 
	 * @param inData
	 */
	public void loadDiagnosesFromScanner( Scanner inData ) {

		if ( inData != null ) {

			while ( inData.hasNext() ) {

				// Create the diagnosis object
				addDiagnoses( new Diagnosis( inData.next(), // name
						inData.nextDouble(), // cost
						inData.nextDouble() // discount
				) );

			}

		}

	}

	/**
	 * Loads billing records from the provided scanner
	 * 
	 * @param inData
	 */
	public void loadRecordsFromScanner( Scanner inData ) {

		if ( inData != null ) {

			while ( inData.hasNext() ) {

				// Add the bill to the database
				addBill( new BillingRecord( inData.next(), // hospital ID
						inData.next(), // admission ID
						inData.next(), // patient ID
						diagnosesByName.find( inData.next() ).getVal(), // diagnosis
						inData.nextInt(), // days treated
						inData.nextBoolean(), // medicaid recipient
						inData.nextDouble() // bill total
				) );

			}

		}

	}

	/**
	 * Adds a billing record to the database
	 * 
	 * @param newBill
	 */
	public void addBill( BillingRecord newBill ) {

		// Add the record to the dataset
		this.records.insert( newBill );

		/**
		 * In theory, by using peekFirst every time, we are storing a reference to the
		 * original dataset. This should save tons of memory
		 */
		recordsByHospital.insert( newBill.getHospitalId(), this.records.peekFirst() );

		recordsByPatient.insert( newBill.getPatientId(), this.records.peekFirst() );
		recordsByAdmission.insert( newBill.getAdmissionId(), this.records.peekFirst() );
		recordsByCost.insert( newBill.getBillTotal(), this.records.peekFirst() );
		recordsByDiagnosis.insert( newBill.getDiagnosis(), this.records.peekFirst() );
		recordsByLengthOfStay.insert( newBill.getDaysTreated(), this.records.peekFirst() );

	}

	/**
	 * adds a diagnosis to the database
	 * 
	 * @param newDiagnosis
	 */
	public void addDiagnoses( Diagnosis diagnosis ) {

		// Do nothing if the diagnosis is null
		if ( diagnosis == null || this.diagnoses == null )
			return;

		if ( this.diagnoses == null )
			resetDiagnoses();

		// Add the diagnosis to the dataset
		this.diagnoses.insert( diagnosis );

		// Index the dataset in lots of useful ways
		diagnosesByName.insert( diagnosis.getName(), this.diagnoses.peekFirst() );
		diagnosesByDiscount.insert( diagnosis.getMedicaidDiscount(), this.diagnoses.peekFirst() );
		diagnosesByCost.insert( diagnosis.getPerDayCost(), this.diagnoses.peekFirst() );

	}

	public void reset() {

		resetDiagnoses();
		resetBillingRecords();

	}

	public void resetDiagnoses() {

		this.diagnoses = new LinkedQueue < Diagnosis >();

		// Diagnoses indexes
		diagnosesByName = new DatabaseSearchTree < String, LinkedNode < Diagnosis > >();
		diagnosesByDiscount = new DatabaseSearchTree < Double, LinkedNode < Diagnosis > >();
		diagnosesByCost = new DatabaseSearchTree < Double, LinkedNode < Diagnosis > >();

	}

	public void resetBillingRecords() {

		this.records = new LinkedQueue < BillingRecord >();

		// Record indexes
		recordsByHospital = new DatabaseSearchTree < String, LinkedNode < BillingRecord > >();
		recordsByPatient = new DatabaseSearchTree < String, LinkedNode < BillingRecord > >();
		recordsByAdmission = new DatabaseSearchTree < String, LinkedNode < BillingRecord > >();
		recordsByCost = new DatabaseSearchTree < Double, LinkedNode < BillingRecord > >();
		recordsByLengthOfStay = new DatabaseSearchTree < Integer, LinkedNode < BillingRecord > >();
		recordsByDiagnosis = new DatabaseSearchTree < Diagnosis, LinkedNode < BillingRecord > >();

	}

	/**
	 * Queries the database for records with the matching hospital name
	 * 
	 * @param hospitalName
	 * @return
	 */
	public ArrayList < BillingRecord > recordsByHospital( String hospitalName ) {

		return extractBillRecords( recordsByHospital.findAll( hospitalName ) );

	}

	/**
	 * Queries the database for records with the matching patientID
	 * 
	 * @param patientID
	 * @return
	 */
	public ArrayList < BillingRecord > recordsByPatient( String patientID ) {

		return extractBillRecords( recordsByPatient.findAll( patientID ) );

	}

	/**
	 * Queries the database for records with the matching admission ID
	 * 
	 * @param admissionID
	 * @return
	 */
	public ArrayList < BillingRecord > recordsByAdmission( String admissionID ) {

		return extractBillRecords( recordsByAdmission.findAll( admissionID ) );

	}

	/**
	 * Returns all bills whos diagnosis matches the given string. This is a cool
	 * function, because it is the essences of table join.
	 * 
	 * @param diagnosisName
	 * @return
	 */
	public ArrayList < BillingRecord > recordsByDiagnosis( String diagnosisName ) {

		LinkedNode < Diagnosis > node = diagnosesByName.find( diagnosisName );
		
		if( node != null ) {
			// Get a reference to the correct diagnosis
			Diagnosis diagnosis = node.getVal();
			// Use that diagnosis to query the records list
			return extractBillRecords( recordsByDiagnosis.findAll( diagnosis ) );
		}
		
		return null;
	}

	public ArrayList < BillingRecord > recordsByDiagnosis( Diagnosis diag ) {
		// Use that diagnosis to query the records list
		return extractBillRecords( recordsByDiagnosis.findAll( diag ) );

	}

	/**
	 * Returns a list of records between min and max total billed
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public ArrayList < BillingRecord > recordsByCost( double min, double max ) {

		// Iterate over the entire BST
		Iterator < LinkedNode < BillingRecord > > itr = recordsByCost.iterator();
		ArrayList < BillingRecord > list = new ArrayList < BillingRecord >();
		BillingRecord next;

		// Iterate over the BST
		while ( itr.hasNext() ) {

			next = itr.next().getVal();
			// Break the loop once we have reached the max
			if ( next.getBillTotal() > max )
				break;

			// Start adding elements once we are at the minimum
			if ( next.getBillTotal() >= min ) {

				list.add( next );

			}

		}

		// Rtrn the list of records
		return list;

	}

	/**
	 * Returns the diagnosis object in the database that matches the given ID. Null
	 * if it doesn't exist
	 * 
	 * @param diagnosisID
	 * @return
	 */
	public Diagnosis diagnosesByName( String diagnosisID ) {
		return this.diagnosesByName.find( diagnosisID ).getVal();
	}

	/**
	 * Lists the first record of each hospital
	 * 
	 * @return
	 */
	public ArrayList < String > listHospitals() {
		return recordsByHospital.listKeys();
	}

	/**
	 * Returns a list of bills in ascending order
	 * 
	 * @return
	 */
	public ArrayList < BillingRecord > dumpBills() {
		// Create a list of proper size to store the return value
		ArrayList < BillingRecord > list = new ArrayList < BillingRecord >( this.recordsByCost.size() );
		// Iterate over the entire tree
		Iterator < LinkedNode < BillingRecord > > itr = this.recordsByCost.iterator();

		// While we can , add the value of the linked node to the list
		while ( itr.hasNext() ) {

			list.add( itr.next().getVal() );

		}
		return list;
	}

	/**
	 * Returns the number of BillingRecords loaded into the datbase
	 * 
	 * @return
	 */
	public int getBillingRecordCount() {
		// The size of any one of our BSTs will be the number of records loaded
		return recordsByDiagnosis.size();
	}

	/**
	 * Returns the number of unique patientIDs
	 * @return
	 */
	public int getPatientCount() {
		return recordsByPatient.keyCount();
	}
	
	/**
	 * Returns the number of unique hospital IDs in the database
	 * @return
	 */
	public int getHospitalCount() {
		return recordsByHospital.keyCount();
	}
	
	/**
	 * Returns the number of unique admission ids
	 * @return
	 */
	public int getUniqueAdmissioncount(){
		return recordsByAdmission.keyCount();
	}
	
	/**
	 * Returns the number of diagnoses loaded in the database
	 * @return
	 */
	public int getDiagnosesCount() {
		return this.diagnoses.size();
	}
	
	/**
	 * Extracts the BillingRecord from the main dataset
	 * 
	 * @param list
	 * @return
	 */
	private ArrayList < BillingRecord > extractBillRecords( Iterable < LinkedNode < BillingRecord > > list ) {

		// Iterate over the list
		Iterator < LinkedNode < BillingRecord > > itr = list.iterator();
		// Create a new list to store the BillingRecords
		ArrayList < BillingRecord > rtrn = new ArrayList < BillingRecord >();

		// Extract the BillingRecord from the node and store it in the new list
		while ( itr.hasNext() ) {

			rtrn.add( itr.next().getVal() );

		}

		return rtrn;

	}

	private ArrayList < Diagnosis > extractDiagnoses( Iterable < LinkedNode < Diagnosis > > list ) {

		// Iterate over the list
		Iterator < LinkedNode < Diagnosis > > itr = list.iterator();
		// Create a new list to store the BillingRecords
		ArrayList < Diagnosis > rtrn = new ArrayList < Diagnosis >();

		// Extract the BillingRecord from the node and store it in the new list
		while ( itr.hasNext() ) {

			rtrn.add( itr.next().getVal() );

		}

		return rtrn;

	}

}


















































