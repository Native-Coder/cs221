package HospitalDatabase;
import HospitalDatabase.Diagnosis;

public class BillingRecord {
	public String getAdmissionId() {
		return admissionId;
	}

	public void setAdmissionId( String admissionId ) {
		this.admissionId = admissionId;
	}

	private static int recordsLoaded = 0;
	String hospitalId, patientId, admissionId;
	Diagnosis diagnosis;
	Integer daysTreated;
	boolean medicaidRecipient;
	double billTotal;

	/**
	 * default null-fields constructor
	 */
	BillingRecord() {
		this(
			"",    // hospital ID
			"",    // admission ID
			"",    // patient ID
			null,  // Diagnosis
			0,     // days treated
			false, // medicaid recipient
			0.0    // bill total
		);
	}

	/**
	 * Instantiation constructor
	 * @param hospitalId
	 * @param admissionId
	 * @param patientId
	 * @param diagnosis
	 * @param daysTreated
	 * @param medicaidRecipient
	 * @param billTotal
	 */
	BillingRecord(
		String hospitalId,
		String admissionId,
		String patientId,
		Diagnosis diagnosis,
		int daysTreated,
		boolean medicaidRecipient,
		double billTotal
	) {
		this.hospitalId        = hospitalId;
		this.admissionId       = admissionId;
		this.patientId         = patientId;
		this.diagnosis         = diagnosis;
		this.daysTreated       = daysTreated;
		this.medicaidRecipient = medicaidRecipient;
		this.billTotal         = billTotal;
		recordsLoaded++;
	}

	/**
	 * Copy constructor
	 * 
	 * @param copy
	 */
	BillingRecord( BillingRecord copy ) {
		this.hospitalId        = copy.hospitalId;
		this.admissionId       = copy.admissionId;
		this.patientId         = copy.patientId;
		this.diagnosis         = copy.diagnosis;
		this.daysTreated       = copy.daysTreated;
		this.medicaidRecipient = copy.medicaidRecipient;
		this.billTotal         = copy.billTotal;
	}

	public String getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId( String hospitalId ) {
		this.hospitalId = hospitalId;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId( String patientId ) {
		this.patientId = patientId;
	}

	public Diagnosis getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis( Diagnosis diagnosis ) {
		this.diagnosis = diagnosis;
	}

	public Integer getDaysTreated() {
		return daysTreated;
	}

	public void setDaysTreated( Integer daysTreated ) {
		this.daysTreated = daysTreated;
	}

	public boolean isMedicaidRecipient() {
		return medicaidRecipient;
	}

	public void setMedicaidRecipient( boolean medicaidRecipient ) {
		this.medicaidRecipient = medicaidRecipient;
	}

	public double getBillTotal() {
		return billTotal;
	}

	public void setBillTotal( double billTotal ) {
		this.billTotal = billTotal;
	}

	@Override
	public String toString() {

		return "BillingRecord [hospitalId=" + hospitalId + ", patientId=" + patientId + ", admissionId=" + admissionId
				+ ", diagnosis=" + diagnosis + ", daysTreated=" + daysTreated + ", medicaidRecipient="
				+ medicaidRecipient + ", billTotal=" + billTotal + "]";

	}
	

}