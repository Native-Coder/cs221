package HospitalDatabase;

public class Diagnosis implements Comparable {
	String name;
	double perDayCost;
	double medicaidDiscount;

	Diagnosis( String name, double cost, double discount ) {
		this.name = name;
		this.perDayCost = cost;
		this.medicaidDiscount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public double getPerDayCost() {
		return perDayCost;
	}

	public void setPerDayCost( double perDayCost ) {
		this.perDayCost = perDayCost;
	}

	public double getMedicaidDiscount() {
		return medicaidDiscount;
	}

	public void setMedicaidDiscount( double medicaidDiscount ) {
		this.medicaidDiscount = medicaidDiscount;
	}

	@Override
	public String toString() {
		return "Diagnosis [name=" + name + ", perDayCost=" + perDayCost + ", medicaidDiscount=" + medicaidDiscount
				+ "]";
	}

	@Override
	public int compareTo( Object arg0 ) {
		return this.name.compareTo( ((Diagnosis) arg0).getName() );
	}
}
