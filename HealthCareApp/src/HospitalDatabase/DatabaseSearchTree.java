package HospitalDatabase;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import support.BSTInterface;

public class DatabaseSearchTree<K, T> implements BSTInterface <K, T>, Iterable <T> {

	/**
	 * The internal representation of a BST node
	 */
	class BSTNode {
		K key;
		T value;
		int count;
		BSTNode left, right, next;

		public BSTNode getNext() {
			return next;
		}

		public void setNext( BSTNode next ) {
			this.next = next;
		}

		public T getValue() {
			return value;
		}

		public void setValue( T value ) {
			this.value = value;
		}

		public K getKey() {
			return key;
		}

		public void setKey( K key ) {
			this.key = key;
		}

		public void setKeyAndInfo( K key, T info ) {
			setKey( key );
			setValue( info );
		}

		public BSTNode getLeft() {
			return left;
		}

		public void setLeft( BSTNode left ) {
			this.left = left;
		}

		public BSTNode getRight() {
			return right;
		}

		public void setRight( BSTNode right ) {
			this.right = right;
		}

		public BSTNode( K key, T info, DatabaseSearchTree <K, T>.BSTNode left, DatabaseSearchTree <K, T>.BSTNode right ) {
			this.key   = key;
			this.value = info;
			this.left  = left;
			this.right = right;
			this.next  = null;
			this.count = 1;
		}

		@Override
		public String toString() {
			return key.toString() + " " + value.toString();
		}

	}

	// root of the binary search tree
	private BSTNode root;
	// the comparator that determines the relation between two elements
	// in a binary search tree
	private Comparator <K> comp;

	private int size;
	
	public DatabaseSearchTree() {
		this.root = null;

		// This is elegant!
		this.comp = new Comparator <K>() {
			@Override
			public int compare( K arg0, K arg1 ) {
				return ( (Comparable) arg0 ).compareTo( arg1 );
			}
		};
	}

	public DatabaseSearchTree( Comparator <K> comp ) {
		this.root = null;
		this.comp = comp;
	}
	
	/**
	 * Finds all elements in the tree with a given tree. Returns them as an ArrayList
	 * @param key
	 * @return
	 */
	public ArrayList<T> findAll( K key ) {
		ArrayList<T> elements = new ArrayList<T>();
		BSTNode next = findAux( this.root, key );
		
		while( next != null ) {
			elements.add( next.getValue() );
			next = next.getNext();
		}
		return elements;
	}
	
	/**
	 * Recursively insert element into the subtree root at node if the key does not
	 * already exist in the tree
	 */
	private BSTNode insertAux( BSTNode node, K target, T element ) {
		if ( node == null ) {
			// If node is null, create a new BSTNode and initialize the info
			// with element. Returns the newly created node.
			// increment the size of the tree
			size++;
			return new BSTNode( target, element, null, null );
		} else {
			int cmp = comp.compare( target, node.getKey() );

			// return the node if the key is found in the tree - nothing to add
			if ( cmp == 0 ) {
				node.setNext( insertAux( node.getNext(), target, element ) );
				return node;
			}
				

			// recursively insert to the left if element is smaller
			// insert to the fight if element is larger.
			if ( cmp < 0 ) {
				node.setLeft( insertAux( node.getLeft(), target, element ) );
			} else if ( cmp > 0 ) {
				node.setRight( insertAux( node.getRight(), target, element ) );
			}

			return node;
		}

	}

	/**
	 * Counts the number of unique keys in the tree
	 * @return
	 */
	public int keyCount() {
		return keyCountAux( root );
	}

	/**
	 * Recursive pre-order traversal function used to count the number of unique keys in the tree
	 * @param node
	 * @return
	 */
	private int keyCountAux( BSTNode node ) {
		if( node == null )
			return 0;
		
		return 1 + keyCountAux( node.getLeft() ) + keyCountAux( node.getRight() );
	}
	
	
	@Override
	public void insert( K key, T element ) {
		if ( element == null || key == null )
			throw new IllegalArgumentException( "Null parameter" );

		root = insertAux( root, key, element );

	}

	/**
	 * Recursively find the target in the subtree rooted at node.
	 * 
	 * If the element at node is equal, return the node. larger, recursively find in
	 * the left subtree smaller, recursively find in the right subtree
	 */
	private BSTNode findAux( BSTNode node, K target ) {
		// search fails
		if ( node == null ) {
			return null;
		}

		int cmp = comp.compare( target, node.getKey() );

		if ( cmp == 0 )
			return node;
		else if ( cmp < 0 ) {
			return findAux( node.getLeft(), target );
		} else if ( cmp > 0 ) {
			return findAux( node.getRight(), target );
		}

		return null;
	}

	@Override
	public T find( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		// return the info - if the key was found
		BSTNode result = findAux( root, key );
		if ( result == null )
			return null;
		return result.getValue();

	}
	/**
	 * Determine whether the specified key is in the binary search tree and return
	 * true or false
	 * 
	 */
	public boolean contains( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		return findAux( root, key ) != null;
	}

	private T lastDeleted;

	/**
	 * Recursively remove target from the subtree rooted at node.
	 */
	private BSTNode removeAux( BSTNode node, K target ) {
		// if cannot continue, the search fails.
		if ( node == null )
			return null;

		int cmp = comp.compare( target, node.getKey() );
		if ( cmp == 0 ) {
			if ( lastDeleted == null )
				lastDeleted = node.getValue();			
			if ( node.getLeft() == null )
				return node.getRight();

			if ( node.getRight() == null )
				return node.getLeft();

			// find replacement and recursively remove the replacement
			BSTNode largestOnLeft = findLargest( node.getLeft() );
			// do the replacement and recursively remove the replacement from
			// the right subtree
			node.setKeyAndInfo( largestOnLeft.getKey(), largestOnLeft.getValue() );
			node.setLeft( removeAux( node.getLeft(), largestOnLeft.getKey() ) );

		} else if ( cmp < 0 ) {
			// recursively remove from the left
			node.setLeft( removeAux( node.getLeft(), target ) );
		} else {
			// recursively remove from the right
			node.setRight( removeAux( node.getRight(), target ) );
		}

		return node;

	}

	public ArrayList<T> findLargest() {
		// ArrayList to store our results
		ArrayList<T> rtrn = new ArrayList<T>();
		// Find the largest key node
		BSTNode largest = findLargest( root );
		// Add all elements with that key to the list
		while( largest != null ) {
			rtrn.add( largest.value );
			largest = largest.next;
		}
		// Return the list
		return rtrn;
	}
	
	private BSTNode findLargest( BSTNode node ) {
		while ( node.getRight() != null )
			node = node.getRight();

		return node;
	}

	@Override
	public T remove( K key ) {
		if ( key == null )
			throw new IllegalArgumentException( "Null parameter" );

		lastDeleted = null;
		root = removeAux( root, key );

		// reduce number of elements in the tree on a successful delete
		if ( lastDeleted != null )
			size--;

		return lastDeleted;
	}

	/**
	 * Fills the provided queue with all keys in the tree
	 * @param queue the queue to fill
	 */
	public ArrayList<K> listKeys() {
		ArrayList< K > list = new ArrayList<K>();
		keyListAux( root, list );
		return list;
	}
	/**
	 * Lists all the keys in the tree
	 * @param node the root of the BST
	 * @param queue the queue to be filled
	 */
	private void keyListAux( BSTNode node, ArrayList<K> queue ) {
		if ( node != null ) {
			// Traverse left side of the tree first
			keyListAux( node.getLeft(), queue );
			// Queue this element
			queue.add( node.getKey() );
			// Traverse right side of the tree
			keyListAux( node.getRight(), queue );
		}
	}

	public String preOrder() {
		return preOrderAux( root );
	}

	private String preOrderAux( BSTNode node ) {
		if ( node == null )
			return "";

		String left  = preOrderAux( node.getLeft() );
		String right = preOrderAux( node.getRight() );

		return node.toString() + "\t" + left + "\t" + right;
	}

	@Override
	public int size() {
		return size;
	}

	/**
	 * Fills the provided array queue with the the contents of the BST, in order
	 * @param node The head of the BST to search
	 * @param queue The queue to fill
	 */
	private void inOrderVisit( BSTNode node, ArrayList<T> queue ) {
		if ( node != null ) {
			// Traverse the left side of this node, because it should be smaller
			inOrderVisit( node.getLeft(), queue );
			// Then enqueue the current node
			queue.add( node.getValue() );
			// Enqeue the next node with the same key
			inOrderVisit( node.getNext(), queue );


			// Finally, traverse the right side of this node, because it should be bigger
			inOrderVisit( node.getRight(), queue );
		}
	}

public Iterator <T> iterator() {
		return new Iterator <T>() {

			ArrayList <T> queue = new ArrayList <T>();
			{
				inOrderVisit( root, queue );
			}

			@Override
			public boolean hasNext() {
				return !queue.isEmpty();
			}

			@Override
			public T next() {
				if ( hasNext() ) {
					return queue.remove( 0 );
				} else
					throw new NoSuchElementException();
			}
		};
	}

}
