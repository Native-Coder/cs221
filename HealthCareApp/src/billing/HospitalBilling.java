package billing;

import java.util.ArrayList;
import java.util.Scanner;

import HospitalDatabase.BillingRecord;
import HospitalDatabase.DatabaseSearchTree;
import HospitalDatabase.Diagnosis;
import HospitalDatabase.HospitalDatabase;

public class HospitalBilling {
	
	private HospitalDatabase database;

	/**
	 * Computes the target cost for the BillingRecord
	 * 
	 * @param bill
	 * @return
	 */
	private double targetCost( BillingRecord bill ) {
		// Compute target cost
		double target = bill.getDiagnosis().getPerDayCost() * bill.getDaysTreated();
		// If they are a medicaid recipient, reduce the cost appropriately
		if ( bill.isMedicaidRecipient() )
			target *= (1.0 - bill.getDiagnosis().getMedicaidDiscount());
		return target;
	}

	/**
	 * Default constructor
	 */
	public HospitalBilling() {
	}

	/**
	 * Creates the diagnoses and billing record entries in the database
	 * 
	 * @param diagnoses
	 * @param records
	 * @return
	 */
	public String processBilling( Scanner diagnoses, Scanner records ) {
		database = new HospitalDatabase( diagnoses, records );

		return String.format( "Diagnoses: %3d stays: %3d hospitals: %3d", database.getDiagnosesCount(),
				database.getBillingRecordCount(), database.getHospitalCount() );

	}

	/**
	 * The method displays the total for each diagnosis (actually charged, not
	 * target cost). Diagnoses are sorted in lexicographical order based on their ID
	 */
	public void displayChargedDiagnosisCost() {
		double cost;

		// List the diagnosis in lexicographical order
		for ( Diagnosis diag : database.listDiagnoses() ) {

			cost = 0;

			// Get all bills for each diagnosis
			for ( BillingRecord bill : database.recordsByDiagnosis( diag ) ) {

				// Total the cost
				cost += bill.getBillTotal();

			}
			System.out.println( String.format( "Diagnosis: %10s total charged cost: %.2f", diag.getName(), cost ) );

		}
	}

	/**
	 * The method displays the target total for each diagnosis (target cost, not
	 * actually charged cost).
	 */
	public void displayTargetDiagnosisCost() {
		double cost;

		// List the diagnosis in lexicographical order
		for ( Diagnosis diag : database.listDiagnoses() ) {

			cost = 0.0;

			// Get all bills for each diagnosis
			for ( BillingRecord bill : database.recordsByDiagnosis( diag ) ) {

				// Total the cost
				cost += targetCost( bill );

			}
			System.out.println( String.format( "Diagnosis: %10s total target cost: %.2f", diag.getName(), cost ) );

		}
	}

	/**
	 * The method displays the target total for each diagnosis (target cost, not
	 * actually charged cost). Diagnoses are sorted in lexicographical order based
	 * on their ID
	 */
	public double totalTargetDiagnosisCost() {
		double cost = 0.0;

		// For each diagnosis
		for ( BillingRecord bill : database.dumpBills() ) {

			// Calculate the target cost
			cost += targetCost( bill );

		}
		return cost;
	}

	/**
	 * The method returns the ratio of the actually charged cost and the target cost
	 * for the specified diagnosis.
	 * 
	 * @param diagnosisID
	 * @return
	 */
	public double diagnosisMultiplier( String diagnosisID ) {
		// Get all bills related to this diagnosis
		ArrayList < BillingRecord > bills = database.recordsByDiagnosis( diagnosisID );
		
		if( bills == null)
			return 0.0;
		
		// Set up a total cost
		double totalCost = 0, totalTarget = 0;

		// For each bill...
		for ( BillingRecord bill : bills ) {

			totalCost += bill.getBillTotal(); // add to the total cost
			totalTarget += targetCost( bill );

		}
		// Return 0 if the denominator would be zero
		return ( totalTarget == 0 ) ? 0 : totalCost / totalTarget;
	}

	/**
	 * This method returns the target total for all diagnoses together.
	 * 
	 * @param diagnosisID
	 * @return
	 */
	public double totalTargetDiagnosisCost( String diagnosisID ) {
		double cost = 0;

		// For each loaded diagnosis...
		for ( Diagnosis diag : database.listDiagnoses() ) {

			for ( BillingRecord bill : database.recordsByDiagnosis( diag ) ) {

				cost += targetCost( bill );

			}

		}
		return cost;
	}

	/**
	 * This method returns the ID of the hospital which charged the most.
	 * 
	 * @return
	 */
	public String mostExpensiveHospital() {
		double total = 0.0, currentHigh = 0.0;
		String mostExpensive = "";

		// For each hospital...
		for ( String hospitalID : database.listHospitals() ) {

			total = 0.0;

			// Add up the bill total...
			for ( BillingRecord bill : database.recordsByHospital( hospitalID ) ) {

				total += bill.getBillTotal();

			}

			// If it's more expensive than the current most expsensive...
			if ( total > currentHigh ) {

				// Make this the new most expensive
				mostExpensive = hospitalID;
				currentHigh = total;

			}

		}

		return mostExpensive;
	}

	/**
	 * This method returns the total cost (actually charged from the hospital, not
	 * target cost) for the specified hospital.
	 * 
	 * @param hospitalID
	 * @return
	 */
	public double totalHospitalCost( String hospitalID ) {
		double total = 0.0;
		for ( BillingRecord bill : database.recordsByHospital( hospitalID ) )
			total += bill.getBillTotal();
		return total;
	}

	/**
	 * The method returns the ratio of the actually charged cost and the target cost
	 * for the specified hospital
	 * 
	 * @param hospitalID
	 * @return
	 */
	public double hospitalMultiplier( String hospitalID ) {
		double totalCharged = 0.0, totalTarget = 0.0;
		ArrayList < BillingRecord > bills = database.recordsByHospital( hospitalID );

		for ( BillingRecord bill : bills ) {

			totalCharged += bill.getBillTotal();
			totalTarget += targetCost( bill );

		}

		return (totalTarget == 0) ? 0 : totalCharged / totalTarget;

	}

	/**
	 * Prints information about each diagnosis
	 */
	public void printDiagnosisInfo() {
		for ( Diagnosis diag : database.listDiagnoses() )
			System.out.println( diag );
	}

	/**
	 * Prints the information for each BillingRecord in the database
	 */
	public void printHospitalStayInfo() {
		for ( BillingRecord bill : database.recordsByCost( 0, Integer.MAX_VALUE ) )
			System.out.println( bill );
	}

	public void printHospitalInfo() {
		
		double amtCharged, targetCost;
		DatabaseSearchTree<String, String> uniquePatients  = new DatabaseSearchTree<String, String>();
		DatabaseSearchTree<String, String> uniqueDiagnoses = new DatabaseSearchTree<String, String>();
		
		for ( String hospitalId : database.listHospitals() ) {
			
			ArrayList<BillingRecord> bills = database.recordsByHospital( hospitalId );
			amtCharged = targetCost = 0;
			
			for ( BillingRecord bill : bills ) {
				amtCharged += bill.getBillTotal();
				targetCost += targetCost( bill );
				
				// We don't care about the values. we just want the key counts. :)
				uniquePatients.insert( bill.getPatientId(), "" );
				uniqueDiagnoses.insert( bill.getDiagnosis().getName(), "" );
				// targetCost += database.
			}
			System.out.println( hospitalId + " reports " + uniquePatients.keyCount() + " patients." );
			System.out.println( "\t" + uniqueDiagnoses.keyCount() + " unique illnesses..." );
			System.out.println( "\t" + amtCharged + " charged with a " + amtCharged / targetCost + " multiplier" );
			System.out.println( "\t" + "and a " + targetCost + " total target cost" );
		}
	}
}


















