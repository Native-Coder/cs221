import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

import HospitalDatabase.HospitalDatabase;
import LinkedList.LinkedQueue;
import billing.HospitalBilling;

public class HospitalApp {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub

		Scanner diagnosisData = new Scanner(new File("diagnosis1.txt"));
		Scanner stayData = new Scanner(new File("patients.txt"));

		HospitalBilling hb = new HospitalBilling();

		System.out.println(hb.processBilling(diagnosisData, stayData));

//		hb.printDiagnosisInfo();
//
//		hb.printHospitalStayInfo();
//
//		hb.printHospitalInfo();
		
		// required methods

		System.out.println();
		hb.displayChargedDiagnosisCost();
		
		System.out.println();
		hb.displayTargetDiagnosisCost();
		
		// multiplier for given diagnosis
		System.out.println();
		String diagnosis="Cancer123";
		System.out.println(String.format("Multiplier for diagnosis %10s is %.2f", diagnosis,hb.diagnosisMultiplier (diagnosis)));

		// total target cost of all diagnoses
		System.out.println(String.format("Total target cost across all diagnoses: %.2f",hb.totalTargetDiagnosisCost()));
		
		
		// most expensive hospital and it's cost
		String answer1 = hb.mostExpensiveHospital();
		System.out.println(String.format("Most expensive hospital %10s cost  %.2f", answer1, hb.totalHospitalCost(answer1)));

		// average weighted markup for a given hospital
		String test1 = "BG2";
		System.out.println(String.format("Average markup for %5s:   %.2f ", test1,hb.hospitalMultiplier("BG2")));
		
	}
}