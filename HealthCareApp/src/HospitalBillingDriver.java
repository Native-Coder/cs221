import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import billing.HospitalBilling;

public class HospitalBillingDriver {

	private static final String displayCharged = "displayCharged";
	private static final String displayTarget = "displayTarget";
	private static final String diagnosisMult = "diagnosisMult";
	private static final String hospitalMult = "hospitalMult";
	private static final String totalTarget = "totalTarget";
	private static final String mostExpensive = "mostExpensive";
	private static final String totalHospital = "totalHospital";
	private static final String quit = "quit";

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub

		Scanner inData = new Scanner(new File("HBassignmentTest.in"));
//		Scanner inData = new Scanner(new File("HBassignmentTest2.in"));
//		Scanner inData = new Scanner(new File("HBassignmentTestCheckAll.in"));
//		Scanner inData = new Scanner(new File("HBNorepetition.in"));
//		Scanner inData = new Scanner(new File("HBLong.in"));

		// read name of text and display
		String testName = inData.next();
		System.out.println(testName);

		// set up reading from the files
		String diagnosesFileName;
		String hospitalStayFileName;
		diagnosesFileName = inData.next();
		hospitalStayFileName = inData.next();

		Scanner diagnosisData = new Scanner(new File(diagnosesFileName));
		Scanner stayData = new Scanner(new File(hospitalStayFileName));

		HospitalBilling hb = new HospitalBilling();

		// process the information from the files
		System.out.println(hb.processBilling(diagnosisData, stayData));
		/*
		hb.printDiagnosisInfo();

		hb.printHospitalStayInfo();

		hb.printHospitalInfo();

		hb.displayTargetHospitalCost();*/

		String choice;
		do {
			choice = inData.next();
			System.out.println(choiceString(choice));
			if (!choice.equals("quit")) {
				executeChoice(choice, hb, inData);
				// driverPrint();
			}
		} while (!choice.equals("quit"));

//		// read the string which specifies the testing to be done.
//		String checksToRun = inData.next();

//		hb.printDiagnosisInfo();
//
//		hb.printHospitalStayInfo();
//
//		hb.printHospitalInfo();

//		// required methods
//		// charged total for each diagnosis
//		System.out.println();
//		testDisplayChargedDiagnosisCost(hb, checksToRun);
//
//		// target total for each diagnosis
//		System.out.println();
//		testDisplayTargetDiagnosisCost(hb, checksToRun);
//
//		// multiplier for given diagnosis
//		System.out.println();
//		testDiagnosisMultiplier(hb, checksToRun, inData);
//
//		// total target cost of all diagnoses
//		testTotalTargetDiagnosisCost(hb, checksToRun);
//
//		// most expensive hospital and it's cost
//		testMostExpensiveHospital(hb, checksToRun);
//
//		// average weighted markup for a given hospital
//		testHospitalMultiplier(hb, checksToRun, inData);
	}
	

	private static String choiceString(String choice) {
		switch (choice) {
		case displayCharged:
			return "\n* Testing displayChargedDiagnosticCost";
		case displayTarget:
			return "\n* Testing displayTargetDiagnosticCost";
		case diagnosisMult:
			return "\n* Testing diagnosisMultiplier";
		case hospitalMult:
			return "\n* Testing hospitalMultiplier";
		case totalTarget:
			return "\n* Testing totalTargetDiagnosisCost";
		case mostExpensive:
			return "\n* Testing mostExpensiveHospital";
		case totalHospital:
			return "\n* Testing totalHospitalCost";
		case quit:
			return "quit";
		default:
			return "nothing";
		}
	}

	// Determine what the user wants to do and execute the appropriate method
	static void executeChoice(String choice, HospitalBilling hosbill, Scanner inData) {
		switch (choice) {
		case "displayCharged":
			hosbill.displayChargedDiagnosisCost();
			break;
		case "displayTarget":
			hosbill.displayTargetDiagnosisCost();
			break;
		case "diagnosisMult":
			executeDiagnosisMultiplier(hosbill, inData);
			break;
		case "hospitalMult":
			executeHospitalMultiplier(hosbill, inData);
			break;
		case "totalHospital":
			executeTotalHospital(hosbill, inData);
			break;
		case "totalTarget":
			System.out.println(
					String.format("Total target cost across all diagnoses: %.2f", hosbill.totalTargetDiagnosisCost()));
			break;
		case "mostExpensive":
			executeMostExpensiveHospital(hosbill);
			break;

		}
		// executePrint(testA, size);
	}

	/**
	 * Test the diagnosisMultiplier method
	 * 
	 * @param hosbill HospitalBilling class
	 * @param inData  The Scanner from which to read the name of the diagnosis
	 */
	private static void executeDiagnosisMultiplier(HospitalBilling hosbill, Scanner inData) {
		String diagnosis = inData.next();
		inData.nextLine();
		System.out.println(String.format("Multiplier for diagnosis %10s is %.2f", diagnosis,
				hosbill.diagnosisMultiplier(diagnosis)));
	}

	/**
	 * Test the hospitalMultiplier method
	 * 
	 * @param hosbill HospitalBilling class
	 * @param inData  The Scanner from which to read the name of the diagnosis
	 */
	private static void executeHospitalMultiplier(HospitalBilling hosbill, Scanner inData) {
		String hospital = inData.next();
		inData.nextLine();
		System.out.println(String.format("Multiplier for diagnosis %10s is %.2f", hospital,
				hosbill.hospitalMultiplier(hospital)));
	}

//	/**
//	 * Test the totalTargetDiagnosisCost method
//	 * 
//	 * @param hosbill     HospitalBilling class
//	 * @param checksToRun A variable which contains info about which check to run.
//	 *                    If the fourth character is a 1, then the
//	 *                    totalTargetDiagnosisCost check is executed.
//	 */
//	private static void testTotalTargetDiagnosisCost(HospitalBilling hosbill, String checksToRun) {
//		if (checksToRun.substring(3, 4).equals("1")) {
////			System.out.println();
//			System.out.println(
//					String.format("Total target cost across all diagnoses: %.2f", hosbill.totalTargetDiagnosisCost()));
//		}
//	}

	/**
	 * Test the mostExpensiveHospital method
	 * 
	 * @param hosbill     HospitalBilling class
	 */
	private static void executeMostExpensiveHospital(HospitalBilling hosbill) {
			String answer1 = hosbill.mostExpensiveHospital();
			System.out.println(String.format("Most expensive hospital %10s cost  %.2f", answer1,
					hosbill.totalHospitalCost(answer1)));
	}

	/**
	 * Test the totalHospitalCost method
	 * 
	 * @param hosbill     HospitalBilling class
	 * @param checksToRun A variable which contains info about which check to run.
	 *                    If the seventh character is a 1, then the
	 *                    totalHospitalCost check is executed.
	 * @param inData      The Scanner from which to read the name of the hospital
	 */
	private static void executeTotalHospital(HospitalBilling hosbill, Scanner inData) {
		String hospitalName = inData.next();
		inData.nextLine();
		System.out.println(String.format("Total charged cost for hospital %10s is  %.2f", hospitalName,
				hosbill.totalHospitalCost(hospitalName)));
	}

}
