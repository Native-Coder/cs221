import java.util.Iterator;

public class PrimeIterator implements Iterator<Integer> {
	LinkedNode<Integer> head, tail, next;
	int currentNum, count = 0;
	
	private class LinkedNode<Integer> {
		LinkedNode<Integer> next;
		int value;	
		
		LinkedNode(){
			count++;
		}
		
		
		LinkedNode( int n ){
			value = n;
			count++;
		}
	}
	
	PrimeIterator(){

		head       = new LinkedNode<Integer>( 2 ); // The first prime is two, but all other primes are odd...	
		next       = head;
		tail       = head;
		currentNum = 2;
	}
	
	/**
	 * Inserts a value into the list
	 * @param num
	 */
	private void insert( int num ) {
		// temp node used to store value
		LinkedNode<Integer> temp = new LinkedNode<Integer>( num );
		// Link the last node to the new node
		tail.next = temp;
		// Make the new node the tail of the list
		tail = temp;
		
		currentNum = num;
	}
	
	@Override
	public boolean hasNext() {
		return true;
	}

	@Override
	public Integer next() {
		if( currentNum == 2) {
			currentNum = 3;
			return 2;
		}
		
		LinkedNode<Integer> node = head;
		int numberToCheck = currentNum;
		// Every composite number has a divisor below its square root...
		while( node != null && node.value <= Math.sqrt( numberToCheck ) ) {
			// If this number is composite...
			if( numberToCheck % node.value == 0) {
				// Reset node
				node = head;
				// Check the next odd number
				numberToCheck += 2;
			}
			// Move to the next prime number
			node = node.next;
		}
		// Once we find the next prime, store it in our list
		insert( numberToCheck );
		currentNum = numberToCheck + 2; // Store the next number to be checked
		return  ( numberToCheck );
	}
}
