package lab10;

public class Mystery {

	public Mystery() {
		// TODO Auto-generated constructor stub
	}

	private int value1;
	private int value2;
	private String value3;
	private  int ID;

	private static int idCount= 0;
	
	/**
	 * @param value1
	 * @param value2
	 * @param value3
	 */
	public Mystery(int value1, int value2, String value3) {
		super();
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.ID = idCount++;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value1;
		result = prime * result + value2;
		result = prime * result + ((value3 == null) ? 0 : value3.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mystery other = (Mystery) obj;
		if (value1 != other.value1)
			return false;
		if (value2 != other.value2)
			return false;
		if (value3 == null) {
			if (other.value3 != null)
				return false;
		} else if (!value3.equals(other.value3))
			return false;
		return true;
	}
	/**
	 * @return the value1
	 */
	public int getValue1() {
		return value1;
	}
	/**
	 * @return the value2
	 */
	public int getValue2() {
		return value2;
	}
	
	@Override
	public String toString() {
		return "Mystery [value1=" + value1 + ", value2=" + value2 + ", value3=" + value3 + ", ID=" + ID + "]";
	}

	
}
