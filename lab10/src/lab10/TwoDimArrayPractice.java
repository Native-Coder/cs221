package lab10;

import java.util.Arrays;
import java.util.Comparator;

public class TwoDimArrayPractice {

	/**
	 * returns the average value of all the values in the profitInfo array which are
	 * larger than threshold. If no element in the array is greater than the
	 * threshold, the method returns 0
	 * 
	 * @param profitInfo
	 * @param thresh
	 * @return
	 */
	public static double average( double profitInfo[][], double thresh ) {
		double total = 0.0, // The running total
			   count = 0.0; // The number of elements counted

		// Iterate over each row
		for ( int row = 0; row < profitInfo.length; row++ ) {
			// Iterate over each column in this row
			for ( int col = 0; col < profitInfo[ row ].length; col++ ) {
				if ( profitInfo[ row ][ col ] > thresh ) {
					total += profitInfo[ row ][ col ]; // Add it to the running total
					count++; // Increment the number of elements count
				}
			}
		}

		return ( count > 0 ? total / count : 0 );
	}

	/**
	 * returns an array which contains the longest string in each row. If a row
	 * contains several strings of the longest length, then return the first one
	 * (with the smallest column value).
	 * 
	 * @param lines
	 * @return
	 */
	public static String[] longestStringInEachRow( String lines[][] ) {
		// Need an array that is the same size as the first dimension of lines
		String[] rtrn = new String[ lines.length ];
		// Start with empty strings
		Arrays.fill( rtrn, "" );

		// Iterate over each row
		for ( int row = 0; row < lines.length; row++ ) {
			// Iterate over each column in that row
			for ( int col = 0; col < lines[ row ].length; col++ ) {
				// If the stored string is smaller than the current array element
				if ( rtrn[ row ].length() < lines[ row ][ col ].length() ) {
					rtrn[ row ] = lines[ row ][ col ]; // Replace the stored string with this array element
				}

			}
		}

		return rtrn;
	}

	/**
	 * returns an Mystery object which is the smallest in the given info array.
	 * ‘smallest’ is determined using the passed in comparator.
	 * 
	 * @param info
	 * @param comp
	 * @return
	 */
	public static Mystery smallestMystery( Mystery info[][], Comparator< Mystery > comp ) {
		Mystery smallest = info[ 0 ][ 0 ];
		// Iterate over each row
		for ( int row = 0; row < info.length; row++ ) {
			// Iterate over each column in that row
			for ( int col = 0; col < info[ row ].length; col++ ) {
				// If the array element is smaller than the smallest
				if ( comp.compare( info[ row ][ col ], smallest ) < 0 )
					smallest = info[ row ][ col ]; // Update the smallest
			}
		}
		return smallest;
	}

}
