package lab10;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class ComputeTerrainInfo {

	/**
	 * finds a path (marked by P) in the terrain which starts at the given
	 * location. The method returns an array of strings indicating the direction of
	 * move to follow the path. There are no diagonal moves.
	 * 
	 * @param terrain
	 * @param locR
	 * @param locC
	 * @return
	 */
	public static String[] findPath( String[][] terrain, int locR, int locC ) {
		if( terrain[ locR ][ locC ] != "P" )
			return null;
		
		LinkedList<String> list = new LinkedList<String>();
		
		/**
		 * While we are on the grid
		 * 	check each direction. make sure that we didn't already check that coordinate (prevents moving backwards), then move
		 */
		while( !outOfBounds( terrain, locR, locC ) ) {
				if( checkAbove( terrain, locR, locC ) && list.peekLast() != "down" ) {
					list.add( "up" );
					locR--;
				} else if( checkLeft( terrain, locR, locC ) && list.peekLast() != "right") {
					list.add( "left" );
					locC--;
				} else if ( checkBelow( terrain, locR, locC ) && list.peekLast() != "up" ) {
					list.add( "down" );
					locR++;
				} else if( checkRight( terrain, locR, locC )  && list.peekLast() != "left") {
					list.add( "right" );
					locC++;
				// If we can't find a place to go, break the loop early
				} else {
					break;
				}
			}
			
			String[] rtrn = new String[ list.size() ];
			Iterator<String> itr = list.iterator(); 
			
			for( int i = 0; itr.hasNext(); i++)
				rtrn[ i ] = itr.next();
			
			return rtrn;
	}
	

	/**
	 * determines how far a feature extends in the given direction from the given
	 * position
	 * 
	 * @param terrain
	 * @param row
	 * @param col
	 * @param rowChange
	 * @param colChange
	 * @return
	 */
	public static int extensionLength( String[][] terrain, int row, int col, int rowChange, int colChange ) {
		// Sanity check
		if( outOfBounds( terrain, row, col ) )
			return 0;
		
		int rowDelta = rowChange, colDelta = colChange;	
		int count = 1;// This block counts
		
		// While we aren't out of bounds and...
		while ( !outOfBounds( terrain, row + rowDelta, col + colDelta ) &&
				// The tile still matches
				terrain[ row ][ col ].equals( terrain[ row + rowDelta ][ col + colDelta] ) ) {
			count++;
			rowDelta += rowChange;
			colDelta += colChange;
		}
		return count;
	}

	/**
	 * determines how many of the surrounding 8 terrain locations are of the
	 * specified feature.
	 * 
	 * @param terrain
	 * @param row
	 * @param col
	 * @param feature
	 * @return
	 */
	public static int surroundCount( String[][] terrain, int row, int col, String feature ) {
		int count = 0;
		
		for ( String s : surroundingTiles( terrain, row, col ) ) {
			if ( s != null && s.equals( feature ) ) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Returns an array with the contents of the tiles surrounding the point ( row,
	 * col )
	 * 
	 * @param arr
	 * @param row
	 * @param col
	 * @return
	 */
	public static String[] surroundingTiles( String[][] arr, int row, int col ) {
		String[] rtrn = new String[ 8 ];
		int index = 0;
		int offsetRow, offsetCol;
		
		// Offset the rows by -1, 0, 1
		for ( int rowOffset = -1; rowOffset <= 1; rowOffset++ ) {
			offsetRow = row + rowOffset;
			// Offset the columns by -1, 0, 1
			for ( int colOffset = -1; colOffset <= 1; colOffset++ ) {
				offsetCol = col + colOffset;
				// Make sure the coordinates are on the board &&   skip the "center" tile.
				if( !outOfBounds( arr, offsetRow, offsetCol ) && !( rowOffset == 0 && colOffset == 0 ) ) {
					// Set the next value in the return array
					rtrn[ index ] = arr[ offsetRow ][ offsetCol ];
					index++;
				}
			}
		}
		return rtrn;
	}

	/**
	 * Determines if the coordinates (row, col) are out of bounds for the array arr
	 * 
	 * @param arr
	 * @param row
	 * @param col
	 * 
	 * @return
	 */
	public static boolean outOfBounds( String[][] arr, int row, int col ) {		
		// Below 0 or greater than the array bounds are a no-go
		boolean tooSmall = ( row < 0 || col < 0 );
		boolean tooBig   = ( row > ( arr.length - 1 ) || ( col > arr[ 0 ].length - 1 ) );
		return ( tooBig || tooSmall );
	}

	/**
	 * returns the value of the element above the given coordinates
	 * 
	 * @param arr
	 * @param row
	 * @param col
	 * @return
	 */
	public static boolean checkAbove( String[][] arr, int row, int col ) {
		if( outOfBounds( arr, row - 1, col ) )
			return false;
		return arr[ row - 1 ][ col ].equals( "P" ) ; 
	}

	/**
	 * returns the value of the element to the left of the given coordinates
	 * 
	 * @param arr
	 * @param row
	 * @param col
	 * @return
	 */
	public static boolean checkLeft( String[][] arr, int row, int col ) {
		if( outOfBounds( arr, row, col - 1 ) )
			return false;
		return arr[ row ][ col - 1 ].equals( "P" );
	}

	/**
	 * returns the value of the element to the right of the given coordinates
	 * 
	 * @param arr
	 * @param row
	 * @param col
	 * @return
	 */
	public static boolean checkRight( String[][] arr, int row, int col ) {
		if( outOfBounds( arr, row, col + 1 ) )
			return false;
		return arr[ row ][ col + 1 ].equals( "P" );
	}

	/**
	 * returns the value of the element below the given coordinates
	 * 
	 * @param arr
	 * @param row
	 * @param col
	 * @return
	 */
	public static boolean checkBelow( String[][] arr, int row, int col ) {
		if( outOfBounds( arr, row + 1, col ) )
			return false;
		return arr[ row + 1 ][ col ].equals( "P" );
	}

}
