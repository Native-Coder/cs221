import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import lab10.ComputeTerrainInfo;

public class ComputeTerrainInfoDriver {

	public static void main(String[] args) throws FileNotFoundException {

//		Scanner inData = new Scanner (System.in);

		Scanner inData = new Scanner(new FileReader("testTerrainExtension1.in"));
//		Scanner inData = new Scanner(new FileReader("testTerrainExtension2.in"));
//		Scanner inData = new Scanner(new FileReader("testTerrainSurroundCount1.in"));
//		Scanner inData = new Scanner(new FileReader("testTerrainSurroundCount2.in"));

		String testName = inData.nextLine();
		System.out.println(testName);

		String testToMake = inData.nextLine();

		switch (testToMake) {
		case "extension":
			testExtension(inData);
			break;
		case "surroundCount":
			testSurroundCount(inData);
			break;
//		case "findPath":
//			testfindPath(inData);
//			break;
		}

//
//		System.out.println("findPath:  start 8, 9: " + findPath(terrain, 8, 9));
//		System.out.println("findPath:  start 3,7 : " + findPath(terrain, 3, 7));
//		System.out.println("findPath: start 8,1 :" + findPath(terrain, 8, 1));
	}

	/**
	 * Test the extensionLength method from the ComputeTerrainInfo class
	 * @param inData	The Scanner which contains testing information
	 */
	private static void testExtension(Scanner inData) {

		while (inData.hasNext()) {
			String[][] terArray = loadTerrainFromFile(inData);
			
			// run all the tests for this terrain
			while (inData.hasNext()) {
				int locR = inData.nextInt();
				int locC = inData.nextInt();
				int changeR = inData.nextInt();
				int changeC = inData.nextInt();
				System.out.println("Testing extensionLength: starting: " +locR + " " + locC + " direction " + changeR + " " + changeC + ": " 
									+ ComputeTerrainInfo.extensionLength(terArray, locR, locC, changeR, changeC));
			}
		}
			
			

	}
	
	/**
	 * Test the surroundCount method from the ComputeTerrainInfo class
	 * @param inData	The Scanner which contains testing information
	 */
	private static void testSurroundCount(Scanner inData) {

		while (inData.hasNext()) {
			String[][] terArray = loadTerrainFromFile(inData);
			
			// run all the tests for this terrain
			while (inData.hasNext()) {
				int locR = inData.nextInt();
				int locC = inData.nextInt();
				String feature = inData.next();
				if (feature.equals("G"))
					feature = " ";
				System.out.println("Testing surroundCount: starting: " +locR + " " + locC + " \""+feature  + "\""  + ": " 
									+ ComputeTerrainInfo.surroundCount(terArray, locR, locC, feature));
			}
		}
			
			

	}

	/*
	 * Create and load the terrain from the Scanner
	 */
	private static String[][] loadTerrainFromFile(Scanner inData) {
		System.out.println("Next test:");
		int rows = inData.nextInt();
		int cols = inData.nextInt();
		inData.nextLine(); // throw away rest of line with \n
		String [][] terArray = new String[rows][cols];
		loadTerrain(terArray,inData);
		print2Darray(terArray);
		return terArray;
	}


	/**
	 * Load the terrain info in the existing array from the file
	 * @param array	The existing array	
	 * @param inData	The Scanner with the info to be loaded
	 */
	private static void loadTerrain(String[][] array, Scanner inData) {
		for (int row = 0; row < array.length; row++) {
			String nextRow = inData.nextLine();
			for (int col = 0; col < array[0].length; col++)
				array[row][col] = nextRow.substring(col, col + 1);
		}
	}

	/**
	 * Display the terrain
	 * 
	 * @param terrain
	 */
	static void print2Darray(String[][] terrain) {
		int rowVal = 0;
		int colVal = 0;
		System.out.print("  ");
		for (int col = 0; col < terrain[0].length; col++)
			System.out.print(String.format("%2d", colVal++));
		System.out.println();
		for (String[] row : terrain) {
			System.out.print(String.format("%2d", rowVal++));
			for (String info : row)
				System.out.print("|" + info);
			System.out.println("|");
		}

	}

}
