import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;

import lab10.Mystery;
import lab10.TwoDimArrayPractice;

public class TwoDimArrayPracticeDriver {

	static Random rand = new Random(521);

	public static void main(String[] args) throws FileNotFoundException{

//		Scanner inData = new Scanner (System.in);

//		Scanner inData = new Scanner (new FileReader("testAverage1.in"));
		Scanner inData = new Scanner (new FileReader("testSmallestMystery1.in"));

		String testName = inData.nextLine();
		System.out.println(testName);

		String testToMake = inData.nextLine();
		
		switch (testToMake) {
		case "average":
			testAverage(inData);
			break;
		case "longestStrings":
			testLongestStringEachRow(inData);
			break;
		case "smallestMystery":
			testSmallestMystery(inData);
			break;
		}

	}

	/**
	 * Test the average method in the TwoDimArrayPractice class
	 * @param inData	The scanner with the testing information
	 */
	private static void testAverage(Scanner inData) {

		while (inData.hasNext()) {
			System.out.println("Next test:");
			int rows = inData.nextInt();
			int cols = inData.nextInt();
			double threshold = inData.nextDouble();
			double[][] testArr = new double[rows][cols];
			loadDoubleArray(testArr, inData);
			double res = TwoDimArrayPractice.average(testArr, threshold);
			System.out.println(String.format("Computed average is %.2f\n", res));

		}
	}

	/**
	 * Load the array from the scanner
	 * @param array	The array to be loaded
	 * @param inData	The scanner with the info to be loaded
	 */
	private static void loadDoubleArray(double[][] array, Scanner inData) {
		for (int row = 0; row < array.length; row++)
			for (int col = 0; col < array[row].length; col++) {
				array[row][col] = inData.nextDouble();
			}
	}

	/**
	 * Test the longestStringInEachRow method in the TwoDimArrayPractice class
	 * @param inData	The scanner with the testing information
	 */
	private static void testLongestStringEachRow(Scanner inData) {
		while (inData.hasNext()) {
			System.out.println("Next test:");
			int rows = inData.nextInt();
			int cols = inData.nextInt();
			inData.nextLine();
			String[][] testArr = new String[rows][cols];
			loadStringArray(testArr, inData);
			String[] longestRes = TwoDimArrayPractice.longestStringInEachRow(testArr);
			display1DArray(longestRes);

		}
	}

	/**
	 * Load the array from the scanner
	 * @param array	The array to be loaded
	 * @param inData	The scanner with the info to be loaded
	 */
	private static void loadStringArray(String[][] array, Scanner inData) {
		for (int row = 0; row < array.length; row++)
			for (int col = 0; col < array[row].length; col++) {
				array[row][col] = inData.nextLine();
			}
	}

	/**
	 * Test the smallestMystery method; the outcome cannot be determined easily since the 
	 * hashvalues are so large that overflow happens. 
	 * @param inData  The scanner with the testing information
	 */
	private static void testSmallestMystery(Scanner inData) {
		while (inData.hasNext()) {
			System.out.println("Next test:");

			int rows = inData.nextInt();
			int cols = inData.nextInt();
			int boundary = inData.nextInt();
			int reps = inData.nextInt();
			Comparator<Mystery> testHashCode = (Mystery one, Mystery two) -> (two.hashCode() - one.hashCode());
			Comparator<Mystery> testValues = (Mystery one, Mystery two) -> (two.getValue1() - one.getValue1());
			for (int i = 0; i < reps; i++) {
				Mystery[][] useIt = createMysteryArray(rows, cols, 521 + i * 53, boundary);
			display2DArray (useIt);
				Mystery smallest = TwoDimArrayPractice.smallestMystery(useIt, testHashCode);
				System.out.println("Smallest using \"testHashCode\" comparator " + smallest);
				smallest = TwoDimArrayPractice.smallestMystery(useIt, testValues);
				System.out.println("Smallest using \"testValues\" comparator " + smallest);
			}
		}
	}

	/** 
	 * Method to create an array of mystery
	 * @param rows		Number of rows in the array to be created
	 * @param cols 		Number of cols in the array to be created
	 * @param seed		Seed value for random generator to be used
	 * @param range 	Range of values to be used (from -range/2 to + range/2)
	 * @return	The created array of objects of type Mystery
	 */
	private static Mystery[][] createMysteryArray(int rows, int cols, int seed, int range) {
		rand.setSeed(seed);
		String val3 = "";
		if (range > 1000000)
			val3 = "xx";
		Mystery[][] newArray = new Mystery[rows][cols];
		for (int row = 0; row < rows; row++)
			for (int col = 0; col < cols; col++) {
//				xxx[row][col] = new Mystery(rand.nextInt(), rand.nextInt(), "");
				newArray[row][col] = new Mystery(-range / 2 + rand.nextInt(range), -range / 2 + rand.nextInt(range),
						val3);
			}
		return newArray;
	}

	/**
	 * Show 2-dim array for generic 2D array
	 * @param info  The array to be displayed
	 */
	public static <T> void display2DArray(T[][] info) {
		for (T[] currentRow : info) {
			for (T currentElem : currentRow) {
				System.out.print(currentElem + " ");
			}
			System.out.println();
		}

	}

	/**
	 * Show 1-dim array for generic 1D array
	 * @param info  The array to be displayed
	 */
	public static <T> void display1DArray(T[] info) {
		for (T currentElem : info)
			System.out.println(currentElem + " ");
	}

}