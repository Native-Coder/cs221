

import lab10.ComputeTerrainInfo;
import lab10.TwoDimArrayPractice;

public class driver {
	public static void main( String args[] ) {

		TwoDimArrayPractice p = new TwoDimArrayPractice();
		ComputeTerrainInfo c  = new ComputeTerrainInfo();
		
		
		testLongestString( p );
		testExtensionLength( c );
		testSurroundCount( c );
		testOutOfBounds( c );
		testPathfinding( c );
		testSurroundingTiles( c );
	}

	public static void testLongestString( TwoDimArrayPractice p ) {
		String[][] arr = {
				{ "a", "b", "this is the longest", "not this one" },//check middle
				{ "this is longer than anything else", "a"  }, // check beginning
				{ "Another ONE!" }, // Check single element
				{ "not this one", "but this one will work"}, // check end
				{"first", "forst" }, // should return first 
				{" ", ""},// check empty strings
		};
		
		
		for( String longest : p.longestStringInEachRow( arr ) ) {
			System.out.println( longest );
		}
		
		
	}
	
	public static void testPathfinding( ComputeTerrainInfo c ) {
		
		// Name says it all, makes sure that we don't move backwards
		String [][] backwardsTest = {
				{ "P",  "P" },
				{ " ", " " }
		};
		
		// Simple path.
		String[][] simpleTest = {
				{ " ", " ", "L", "R", "W" },
				{ " ", "L", "L", "W", " " },
				{ " ", " ", "W", "P", "R" },
				{ " ", "W", "W", "P", "R" },
				{ "W", "P", "P", "P", "R" },
		};
		
		String[][] loopTest = {
				{ "P", "P", "w", "P", "P" },
				{ "P", "L", "W", "W", "P" },
				{ "P", " ", "W", "W", "P" },
				{ "P", "W", "W", "W", "P" },
				{ "P", "P", "P", "P", "P" },
		};
		
		String[][] complexTest = {
				{ " ", "P", "P", "P", " " },
				{ "P", "P", "W", "P", " " },
				{ "P", " ", "W", "P", "P" },
				{ "P", " ", "P", "W", "P" },
				{ "P", "P", "P", " ", "P" },
		};
		
		/**
		 * Grid is not a perfect square. 
		 */
		String[][] hugeTest = {
				{ "P","P"," ","P","P","P"," ","P","P","P"," "," "," "," "," ","P","P","P","P","P","P",  },
				{ " ","P"," ","P"," ","P"," ","P"," ","P"," "," "," "," "," ","P"," "," "," "," ","P",  },
				{ " ","P"," ","P"," ","P"," ","P"," ","P"," "," "," "," "," ","P"," "," "," "," ","P",  },
				{ "P","P"," ","P"," ","P"," ","P"," ","P"," ","P","P","P"," ","P"," "," "," "," ","P",  },
				{ "P"," ","P","P"," ","P"," ","P"," ","P"," ","P"," ","P"," ","P","P","P"," "," ","P",  },
				{ "P","P","P"," ","P","P"," ","P"," ","P"," ","P"," ","P"," "," "," ","P"," "," ","P",  },
				{ " "," "," ","P","P"," ","P","P"," ","P"," ","P"," ","P","P","P"," ","P"," "," ","P",  },
				{ "P","P","P","P"," "," ","P"," "," ","P"," ","P"," "," "," ","P"," ","P"," "," ","P",  },
				{ "P"," "," "," "," "," ","P"," ","P","P"," ","P"," "," "," ","P"," ","P"," "," ","P",  },
				{ "P"," ","P","P","P"," ","P"," ","P"," "," ","P","P"," "," ","P"," ","P"," "," ","P",  },
				{ "P"," ","P"," ","P","P","P"," ","P"," "," "," ","P"," "," ","P"," ","P"," "," ","P",  },
				{ "P"," ","P"," "," "," "," "," ","P"," "," "," ","P"," "," ","P","P","P"," "," ","P",  },
				{ "P"," ","P"," ","P","P","P"," ","P"," ","P","P","P"," "," "," "," "," "," "," ","P",  },
				{ "P"," ","P","P","P"," ","P"," ","P"," ","P"," "," "," "," "," "," "," ","P","P","P",  },
				{ "P"," "," "," "," "," ","P"," ","P"," ","P"," "," "," "," "," "," "," ","P"," "," ",  },
				{ "P","P","P","P","P","P","P"," ","P"," ","P"," "," "," ","P","P","P"," ","P"," "," ",  },
				{ " "," "," "," "," "," "," "," ","P"," ","P"," "," "," ","P"," ","P"," ","P"," "," ",  },
				{ "P","P","P","P","P","P","P","P","P"," ","P"," "," "," ","P"," ","P"," ","P"," ","P",  },
				{ "P"," "," "," "," "," "," "," "," "," ","P","P"," "," ","P"," ","P"," ","P"," ","P",  },
				{ "P"," "," "," "," "," "," "," "," "," "," ","P"," "," ","P"," ","P","P","P"," ","P",  },
				{ "P"," "," "," "," "," ","P","P","P"," "," ","P"," "," ","P"," "," "," "," "," ","P",  },
				{ "P"," "," "," "," "," ","P"," ","P"," ","P","P"," "," ","P"," ","P","P","P"," ","P",  },
				{ "P"," "," "," "," "," ","P"," ","P"," ","P"," ","P","P","P"," ","P"," ","P"," ","P",  },
				{ "P"," "," "," ","P","P","P"," ","P","P","P"," ","P"," "," ","P","P"," ","P"," ","P",  },
				{ "P"," "," "," ","P"," "," "," "," "," "," "," ","P","P"," ","P"," "," ","P","P","P",  },
				{ "P","P","P","P","P"," "," "," "," "," "," "," "," ","P","P","P"," "," "," "," "," ",  },
		};
		
		System.out.print( "Performing backwards test :" );
		System.out.print( c.findPath(  backwardsTest, 0, 0 ) );
		System.out.println();
		
		System.out.print( "Performing simple test: " );
		System.out.print( c.findPath(  simpleTest, 4, 1 ) );
		System.out.println();
		
		System.out.print( "Performing infinite loop test: " );
		System.out.print( c.findPath(  loopTest, 0, 1 ) );
		System.out.println();
		
		System.out.print( "Performing complex test: " );
		System.out.print( c.findPath(  complexTest, 4, 4 ) );
		System.out.println();
		
		System.out.print( "Performing HUGE test: " );
		System.out.print( c.findPath(  hugeTest, 0, 0 ) );
		System.out.println();
		
	}
	
	public static void testExtensionLength( ComputeTerrainInfo c ) {
		String[][] grid = {
				// 0    1   2    3     4
				{ " ", " ", "L", "R", "W" },
				{ " ", "L", "L", "W", " " },
				{ " ", " ", "W", "P", "R" },
				{ " ", "W", "W", "P", "R" },
				{ "W", "P", "P", "P", "R" },
		};
		
		// Parallel to exepctedOutputs
		int [][] tests = {
				//row, column, rowChange, columnChange
				{ 0, 2,  1,  0 }, // Start at top `L`, go down
				{ 2, 2,  1, -1 }, // Start at middle `W`, go down diagonally.
				{ 0, 4,  1, -1 }, // Start at top right corner, do down diagonally
				{ 4, 1,  0,  1 }, // Start at bottom-left `P`, move to the right
				{ 4, 0, -1, -1 }  // Try to go out-of-bounds
		};
		// Parallel to tests
		int[] expectedOutputs = {
				2, 3, 5, 3, 1
		};
		
		for( int i = 0; i < tests.length; i++ ) {
			System.out.println( "Start at ( " + tests[ i ][ 0 ] +", " + tests[ i ][ 1 ] +
					" ), row change: " + tests[ i ][ 2 ] +
					" col change: " + tests[ i ][ 3 ] );
			System.out.println( "\tExpected: " + expectedOutputs[ i ] +
					", Actual: " +
					c.extensionLength(
						grid,
						tests[ i ][ 0 ],
						tests[ i ][ 1 ], 
						tests[ i ][ 2 ],
						tests[ i ][ 3 ] 
					));
		}
	}
	
	public static void testOutOfBounds( ComputeTerrainInfo c ) {
		String[][] arr = {
			{ "W", "W", "W" },
			{ "W", "C", "W" },
			{ "W", "W", "W" }
		};
		
		int[][] tests = {
				{  0, 0 },
				{  0, 3 }, // out of bounds
				{  3, 3 }, // out of bounds
				{  2, 2 },
				{ -1, 0 } // out of bounds
		};
		
		// Run each test
		for( int i = 0; i < tests.length; i++ ) {
			System.out.println( "( " + tests[i][0] + ", " + tests[i][1] + " ) out of bounds: " +
				c.outOfBounds( arr, tests[ i ][ 0 ], tests[ i ][ 1 ] ) );
		}
	}
	
	public static void testSurroundCount( ComputeTerrainInfo c ) {
		// Mostly a sanity check
		String[][] simpleTest = {
				{ "W", "W", "W" },
				{ "W", "C", "W" },
				{ "W", "W", "W" }
		};
		
		// Mostly a sanity check
		String[][] normalTest = {
				{ "W", "R", "C" },
				{ "W", "C", "C" },
				{ "W", "R", "C" }
		};
		
		// Ensure that we accounted for the edge case
		String[][] edgeTest = {
				{ "C", "W", "W" },
				{ "W", "W", "W" },
				{ "W", "W", "W" }
		};
			
		// Ensure that we accounted for the edge case
		String[][] nullTest = {
				{ "W" , null, null },
				{ "W" , "C" , "W"  },
				{ null, "W" , "W"  }
		};
		
		// Make sure that each digit is counted only once
		String[][] singletonTest  = {
				{ "1", "2", "3" },
				{ "4", "C", "5" },
				{ "6", "7", "8" }
		};
		
		System.out.print( "Making sure that every surrounding feature is counted. Expected: 8, Actual: " );
		System.out.print( c.surroundCount( simpleTest, 1, 1, "W" ) );
		System.out.println();
		
		System.out.print( "Mixing up the features. Expected: 3, Actual: " );
		System.out.print( c.surroundCount( normalTest, 1, 1, "W" ) );
		System.out.println();
		
		System.out.print( "Testing edge case, Expected: 3, Actual: " );
		System.out.print( c.surroundCount( edgeTest, 0, 0, "W" ) );
		System.out.println();
		
		System.out.print( "Null testing, Expected: 5, Actual: " );
		System.out.print( c.surroundCount( nullTest, 1, 1, "W" ) );
		System.out.println();
		
		System.out.print( "Beginning singletonTest..." );
		for( int row = 0; row < singletonTest.length; row++ ) {
			for( int col = 0; col < singletonTest[ 0 ].length; col++ ) {
				
				if( row == 1 && col == 1 )
					continue; // Skip the center tile
				
				// As soon as the count is not 1, this test fails
				if( c.surroundCount( singletonTest, 1, 1, singletonTest[ row ][ col ] ) != 1 ) {
					System.out.println( "FAIL ");
					return;
				}
			}
		}
		// This test only passes if the loop is allowed to complete
		System.out.println( "PASS" );
	}
	
	
	public static void testSurroundingTiles( ComputeTerrainInfo c ) {
		
		// Mostly a sanity check
		String[][] simpleTest = {
				{ "1", "2", "3" },
				{ "4", "C", "5" },
				{ "6", "7", "8" }
		};
		
		// Make sure we properly handled null
		String[][] nullTest = {
				{ "1", "2", "3" },
				{ "4", "C", "5" },
				{ "6", "7", null }				
		};
		
		// Make sure we properly handle empty strings
		String[][] blankTest = {
				{ "1", "2", "3" },
				{ "4", "C", "5" },
				{ "6", "7", "" }				
		};
		
		// Make sure accounted for the fact that the "center" tile is on the edge 
		String[][] outOfBoundsTest = {
				{ "1", "2", "3" },
				{ "4", "5", "6" },
				{ "7", "8", "C" }				
		};
		
		// I couldn't think of another test that was actually applicable...
		String[][] emptySurroundTest = {
				{ null, null, null },
				{ null, "C" , null },
				{ null, null, null }
		};
				
		System.out.print( "In order: " );
		printArray( c.surroundingTiles( simpleTest, 1, 1 ) );
		System.out.println();
		
		System.out.print( "NULL as last element : " );
		printArray( c.surroundingTiles( nullTest, 1, 1 ) );
		System.out.println();
		
		System.out.print( "empty string as last element: " );
		printArray( c.surroundingTiles( blankTest, 1, 1 ) );
		System.out.println();

		System.out.print( "Edge check expects [5 6 8] : " );
		printArray( c.surroundingTiles( outOfBoundsTest, 2, 2 ) );
		System.out.println();
		
		System.out.print( "All surrounding elements null : " );
		printArray( c.surroundingTiles( emptySurroundTest, 1, 1 ) );
		System.out.println();
		
	}
	
	
	public static void printArray( String[] a ) {
		System.out.print( "[ " );
		for( int i = 0; i < a.length; i++ ) {
			System.out.print( a[ i ] );
			if( i < a.length - 1 )
				System.out.print(",");
			System.out.print( " " );
		}
		System.out.print( "]" );
	}

}



