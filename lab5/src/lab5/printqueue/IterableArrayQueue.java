package lab5.printqueue;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IterableArrayQueue<T> extends BoundedArrayQueue<T> implements Iterable<T> {

	public IterableArrayQueue(int size) {
		super(size);
	}

	/**
	 * returns an instance from a anonymous inner class.
	 */
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			private int index = 0;

			public T next() {
				T res;

				if (!hasNext())
					throw new NoSuchElementException("No next element available!");
				res = elements[(index + front) % elements.length];
				index++;

				return res;
			}

			public boolean hasNext() {
				return index < size();
			}

			public void remove() {
				int i = index-1;
				// move remaining numOfElements - index-1 elements to the
				// front by one position
				while (i < numOfElements - 1) {
					elements[(i + front) % elements.length] = elements[(i + front + 1) % elements.length];
					i++;
				}
				// set no longer used element to null and move rear back by one
				elements[(i + front) % elements.length] = null;
				rear = (rear - 1) % elements.length;
				numOfElements--;
				// the element which is not at index i has not been iterated over yet
				index--;

				// throw new UnsupportedOperationException("remove operation not supported!");
			}
		};
	}

}