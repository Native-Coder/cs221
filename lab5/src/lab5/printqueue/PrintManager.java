package lab5.printqueue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

public class PrintManager {
	
	// the queue for the print jobs
	static IterableArrayQueue<PrintJob> jobs = new IterableArrayQueue<PrintJob>(20);
	
	public static void main (String [] args) {
		readFromFile("printjobs.txt") ;
		printQueue();
		//printQueue( jobs );
		removeColorJobs();
		printQueue();
	}
	
	/**
	 * Read the info about the print jobs from the file and enqueue them in the
	 * order in which they appear in the file.
	 * @param fileName The name of the file with the print job info
	 * @throws FileNotFoundException 
	 */
	public static void readFromFile( String fileName ) {
		try {
			Scanner fileReader = new Scanner( new File( fileName ) );
			
			// Enqueue all the jobs
			while( fileReader.hasNext() ) {
				jobs.enqueue( new PrintJob(
					Integer.valueOf( fileReader.next().trim() ),      // Grab an int
					Boolean.parseBoolean( fileReader.next().trim() ), // Grab a bool
					fileReader.nextLine().trim()                      // Grab an everything else
				) );
			}
			fileReader.close(); // Resources leaks and all that...	
		}catch( FileNotFoundException e ) {
			System.out.println( "Failed to load the file!" );
		}
		
	}
	
	/**
	 * Remove all the jobs in the queue which need to use 
	 * the color option of the printer
	 * using an iterator
	 */
	public static void removeColorJobs() {
		Iterator<PrintJob> itr = jobs.iterator();
		// While our iterator has next...
		while( itr.hasNext() ) {
			// Evaluate job and remove it if necessary
			if( itr.next().isColor() )
				itr.remove();
		}
	}
	
	/**
	 * Print all the jobs in the queue using an iterator
	 * print each job info, using the toString method of the PrintJob class
	 * print  one job  per line 
	 */
	public static void printQueue() {
		Iterator<PrintJob> itr = jobs.iterator();
		//System.out.println( "There are " + ( queueLength ) + " many jobs in the queue" );
		// While the iterator hasNext...
		while( itr.hasNext() ) {
			// implicitly call the PrintJobs toString method
			System.out.println( itr.next() );
		}
	}
	
	/**
	 * Print all the jobs in the queue without an iterator
	 */
	public static void printQueue( QueueInterface<PrintJob> jobsQueue ) {
		int queueLength = jobsQueue.size();
		PrintJob job;
		System.out.println( "There are " + ( queueLength ) + " many jobs in the queue" );
		// print each job info, using the toString method of the PrintJob class
		// print  one job  per line
		for( int i = 0; i < queueLength; i++ ) {
			job = jobsQueue.dequeue(); // Store the front job
			System.out.println( job ); // Evaluate the stored job
			jobsQueue.enqueue( job );  // Put it at the back of the queue
		}
	}


}
