package lab5.printqueue;

public class PrintJob {
	private int pages;
	private boolean color;
	private String name;
	
	/**
	 * @param pages
	 * @param needsColor
	 */
	public PrintJob(int pages, boolean color,String name) {
		super();
		this.pages = pages;
		this.color = color;
		this.name  = name;
	}

	@Override
	public String toString() {
		return "PrintJob [pages=" + pages + ", color=" + color + ", name=" + name + "]";
	}

	/**
	 * @return the pages
	 */
	public int getPages() {
		return pages;
	}

	/**
	 * @return the needsColor
	 */
	public boolean isColor() {
		return color;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	
	
	

}
