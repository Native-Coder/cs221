/**
 * 
 */
package lab5.dllqueue;

import java.util.Iterator;

public class IterableDLLQueue<T> extends DLLQueue<T> implements Iterable<T> {

	/**
	 * 
	 */
	public IterableDLLQueue() {
		super();
	}

	// define an iterator which traverses the list forwards
	public Iterator<T> iterator() {

		return new Iterator<T>() {
			// to traverses the list
			private DLLNode nextPtr = front;

			public T next() {
				T res = nextPtr.getVal();
				nextPtr = nextPtr.getNext();
				return res;
			}

			public boolean hasNext() {
				return (nextPtr != null);
			}

			public void remove() {
				throw new UnsupportedOperationException("Remove operation not supported.");

			}
		};
	}

	// define an iterator which traverses the list backwards
	public Iterator<T> iteratorBack() {

		return new Iterator<T>() {
			
			private DLLNode nextPtr = rear;
			
			public T next () {
				T res = nextPtr.getVal();
				nextPtr = nextPtr.getPrev();
				return res;
			}
			
			public boolean hasNext() {
				return (nextPtr != null);
			}
			
			public void remove () {
				throw new UnsupportedOperationException("Remove operation not supported.");
			}
		};
	}
	
	// display the info in the queue forwards and backwards
	public void showQueue() {
		System.out.println("Forward:");
		showQueueForwards();
		System.out.println("Backward:");
		showQueueBackwards();
	}

	// display the info in the queue forwards
	private void showQueueForwards() {
		Iterator<T> itr = this.iterator();
		showQueue(itr);
	}
	
	// display the info in the queue forwards
	private void showQueueBackwards() {
		Iterator<T> itr = this.iteratorBack();
		showQueue(itr);
	}

	// display the info in the queue with the given iterator
	private void showQueue(Iterator<T> itr) {
		System.out.println("DriverPrint: Size of queue: " + size());
		int count = 0;
		while (itr.hasNext() && count < 10) {
			T elem = itr.next();
			System.out.print(elem + " ");
			count++;
		}
		System.out.println("\nNumber of elements printed: (cutoff at 10) " + count);
	}

}
