package lab5.dllqueue;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DLLQueue<T> implements QueueInterface<T> {

	// Fields
	protected DLLNode front, rear;
	protected Integer numOfElements;
		
	// constructor
	public DLLQueue() {
		// Instantitaion
		this.rear          = null;
		this.front         = null;
		this.numOfElements = 0;
	}
	
	@Override
	public void enqueue(T element) throws QueueOverflowException {
		DLLNode newNode = new DLLNode( element, null, null );
		// If the list is empty, create a single-node terminated at both ends by null pointers
		if( this.numOfElements == 0 ) {			
			// Link the node to itself
			this.front = this.rear = newNode;
			
		// Otherwise, create a normal link
		} else {
			
			// Link rear to new node
			this.rear.setNext( newNode );
			// Link new node back to rear
			newNode.setPrev( rear );
			// "Move" rear to newNode
			this.rear = newNode;
			
		}
		this.numOfElements++;
	}

	@Override
	public T dequeue() throws NoSuchElementException {
		// If the queue is empty, throw an exception
		if( this.numOfElements == 0 ) throw new NoSuchElementException();
		
		// If the queue is not empty grab the value
		T val = this.front.getVal();
		
		// If there is only one element in the list...
		if( this.numOfElements == 1 ) {
			// remove all references to our list
			this.front = null;
			this.rear  = null;
		} else {
			// "Move" the reference for the front of the queue
			this.front = this.front.getNext();
			this.front.getPrev().setNext( null );
			this.front.setPrev( null );
		}
		
		this.numOfElements--;
		return val;
	}
	
	/**
	 * @return true if the queue is empty, false otherwise.
	 */
	@Override
	public boolean isEmpty() {
		return ( rear == null && front == null );
	}

	@Override
	public boolean isFull() {
		return false;
	}

	/**
	 * @return the number of elements in the queue.
	 */
	@Override
	public int size() {
		return numOfElements;
	}
	
	// DO NOT MODIFY THE DLLNode class
	class DLLNode {
		// value stored in each element
		private T val;
		// link to the next node in the
		// linked list
		private DLLNode next;
		private DLLNode prev;

		/**
		 * @param val
		 * @param next
		 */
		public DLLNode(T val, DLLNode next, DLLNode prev) {
			super();
			this.val = val;
			this.next = next;
			this.prev = prev;
		}

		/**
		 * @param val
		 */
		public DLLNode(T val) {
			this(val, null, null);
			this.val = val;
		}

		/**
		 * @return the val
		 */
		public T getVal() {
			return val;
		}

		/**
		 * @param val the val to set
		 */
		public void setVal(T val) {
			this.val = val;
		}

		/**
		 * @return the next
		 */
		public DLLNode getNext() {
			return next;
		}

		/**
		 * @param next the next to set
		 */
		public void setNext(DLLNode next) {
			this.next = next;
		}

		/**
		 * @return the prev
		 */
		public DLLNode getPrev() {
			return prev;
		}

		/**
		 * @param prev the prev to set
		 */
		public void setPrev(DLLNode prev) {
			this.prev = prev;
		}

	}
}
