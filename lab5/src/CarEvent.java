
public class CarEvent {
	protected int arrivalTime;
	protected int serviceTime;
	protected int finishTime;

	public CarEvent(int arrivalTime, int serviceTime) {
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}

	public void setFinishTime(int time) {
		finishTime = time;
	}

	public int getFinishTime() {
		return finishTime;
	}
	
	//@Override
	public String toString() {
		return "CarEvent [arrivalTime=" + arrivalTime + ", "
				+ "serviceTime=" + serviceTime + ", "
				+ "finishTime=" + finishTime
				+ "]";
	}


	
	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int time) {
		serviceTime = time;
	}


	public int getWaitTime() {
		return (finishTime - arrivalTime - serviceTime);
	}
	
	public boolean equals (CarEvent other) {
		if (other.getArrivalTime() == this.getArrivalTime() &&
				other.getFinishTime() == this.getFinishTime() &&
				other.getWaitTime() == this.getWaitTime())
			return true;
		return false;
	}

}
