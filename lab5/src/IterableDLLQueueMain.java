
import lab5.dllqueue.IterableDLLQueue;

public class IterableDLLQueueMain {

	public static void main(String[] args) {
		
		// a demo of how the 
		IterableDLLQueue<Integer> testQueue = new IterableDLLQueue<Integer>();
		testQueue.enqueue(30);
		testQueue.showQueue();
		testQueue.enqueue(22);
		testQueue.showQueue();
		testQueue.enqueue(45);
		testQueue.showQueue();
		
		System.out.println("dequeued: " +testQueue.dequeue());
		testQueue.showQueue();

	}
}
