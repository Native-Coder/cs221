
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import lab5.dllqueue.DLLQueue;
//import lab5.dllqueue.DisplayDLLQueue;
import lab5.dllqueue.IterableDLLQueue;
import lab5.dllqueue.QueueInterface;

//import chapter3.QueueInterface;
//import lab5.dllqueue.DLLQueue;
//import lab5.dllqueue.IterableDLLQueue;




public class QueueDLLDriverLab5 {
	
	private final static String quit="quit";
	private static final String noParamConstructor = "noParamConstructor";
	private static final String size = "size";
	private static final String enqueue = "enqueue";
	private static final String dequeue = "dequeue";
	private static final String isFull = "isFull";
	private static final String isEmpty = "isEmpty";
	private static final String nonStaticTest = "nonStaticTest";
	private static final String genericTest = "genericTest";
	private static final String interfaceTest = "interfaceTest";
	
//	private static final int get = 8;
//	private static final int isFull = 9;
//	private static final int quit = 10;
	
	private static IterableDLLQueue<Integer> testQueue = null;
	private static IterableDLLQueue<Integer> [] testQueueArray = null;
	
	private static String countsStr = "";

	private static String choiceString(String choice) {
		switch (choice) {
		case noParamConstructor:
			return "\n* Calling noParamConstructor";
		case enqueue:
			return "\n* Testing enqueueing an element";
		case dequeue:
			return "\n* Testing dequeueing an element";
		case isFull:
			return "\n* Testing isFull";
		case isEmpty:
			return "\n* Testing isEmpty";
		case size:
			return "size";
		case quit:
			return "quit";
		case nonStaticTest:
			return "\n* Testing that methods/class is not static";
		case genericTest:
			return "\n* Testing that class is generic";
		case interfaceTest:
			return "\n* Testing that class is implementing interface";
		default:
			return "nothing";
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		// read data from re-directed file
		// Scanner inData = new Scanner(System.in);

//	    Scanner inData = new Scanner (new File ("dllqLab4test1.in"));
//	    Scanner inData = new Scanner (new File ("dllqLab4test2.in"));
	    Scanner inData = new Scanner (new File ("dllqLab4test3.in"));
//	    Scanner inData = new Scanner (new File ("dllqLab4test4.in"));
//	    Scanner inData = new Scanner (new File ("dllqLab4test5.in"));
//	    Scanner inData = new Scanner (new File ("dllqLab4test6.in"));
//	    Scanner inData = new Scanner (new File ("dllqLab4test7.in"));
	    String name = inData.nextLine();
	    System.out.println("Test: " + name);

		String choice;
		do {
			choice = getNextChoice(inData);
			String info = choiceString(choice);
			if (!info.equals(""))
				System.out.println(info);
			if (!choice.equals(quit))
				executeChoice(choice, inData);
		} while (!choice.equals(quit));
	}
	
	// ask the user for what s/he wants to do
	private static String getNextChoice(Scanner inData) {
		String choice = null;
		choice = inData.next();
		inData.nextLine();
		return choice;
	}


	// ask the user for what s/he wants to do
	static int promptUserForChoice(Scanner inData) {
		int choice = 0;
		// while (1 > choice || choice > 5) {
		// System.out.println("Select your choice:");
		// System.out.println(" 1 -- Load the array from a file");
		// System.out.println(" 2 -- Insert an element");
		// System.out.println(" 3 -- Delete an element");
		// System.out.println(" 4 -- Is the array full?");
		// System.out.println(" 5-- Quit");
		choice = inData.nextInt();
		inData.nextLine();
		// }
		return choice;
	}

	// Determine what the user wants to do and execute the appropriate method
	static void  executeChoice(String choice, Scanner inData) {
		switch (choice) {
		case noParamConstructor:
			testQueue = new IterableDLLQueue<Integer>();
			break;
		case enqueue:
			executeInsert(inData);
			break;
		case dequeue:
			executeDelete(inData);
			break;
		case  nonStaticTest :
			executeNonStatic(inData);
			break;
		case  isFull :
			System.out.println("The queue is full: " + testQueue.isFull());
			break;
		case  isEmpty :
			System.out.println("The queue is empty: " + testQueue.isEmpty());
			break;
		case  size :
			System.out.println("The size of the queue is  " + testQueue.size());
			break;
		case  genericTest :
			executeGeneric(inData);
			break;
		case  interfaceTest :
			executeInterface(inData);
			break;

		}
		// executePrint(testA, size);
	}

	// perform the necessary update to complete the insert
	static void executeInsert(Scanner inData) {
		// System.out.print("Enter the circle info (x, y, width, height): ");
		int temp = inData.nextInt();
		inData.nextLine();
		System.out.println(" .... enqueue new element at rear ");
		testQueue.enqueue(temp);
	}

	// perform the necessary updates to perform the delete
	static void executeDelete(Scanner inData) {
		// System.out.print("Enter subscript to be deleted: ");
		System.out.println("dequeue elem " + testQueue.dequeue());
	}


	// using extension to print the results
	static void driverPrint() {
		if (testQueue == null) 
			return;
		testQueue.showQueue();
	}
	
	// test that two different circular queues can be maintained
	static void executeNonStatic (Scanner inData) {
		
		testQueueArray = new IterableDLLQueue [2];
		testQueueArray[0] = new IterableDLLQueue<Integer>();
		testQueueArray[1] = new IterableDLLQueue<Integer>();
		
		int temp = 	inData.nextInt();
		testQueueArray[0].enqueue(temp);
		temp = 	inData.nextInt();
		testQueueArray[0].enqueue(temp);
		temp = 	inData.nextInt();
		testQueueArray[0].enqueue(temp);

		temp = 	inData.nextInt();
		testQueueArray[1].enqueue(temp);
		temp = 	inData.nextInt();
		testQueueArray[1].enqueue(temp);
		temp = 	inData.nextInt();
		testQueueArray[1].enqueue(temp);
		temp = 	inData.nextInt();
		testQueueArray[1].enqueue(temp);
		
		temp = testQueueArray[0].dequeue();
		
		System.out.println("First queue:");
		testQueueArray[0].showQueue();
		
		System.out.println("Second queue: Should be completely different");
		testQueueArray[1].showQueue();
	}
	
	// test that interface is implemented
	static void executeInterface (Scanner inData) {
		QueueInterface<CarEvent> qCar = (QueueInterface<CarEvent>) new DLLQueue<CarEvent>();
		qCar.enqueue(new CarEvent(2,3));
		qCar.enqueue(new CarEvent(4,5));
	}
	
	
	// test that interface is implemented
	static void executeGeneric (Scanner inData) {
		DLLQueue<CarEvent> qCar =new DLLQueue<CarEvent>();
		qCar.enqueue(new CarEvent(2,3));
		qCar.enqueue(new CarEvent(4,5));
	}
	


}
