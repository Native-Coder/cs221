
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import lab6.LinkedListSupport;
import lab6.RecursionPractice;
import support.Circle;

public class RecursionPracticeDriver {

	private static final String quit = "quit";
	private static final String findInArray = "findInArray";
	private static final String maxInList = "maxInList";
	private static final String interleave = "interleave";
	private static final String mergeSortLevels = "mergeSortLevels";
	private static final String mergeSortCalls = "mergeSortCalls";
	private static final String convertToBase = "convertToBase";

	private static String choiceString(String choice) {
		switch (choice) {
		case maxInList:
			return "\n Testing maxInList";
		case interleave:
			return "\n Testing interleave";
		case findInArray:
			return "\n Testing maxInList";
		case mergeSortLevels:
			return "\n Testing mergeSortLevels";
		case mergeSortCalls:
			return "\n Testing mergeSortCalls";
		case convertToBase:
			return "\n Testing convertToBase";
		case quit:
			return "quit";
		default:
			return "nothing";
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		// read data from re-directed file
		// Scanner inData = new Scanner(System.in);

//		Scanner inData = new Scanner(new File("lab6mixed.in"));
//		Scanner inData = new Scanner(new File("mergeSortCalls.in"));
//		Scanner inData = new Scanner(new File("mergeSortLevels.in"));
//		Scanner inData = new Scanner(new File("convertNumbers.in"));
//		Scanner inData = new Scanner(new File("interleaving.in"));
//		Scanner inData = new Scanner(new File("findInArrayCircles.in"));
		Scanner inData = new Scanner(new File("maxInListCircles.in"));

		String choice;
		do {
			choice = getNextChoice(inData);
			String info = choiceString(choice);
			if (!info.equals(""))
				System.out.println(info);
			if (!choice.equals(quit))
				executeChoice(choice, inData);
		} while (!choice.equals(quit));

	}

	// ask the user for what s/he wants to do
	private static String getNextChoice(Scanner inData) {
		String choice = null;
		choice = inData.next();
		inData.nextLine();
		return choice;
	}

	// Determine what the user wants to do and execute the appropriate method
	static void executeChoice(String choice, Scanner inData) {
		switch (choice) {
		case maxInList:
			executeMaxInList(inData);
			break;
		case convertToBase:
			executeConvertToBase(inData);
			break;
		case findInArray:
			executeFindInArray(inData);
			break;
		case mergeSortCalls:
			executeMergeSortCalls(inData);
			break;
		case mergeSortLevels:
			executeMergeSortLevels(inData);
			break;
		case interleave:
			executeInterleave(inData);
			break;

		}
	}

	static void executePrintNoIterator(Scanner noUse) {
//		PrintManager.printQueue(PrintManager.jobs);
	}

	static void executeMaxInList(Scanner inData) {
		LinkedListSupport lls = new LinkedListSupport();

		// create a linked list
		Circle[] circs = executeLoadArray(inData);
		lls.createLinkedListCircles(circs);

		Circle res1 = RecursionPractice.maxCircleList(lls.getFrontCl());

		System.out.println("Max circle is " + res1);

	}

	static void executeFindInArray(Scanner inData) {
		Circle[] circs = executeLoadArray(inData);
		Circle target =new Circle(inData.nextDouble());
		double precision = inData.nextDouble();
		inData.nextLine();
		System.out.println("Index where circle found: " + RecursionPractice.findInArray(target, circs, 0, precision));

	}

	static void executeConvertToBase(Scanner inData) {
		int base = inData.nextInt();
		int num = inData.nextInt();
		inData.hasNextLine();
		System.out.println("decimal number: " + num + " base: " + base + " :  "
				+ RecursionPractice.convertToNumberSystem(base, num));
	}

	static void executeInterleave(Scanner inData) {
		String word1 = inData.nextLine().trim();
		String word2 = inData.nextLine().trim();
		System.out.println(
				"Interleaving " + word1 + " and " + word2 + " : " + RecursionPractice.interleave(word1, word2));
	}

	static void executeMergeSortLevels(Scanner inData) {
		int sizeOfArray = inData.nextInt();
		inData.hasNextLine();
		System.out.println(
				" MergeSortLevels for size " + sizeOfArray + " : " + RecursionPractice.mergeSortLevels(sizeOfArray));
	}

	static void executeMergeSortCalls(Scanner inData) {
		int sizeOfArray = inData.nextInt();
		inData.hasNextLine();
		System.out.println(
				" MergeSortCalls for size " + sizeOfArray + " : " + RecursionPractice.mergeSortCalls(sizeOfArray));
	}

	// perform the necessary update to complete the insert
	private static Circle[] executeLoadArray(Scanner inData) {
		// System.out.print("Enter the circle info (x, y, width, height): ");
		int numCircles = inData.nextInt();
		Circle[] res = new Circle[numCircles];
		for (int i = 0; i < numCircles; i++) {
			res[i] = new Circle(inData.nextDouble(), inData.nextDouble(), inData.nextDouble(),
					inData.nextLine().trim());

		}
		return res;
	}

//	// perform the necessary updates to complete the load
//	static void executeLoad(Scanner inData) {
//		// System.out.print("Enter the name of the textfile: ");
//		String fileName = inData.nextLine();
//		System.out.println("File used for loading: " + fileName);
//		PrintManager.readFromFile(fileName);
//
//	}

//	// perform the necessary updates to perform the delete
//	static void executeDelete(Scanner inData) {
//		// read circle info from scanner and search for it in list
//		Circle target = makeCircleFromScanner(inData);
//		int index = list.search(target);
//		
//		boolean res = list.delete(target);
//		String notStr = (res)? "" : "not";
//
//		System.out.println("Element " + target + " was " + notStr + " deleted from index " + index + ".");
//	}
//
//	// perform the necessary updates to perform the delete
//	static void executeSearch(Scanner inData) {
//		// System.out.print("Enter info of circle to be deleted: ");
//		Circle target = makeCircleFromScanner(inData);
//		
//		int index = list.search(target);
//		System.out.println("Search for " + target + " returns " + index);
//	}
//
//	private static Circle makeCircleFromScanner(Scanner inData) {
//		double radius = inData.nextDouble();
//		double centerX = inData.nextDouble();
//		double centerY = inData.nextDouble();
//		Circle target = new Circle (radius, centerX, centerY, "");
//		inData.nextLine();
//		return target;
//	}
//	
//	
//
//	// perform the necessary updates to perform the get operation
//	static void executeGet(Scanner inData) {
//		// System.out.print("Enter subscript to be deleted: ");
//		int index = inData.nextInt();
//		inData.nextLine();
//		System.out.println("get elem at index " + index + ": " + list.get(index));
//	}
//
//	// create a new rectangle array
//	static void executeCreate(Scanner inData) {
//		// how much elements should the array hold?
//		int capacity = inData.nextInt();
//		inData.nextLine();
//		list = new ArrayOfCircles(capacity);
//
//		System.out.println("Created list with " + capacity + " slots. \n ");
//
//	}
//
//	// prints the list
//	static void executePrint() {
//		ArrayOfCirclesApp.printCirles(list);
//	}

}
