package lab6;

import lab6.LinkedListSupport.LinkedNode;
import support.Circle;

/**
 * @author Ty Meador (Native-Coder)
 *
 */
public class RecursionPractice {

	/**
	 * Finds the largest value in a linked list
	 * 
	 * @param head The head of the linked list
	 * @returns the largest element in the array
	 */
	public static Circle maxCircleList( LinkedNode<Circle> head ) {
		
		if( head == null ) return null;
		
		// Base case: there is no next element
		if( head.getNext() == null )
			return head.getElement();
		
		// General case: We have at least two elements to compare
		Circle first  = head.getElement();                                   // Get the element that we want to evaluate
		Circle second = maxCircleList( head.getNext() );                     // compare it to the largest value in the rest of the list. ("The rest of the list" is the "smaller caller")
		return ( first.getRadius() >= second.getRadius() ) ? first : second; // Return the larger of the two circles, or the first if they are equal
	}

	/**
	 * Converts a given number to a given base, as long as the base is < 10
	 * @param base
	 * @param num
	 * @return a string which represents the provided number converted to the provided base
	 */
	public static String convertToNumberSystem( int base, int num ) {
		// base case: if number is less than base, it is equal to, just return the number as a string
		if (num < base)
			return "" + num;
		// general case: convert each digit to the next 
		String restSum = convertToNumberSystem( base, num / base );
		return "" + restSum + num % base;
	}
	
	/**
	 * Recursively finds the index of a given value in a given array
	 * @param target
	 * @param circleArray
	 * @param index
	 * @param precision
	 * @return The index of the given value in the given array, -1 if the element is not found, or the array is empty
	 */
	public static int findInArray( Circle target, Circle[] circleArray, int index, double precision ) {
		// Edge cases
		if( index >= circleArray.length || circleArray.length == 0 )
			return -1;
		
		/*
		 *  Base Case: The numbers are within the margin of error
		 *  Using == to compare doubles is problematic.
		 *  So we make sure the numbers are within the given margin of error.
		 *  ( margin of error is represented by the `precision` variable) 
		 */
		if( Math.abs( target.getRadius() - circleArray[ index ].getRadius() ) < precision )
			return index;
		// General case: number hasn't been found yet
		else
			return findInArray( target, circleArray, ++index, precision );
	}

	/**
	 * Interleaves two strings using a recursive method
	 * @param one
	 * @param two
	 * @return String built by interleaving the two given strings
	 */
	public static String interleave( String one, String two ) {
		// Base Case: One of our strings is empty
		if( one.length() == 0 )
			return two;
		if( two.length() == 0)
			return one;
		
		// General Case: Take first character of each word, then append the interleaved rest of the word
		return Character.toString( one.charAt( 0 ) ) +
			   Character.toString( two.charAt( 0 ) ) + 
			   interleave( one.substring( 1 ), two.substring( 1 ) );
	}

	/**
	 * Mathematically computes the number of mergeSortLevels based on the given arraySize 
	 * @param arraySize
	 * @return The number of binary "levels" to the mergeSort tree
	 */
	public static int mergeSortLevels( int arraySize ) {
		// If we go down to 1, we accidentally count the original number as a level, which is incorrect.
		if( arraySize <= 2 )
			return 1;
		
		else {
			// If the number is odd....
			if( ( arraySize & 1 ) == 1) {
				// Traverse down the "left" side of the tree
				return 1 + mergeSortLevels( ( arraySize / 2 ) + 1 );
			// Otherwise, it doesn't matter		
			} else {
				return 1 + mergeSortLevels( ( arraySize / 2 ) );
			}
		}
			
	}

	/**
	 * Recursively computes the number of mergeSortCalls based on the given arraySize
	 * @param arraySize
	 * @return The number of calls required to a recursive mergeSort implementation
	 */
	public static int mergeSortCalls( int arraySize ) {
		if( arraySize <= 1 )
			return 0;
		else
			return 2 + mergeSortCalls( arraySize - 1 );
	}

}
