
package lab6;
import lab6.RecursionPractice;
import support.Circle;
import java.util.Random;
import lab6.LinkedListSupport.LinkedNode;
import java.util.Arrays;

/**
 * @author Ty Meador (Native-Coder)
 *
 */
public class TestRecursionApp {
	
	// Test cases for interlave function
	static String[][] interleaveTests = new String[][] {
		//input1  , input2   , expected output
		{ "tyler" ,"meador"  , "tmyelaedror" },
		{ ""      , "meador" , "meador"      },
		{ "tyler" , ""       , "tyler"       },
		{ ""      , ""       , ""            },
		{ "1357"  , "2468"   , "12345678"    }
	};
	
	// Test cases for maxCircleList function
	static Double[][] maxCircleTests = new Double[][] {
		// largest value, ...testCases
		{ 10.0          , 5.0, 3.3, 0.0, 6.0, 5.0, 10.0 },
		{ 10.0          , 10.0, 5.0, 3.3, 1.0, 6.0, 5.0 },
		{ 1.0           , 1.0 },
		{ 5.5           , 5.5, 5.5 },
		{ 0.0           , 0.0 }
	};
	
	// Test cases for convertToNumberSystem function
	static int[][] changeOfBaseTests = new int[][] {
		// Number, base, expected output
		{ 1024   , 8   , 2000   },
		{ 32     , 2   , 100000 },
		{ 18     , 3   , 200    },
		{ 73     , 9   , 81     },
		{ 0      , 2   , 0      },
		{ 1999   , 10  , 1999   }
	};
	
	// Test casses for arraySearch function
	static double[][] arraySearchTests = new double[][] {
		// target, index, precision, ...array values
		{ 15.0   , 2    , .0001    , 25.2, 99.6, 15.0, 19.6, 32.5 },
		{ 1.0    , 0    , 0.6      , .5, 1.0, 16.32, 86.5         },
		{ 0.0    , 4    , .0001    , 25.2, 99.6, 15.0, 19.6, 0.0  },
		{ 15.0   , -1   , .0001    , 25.2, 99.6, 16.0, 19.6, 32.5 },
		{ 15.0   , -1   , .0001                                   }
	};
	
	// Test cases for mergeSort functions
	static int[][] mergeSortTests = new int[][] {
		// array size, levels, calls
		{ 7          , 3     , 12    },
		{ 8          , 3     , 14    },
		{ 14         , 4     , 26    },
		{ 0          , 1     , 0     },
		{ 1024       , 10    , 2046  },
		{ 32         , 5     , 62    },
		{ 1025       , 11    , 2048  }
	};
	// Main method
	public static void main( String[] args ) {
		RecursionPractice rp = new RecursionPractice();
		
		testInterleave( rp );
		System.out.println();
		
		testMaxCircle( rp );
		System.out.println();
		
		testBaseChange( rp );
		System.out.println();
		
		testArraySearch( rp );
		System.out.println();
		
		testMergeSort( rp );
		System.out.println();
	}
	
	
	private static void testInterleave( RecursionPractice rp ) {
		// Iterate through each test
		for( int i = 0; i < interleaveTests.length; i++ ) {
			// Store a reference to the current test
			String[] test = interleaveTests[ i ];
			// Store our result so we can use it
			String result = rp.interleave( test[ 0 ], test[ 1 ] );
			
			// If the results are not identical...
			if( result.compareTo( test[ 2 ] ) != 0 ) {
				// Let the user know
				System.out.println( "FAIL: interleave( " + test[ 0 ] + ", " + test[ 1 ] + " ) output " + result );
				System.out.println( "    Expected output was " + test[ 2 ] );
			// If the results were identical, let the user know they passed this particular test
			} else {
				System.out.println( "PASS: interleave( " + test[ 0 ] + ", " + test[ 1 ] + " ) output " + result );
			}
		}
	}
	
	private static void testMaxCircle( RecursionPractice rp ) {
		
		for( int i = 0; i < maxCircleTests.length; i++ ) {
			
			// Store a reference to the current test that we are executing
			Double [] test = maxCircleTests[ i ];
			/*
			 *  All test cases store the max value as the first array element,
			 *  making it easy to autonomously check them
			 */
			
			double maxVal = test[ 0 ];
			/*
			 * This will store a reference to the FIRST circle that has a radius equal to maxVal.
			 * This ensures that the method is actually returning the FIRST circle with a radius
			 * equal to maxVal, even if multiple circles have maxVal as their radius.
			 */
			Circle expectedOutput = null;
			// This will temporarily store our nodes, so we can easily link them
			LinkedNode<Circle>[] nodeArr = new LinkedNode[ test.length ];
			
			// Create nodes
			for( int testNum = 1; testNum < test.length; testNum++ ) {
				nodeArr[ testNum - 1 ] = new LinkedNode( new Circle( test[ testNum ] ) );
				
				/*
				 * If this is the first instance of a circle that has a radius equal to maxVal,
				 * store a reference to it. We will use this to validate that the correct
				 * circle is being returned
				 */
				if( test[ testNum ] == maxVal && expectedOutput == null )
					expectedOutput = nodeArr[ testNum - 1 ].getElement();
			}
			
			// Link nodes
			for( int ii = 0; ii < nodeArr.length - 1; ii++ ) {
				nodeArr[ ii ].setNext( nodeArr[ ii + 1 ] );
			}
			
			// The expected output of the test is the first element of the test case arrays
			Circle output = rp.maxCircleList( nodeArr[ 0 ] );
			
			// Check for success and let the user know what happened...
			if( output != expectedOutput) {
				System.out.println( "FAIL: maxCircleList( " + nodeArr[ 0 ] + " ) output " + output );
				System.out.println( "    Expected output was " + expectedOutput );
			} else {
				System.out.println( "PASS: maxCircleList( " + nodeArr[ 0 ] + " ) output " + output );
			}
		}
	}
	
	private static void testBaseChange( RecursionPractice rp ) {
		// Iterate through each test case...
		for( int i = 0; i < changeOfBaseTests.length; i++ ) {
			// Store a reference to the current test...
			int[]  test   = changeOfBaseTests[ i ];
			// Store the result of the function call for later...
			String result = rp.convertToNumberSystem( test[ 1 ], test[ 0 ] );
			
			// Check for success and let the user know what happened
			if( result.compareTo( Integer.toString( test[ 2 ] ) ) != 0 ) {
				System.out.println( "FAIL: convertToBase( " + test[ 0 ] + ", " + test[ 1 ] + " ) output " + result );
				System.out.println( "    Expected output was " + test[ 2 ] );
			} else {
				System.out.println( "PASS: convertToBase( " + test[ 0 ] + ", " + test[ 1 ] + " ) output " + result );
			}
		}
	}
	
	private static void testArraySearch( RecursionPractice rp ) {
		
		for( int i = 0; i < arraySearchTests.length; i++ ) {
			
			double[] test      = arraySearchTests[ i ];
			double target      = test[ 0 ];
			int expectedOutput = (int) test[ 1 ];
			double precision   = test[ 2 ];
			Circle[] arr       = new Circle[ test.length - 3 ];
			
			for( int ii = 3; ii < test.length; ii++ ) {
				arr[ ii - 3 ] = new Circle( test[ ii ] );
			}
			
			int result = rp.findInArray( new Circle( target ), arr, 0, precision);
			
			if( result != expectedOutput ) {
				System.out.println( "FAIL: findInArray( " + target + ", " + arr + ", " + 0 + ", " + precision + ") output " + result );
				System.out.println( "    Expected output was " + expectedOutput );
			} else {
				System.out.println( "PASS: findInArray( " + target + ", " + arr + ", " + 0 + ", " + precision );
			}
		}
	}
	
	private static void testMergeSort( RecursionPractice rp ) {
		for( int i = 0; i < mergeSortTests.length; i++ ) {
			
			int[] test   = mergeSortTests[ i ]; // The test case currently being evaluated
			int input    = test[ 0 ]; // The input used to make the calls
			int levels   = test[ 1 ]; // Expected levels output
			int calls    = test[ 2 ]; // Expected calls output
			int levelOut = rp.mergeSortLevels( input ); // Actual levels output
			int callsOut = rp.mergeSortCalls( input );  // Actual calls output
			
			if( levels != levelOut ) {
				System.out.println( "FAIL: mergeSortLevels( " + input + " ) output " + rp.mergeSortLevels( input ) );
				System.out.println( "    Expected output was " + levels );
			}
			
			if( calls != callsOut ) {
				System.out.println( "FAIL: mergeSortCalls( " + input + " ) output " + rp.mergeSortCalls( input ) );
				System.out.println( "    Expected output was " + calls );
			}
			
			
			if( calls == callsOut && levels == levelOut) {
				System.out.println( "PASS: mergeSortLevels( " + input + " )" );
				System.out.println( "PASS: mergeSortCalls( " + input + " )" );	System.out.println();
			}
		}
	}
	
	/**
	 * Recursive method to generate a linked list of circles (just for fun, really)
	 * @param head
	 * @param size
	 * @param iteration
	 * @return The head of the list
	 */
	private static LinkedNode<Circle> generateCircleList( int size ) {
		Circle c = new Circle( Math.random() * 10 );
		// generate the next node...
		LinkedNode<Circle> next = new LinkedNode<Circle>( c );
		
		// General case...
		if( size > 1 ){
			size--;
			next.setNext( generateCircleList( size ) );
		}
		// Base case, just return the node
		return next;
		
	}
}
