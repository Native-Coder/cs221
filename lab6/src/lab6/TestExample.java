package lab6;

import support.Circle;

public class TestExample {

	public static void main(String[] args) {

		testMaxCircleList();

	}

	/**
	 * Testing testMaxCircleList method
	 */
	public static void testMaxCircleList() {
		// get the support to create the linked list
		LinkedListSupport lls = new LinkedListSupport();

		// create a linked list
		Circle[] cirArray2Elems = { new Circle(4.5, 0, 0, "cc1"), new Circle(2.3, 1, 2, "cc2") };
		lls.createLinkedListCircles(cirArray2Elems);

		// test the method - max is first
		Circle res1 = RecursionPractice.maxCircleList(lls.getFrontCl());

		System.out.println("Max circle for test 1: " + res1);
	}
	
	/**
	 * Testing findInArray method
	 */
	public static void testInArray() {
		// creating array of circles
		Circle[] circs = { new Circle(1, 2, 3, "not this one"), new Circle(1.399999, 3, 4, "maybe this one") };
		Circle target = new Circle(1.4, 100, 101, "definitely this");
		
		// test the method - within precision, but somewhat close...
		System.out.println(RecursionPractice.findInArray(target, circs, 0, 0.00001));

	}

	
}
