package lab6;

import support.Circle;

public class LinkedListSupport {
	 private LinkedNode<String> frontSt;
	 private LinkedNode<String> rearSt;

	 private LinkedNode<Circle> frontCl;
	 private LinkedNode<Circle> rearCl;

	/** 
	 * Constructor 
	 */
	 public LinkedListSupport() {
		// front and rear of linked list for String elements
		frontSt = null;
		rearSt = null;
		// front and rear of linked list for circle elements
		frontCl = null;
		rearCl = null;
	}
	 
	 /**
	  * Return a pointer to the linked list of Strings
	  */
	 public LinkedNode<String> getFrontSt() {
		 return frontSt;
	 }
	 /**
	  * Return a pointer to the linked list of Strings
	  */
	 public LinkedNode<Circle> getFrontCl() {
		 return frontCl;
	 }

	
	/**
	 * Create a linked list of Strings - the values in the list are 
	 *  hard coded
	 */
	public void createLinkedListStrings() {
		// create the linked list
		frontSt = new LinkedNode<String>("Base");
		rearSt = frontSt;
		rearSt = insertEnd(rearSt, "cases");
		rearSt = insertEnd(rearSt, "relate to");
		rearSt = insertEnd(rearSt, "general");
		rearSt = insertEnd(rearSt, "cases");
	}

	/**
	 * Create a linked list of Circles - the values in the list are 
	 *  hard coded
	 */
	public void createLinkedListCircles() {
		// create the linked list
		frontCl = new LinkedNode<Circle>(new Circle(4.5, 0, 0, "cir1"));
		rearCl = frontCl;
		rearCl = insertEnd(rearCl, new Circle(2.3, 2, 3, "half"));
		rearCl = insertEnd(rearCl, new Circle(5.5, 4, 5, "minimal"));
		rearCl = insertEnd(rearCl, new Circle(1.2, 14, 15, "itsy-bitsy"));
		rearCl = insertEnd(rearCl, new Circle(5.5, 20, 30, "small"));
		rearCl = insertEnd(rearCl, new Circle(5, 22, 32, "tiny"));
	}

	/**
	 * Create a linked list of Circles - the values in the list are 
	 *  taken from the given array
	 *  @param array  The circles to be added to the linked list
	 */
	public void createLinkedListCircles(Circle[] array) {
		// create the linked list
		if (array.length ==0) {
			frontCl = null;
			return;
		}
		frontCl = new LinkedNode<Circle>(array[0]);
		rearCl = frontCl;
		for (Circle cir : array)
			rearCl = insertEnd(rearCl, cir);
	}

	/**
	 * Generic method to insert into the linked list
	 * @param <T>
	 * @param rearPtr	New element is inserted after this pointer
	 * @param value		Value of the new node to be added
	 * @return			The new value of the pointer to the last elem in the linked list
	 */
	private static <T> LinkedNode<T> insertEnd(LinkedNode<T> rearPtr, T value) {
		// insert a new element at the beginning
		LinkedNode<T> newNode = new LinkedNode<T>(value);
		rearPtr.setNext(newNode);
		rearPtr = newNode;
		return rearPtr;

	}

	// this is the same Node class that was used for the LinkedQueue
	// implementation
	// except it was made generice
	static class LinkedNode<T> {
		private T element;
		private LinkedNode<T> next;

		public T getElement() {
			return element;
		}

		public void setElement(T element) {
			this.element = element;
		}

		public LinkedNode<T> getNext() {
			return next;
		}

		public void setNext(LinkedNode<T> next) {
			this.next = next;
		}

		public LinkedNode(T element) {
			super();
			this.element = element;
		}
	}


}
