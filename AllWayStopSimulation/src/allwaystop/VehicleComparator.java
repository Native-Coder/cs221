package allwaystop;
import java.util.Comparator;
import support.Vehicle;
/*
 * Used to sort the Vehicles by their arrival time
 */
public class VehicleComparator implements Comparator<Vehicle> {
	@Override
	public int compare( Vehicle o1, Vehicle o2 ) {
		if( o1.getArrivalTime() == o2.getArrivalTime() ) return 0;
		// This ensures that we are sorted in reverse order
		return ( o1.getArrivalTime() > o2.getArrivalTime() ) ? 1 : -1;
	}
}
