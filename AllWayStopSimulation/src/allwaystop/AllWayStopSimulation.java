package allwaystop;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import allwaystop.VehicleEventQueue;
import support.Vehicle;

/**
 * @author Ty Meador (Native-Coder)
 *
 */
public class AllWayStopSimulation {	
	
	public static double TURNINGTIME = 4;
	
	// the list of vehicles arriving on all roads leading to all way stop
	private ArrayList<Vehicle> arrivalList;

	// used to randomly generate the arriving vehicles (within given specifications)
	private Random rand = new Random();
	private int numRoads;
	private ArrayList<Vehicle> arrivals;
	
	/**
	 * Constructor
	 * 
	 * @param numRoads   The number of roads for this all way stop (assumed to be 3 o 4)
	 */
	public AllWayStopSimulation ( int numRoads ) {
		this( numRoads, new ArrayList<Vehicle>() );
	}

	/**
	 * Constructor
	 * 
	 * @param numRoads   The number of roads for this all way stop (assumed to be 3 o 4)
	 * @param arrivals 	 The list of vehicles arriving on all roads leading to all way stop
	 */
	public AllWayStopSimulation ( int numRoads, ArrayList<Vehicle> arrivals ) {
		this.numRoads = numRoads;
		this.arrivals = arrivals;
	}
	
	/**
	 *  Reset the simulation data to get ready to start a new simulation
	 */
	public void resetSimulation( int numRoads ) {
		this.numRoads = numRoads;
	}
	

	/**
	 * @param arrivalList The arrivalList to set
	 */
	public void setArrivalList( ArrayList<Vehicle> arrivalList ) {
		this.arrivalList = arrivalList;
	}

	/**
	 * Simulate the traffic for the given duration by 
	 * processing the vehicles which arrive to this all-way-stop
	 * 
	 * @param beginningTime Start of the simulation
	 * @param duration      Duration of the simulation
	 * @return A string containing the vehicle information (in 
	 * the order in which the vehicles depart the intersection)
	 */
	public String runSimulation( double beginningTime, double duration ) {
		// Build our event queue
		VehicleEventQueue eventQueue = new VehicleEventQueue(
			beginningTime,
			duration,
			cull( this.arrivals, beginningTime, duration ),
			this.numRoads
		);
				
		String output = "";
		
		// Build our output string
		while( !eventQueue.isEmpty() ) {
			output += eventQueue.dequeue() + "\n";
		}
		
		return output;
	}
	
	/**
	 * Removes any vehicles that do not arrive after start, and before start + duration from the provided arrivalList
	 * @param arrivalList
	 * @param start
	 * @param duration
	 * @return the culled ArrivalList<Vehicle>
	 */
	private ArrayList<Vehicle> cull( ArrayList<Vehicle> arrivalList, double start, double duration ){
		ArrayList<Vehicle> culled = new ArrayList<Vehicle>( arrivalList );
		Iterator<Vehicle> itr = arrivalList.iterator();
		Vehicle next;
		// Find all vehicles that arrive outside of our given time bounds and remove them
		while( itr.hasNext() ) {
			next = itr.next();
			if( next.getArrivalTime() < start || next.getArrivalTime() > start + duration )
				itr.remove();
		}
		return culled;
	}
	
	
	/**
	 * Generates arriving vehicles and adds them to the arrivalList of vehicles
	 * 
	 * @param startingTime         The time after which the generated vehicles start
	 *                             to arrive
	 * @param timeInterval         Generate vehicles with the given inter-arrival
	 *                             interval as long as the time is <= startingTime +
	 *                             timeInterval
	 * @param interArrivalInterval The average time between vehicles
	 */
	public int generateVehicles(double startingTime, double timeInterval, double interArrivalInterval, int roads) {
		arrivalList = new ArrayList<Vehicle>();
		double endTime = startingTime + timeInterval;
		double time = startingTime; // to hold the current time during generation
		// generate the next arrival time
		Vehicle car = genNextVehicle(time, interArrivalInterval, roads);

		// add vehicles while the endTime is not reached and update the current time
		while (car.getArrivalTime() <= endTime) {
			arrivalList.add(car);
			time = car.getArrivalTime();
			car = genNextVehicle(time, interArrivalInterval, roads);
		}
		return arrivalList.size();
	}

	
	// generate one vehicle
	private Vehicle genNextVehicle(double generationTime, double interArrival, int roads) {
		// determine arrival time
		double arrivalTime = rand.nextDouble() * interArrival + generationTime;

		// determine the road on which the vehicle approaches of the vehicle
		double roadChance = rand.nextDouble();
		int road = (int) (roadChance* roads);

		// create the new vehicle
		return new Vehicle(road, arrivalTime, TURNINGTIME);
	}


}
