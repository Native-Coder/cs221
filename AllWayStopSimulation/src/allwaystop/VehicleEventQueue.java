package allwaystop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import glassyqueue.BoundedArrayGlassyQueue;
import glassyqueue.UnboundedArrayGlassyQueue;
import allwaystop.VehicleComparator;
import support.Vehicle;

/**
 * @author Ty Meador (Native-Coder)
 *
 */
public class VehicleEventQueue extends BoundedArrayGlassyQueue <String> {
	private ArrayList <Vehicle> arrivalList;
	/*
	 * This ArrayList holds one UnboundedArrayGlassyQueue for each road. Each road
	 * needs an unbounded implementation because we have no idea how many vehicles
	 * will end up on each road.
	 */
	private ArrayList <UnboundedArrayGlassyQueue <Vehicle>> roads;
	/*
	 * We know that we can only calculate a leave time if the vehicle is AT the
	 * intersection (not in line, but actually sitting at the stop sign). So a
	 * bounded array is best to keep track of who is sitting at the intersection.
	 * Regardless of implementation, we can think of leaveOrder as a
	 * "circular queue", just like an intersection. The reason I think that a
	 * BoundedArray is best is because we won't have to go through any expand()
	 * operations, which are incredibly expensive
	 */
	private BoundedArrayGlassyQueue <Vehicle> leaveOrder;
	// Used to sort the vehicles by arrival time (We were never told if the
	// arrayList would be sorted)
	private VehicleComparator arrivalTimeComparator = new VehicleComparator();
	private double start, currentTime;

	/**
	 * Constructor
	 * 
	 * @param start the time that the simulation should start
	 * @param duration the amount of time that the simulation should run
	 * @param arrivals an arrayList containing all of the vehicles that are a part
	 *            of the simulation
	 * @param roadCount the number of roads leading up to the intersection
	 */
	public VehicleEventQueue( double start, double duration, ArrayList <Vehicle> arrivals, int roadCount ) {

		// Create a queue of the correct size
		super( arrivals.size() );

		// Instantiate our private members
		this.currentTime = start;
		this.arrivalList = arrivals;
		this.leaveOrder = new BoundedArrayGlassyQueue <Vehicle>( roadCount );
		// Gets the queue ready for a simulation
		reset( roadCount );
	}

	/**
	 * Resets the simulation, and primes the queue
	 * 
	 * @param numRoads a integer representing the number of roads to be used for
	 *            this simulation
	 */
	public void reset( int numRoads ) {
		// Create an ArrayList to hold our roads...
		this.roads = new ArrayList <UnboundedArrayGlassyQueue <Vehicle>>( numRoads );

		// Sort the arrivalList by the vehicles arrival times.(We were never told that
		// they would be in order...)
		Collections.sort( this.arrivalList, this.arrivalTimeComparator );

		// Create our "roads"
		for ( int i = 0; i < numRoads; i++ ) {
			this.roads.add( new UnboundedArrayGlassyQueue <Vehicle>() );
		}

		// Finally, prime this queue
		prime();
	}

	/**
	 * Primes this event queue with the correct order of events
	 */
	private void prime() {
		Vehicle nextVehicle;

		/*
		 * We know exactly how many events we will output, and so call the super
		 * constructor with the exact size and just fill ourself up!
		 */
		while ( !this.isFull() ) {
			// Reset the iterator
			Iterator <Vehicle> itr = arrivalList.iterator();

			// Once all vehicles have arrived, finish up the leaveOrder
			nextVehicle = itr.hasNext() ? itr.next() : leaveOrder.peek();

			// If this is the first vehicle
			if ( leaveOrder.isEmpty() ) {

				// Remove from arrival list, as it has now arrived
				itr.remove();
				// The current time is now the arrival time of this vehicle
				currentTime = nextVehicle.getArrivalTime();
				// Set the departure time of the vehicle
				nextVehicle.setDepartureTime( currentTime + nextVehicle.getTurnTime() );
				// Add the vehicle to its proper road
				roads.get( nextVehicle.getIncomingRoad() ).enqueue( nextVehicle );
				// Add to the leave queue
				leaveOrder.enqueue( nextVehicle );
				// Enqueue the event string
				// super.enqueue( eventString( nextArrival ) );

				// If the next event is an arrival
			} else if ( nextVehicle.getArrivalTime() < leaveOrder.peek().getDepartureTime()
					&& nextVehicle != leaveOrder.peek() ) {

				// Remove from arrival list, as it has now arrived
				itr.remove();
				// Put the vehicle on the proper road
				roads.get( nextVehicle.getIncomingRoad() ).enqueue( nextVehicle );
				// The current time is now the arrival time of this vehicle
				currentTime = nextVehicle.getArrivalTime();

				// If this car is at the intersection (if it's the first one in its roads'
				// queue)
				if ( roads.get( nextVehicle.getIncomingRoad() ).peek() == nextVehicle ) {

					// Set the departure time of the vehicle
					nextVehicle
							.setDepartureTime( leaveOrder.peekLast().getDepartureTime() + nextVehicle.getTurnTime() );
					// Add the vehicle to the leave order
					leaveOrder.enqueue( nextVehicle );

				}

				// Enqueue the event string
				// super.enqueue( eventString( nextArrival ) );

				// If the next event is a departure event
			} else {
				// Remove the vehicle from the leave order queue
				Vehicle leaving = leaveOrder.dequeue();
				// Update the current time
				currentTime = leaving.getDepartureTime();
				// Remove the vehicle from the road
				roads.get( leaving.getIncomingRoad() ).dequeue();

				// If there is a car behind this one, it needs to have a departure time set...
				if ( !roads.get( leaving.getIncomingRoad() ).isEmpty() ) {
					Vehicle next = roads.get( leaving.getIncomingRoad() ).peek();

					// If the leaveOrder is empty, the departure time is based on current time...
					if ( leaveOrder.isEmpty() )
						next.setDepartureTime( currentTime + next.getTurnTime() );
					// If the leaveOrder is not empty, the departure time is based on the
					// longest-waiting vehicle
					else
						next.setDepartureTime( leaveOrder.peekLast().getDepartureTime() + next.getTurnTime() );
					leaveOrder.enqueue( next );
				}

				// Enqueue the event string
				super.enqueue( eventString( leaving ) );

			}
		}
	}

	/**
	 * Generates a properly-formatted event string
	 * 
	 * @param vehicle
	 * @return a properly formatted event string
	 */
	private String eventString( Vehicle vehicle ) {
		return "ID:   "       + ( vehicle.getID() ) +
			" time: "         + String.format( "%.2f", currentTime ) +
			" road:   "       + vehicle.getIncomingRoad() +
			" \tremaining   " + roads.get( vehicle.getIncomingRoad() ).size();
	}

	/**
	 * Gets the start time
	 * 
	 * @return double representing the start time of the simulation
	 */
	public double getStart() {
		return start;
	}

	/**
	 * Sets the start time
	 * 
	 * @param start a double representing the start time of the simulation
	 */
	public void setStart( double start ) {
		this.start = start;
	}
}
