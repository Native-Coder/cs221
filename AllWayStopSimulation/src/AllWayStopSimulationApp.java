
import java.util.ArrayList;

import allwaystop.AllWayStopSimulation;
import support.Vehicle;
//import chapter3.TrafficSimulation.TrafficSimulation;

public class AllWayStopSimulationApp {

	static AllWayStopSimulation mwss;

	public static void main(String[] args) {

		// create list to be used for simulation
		int [] arrivalRoads = {0,1,2,2,0,2,1,1,0};
		double [] arrivalTimes = {1,4,7,8.5,9.5,11,14,16,20};
		
		ArrayList<Vehicle> vehicleList = createVehicleList(arrivalRoads, arrivalTimes);
		// specification for the given road approaching a junction/intersection
		int numerOfRoads= 3;
		mwss = new AllWayStopSimulation(numerOfRoads, vehicleList);

//		// generate a bunch of vehicles to approach on more way stop
//		int count = mwss.generateVehicles(0, 42, 4, 3);
//		System.out.println("Number of vehicles in simulation: " + count);

		// run the simulation for those vehicles for the given parameters
		double startTime = 0;
		double simulationLength = 120;
		int secondsToPause = 1; // for display; if 0 nothing is displayed
		//String result = mwss.runSimulation(startTime, simulationLength, secondsToPause);
		String result = mwss.runSimulation(startTime, simulationLength);
		System.out.println(result);

//		// repeat a bunch of times, to get good average of the collected data
//		int repetitions = 0;
//		for (int i = 0; i < repetitions; i++) {
//			System.out.println("NEW SIMULATION");
//			mwss.resetSimulation(numerOfRoads);
//			int count = mwss.generateVehicles(0, 42, 4, 3);
//			System.out.println("Number of vehicles in simulation: " + count);
//			mwss.runSimulation(startTime, simulationLength, secondsToPause);
//		}

	}
	
	/**
	 * Create vehicleList from given arrays for arrivalRoads and arrivalTimes
	 * 
	 * @param arrivalRoads  The roads from which vehicles approach the more way stop 
	 * @param arrivalTimes  The times when vehicles approach the more way stop
	 * @return	The generated vehicles (in the order given by the arrays)
	 */
	private static ArrayList<Vehicle> createVehicleList(int [] arrivalRoads, double [] arrivalTimes) {
		ArrayList<Vehicle> resLst = new ArrayList<Vehicle>();
		for (int i=0; i<arrivalRoads.length; i++ ) {
			Vehicle next = new Vehicle(arrivalRoads[i], arrivalTimes[i], AllWayStopSimulation.TURNINGTIME);
			resLst.add(next);
		}
		return resLst;
	}

}
