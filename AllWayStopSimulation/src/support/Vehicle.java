package support;

public class Vehicle {
	
	private int incomingRoad;
	private double arrivalTime;
	private double turnTime;
	private double departureTime;
	private int ID;
	
	private static int idGenerator=0;
	
	/**
	 * 
	 */
	public Vehicle() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param incomingRoad
	 * @param arrivalTime
	 * @param turnTime
	 */
	public Vehicle(int incomingRoad, double arrivalTime, double turnTime) {
		super();
		this.incomingRoad = incomingRoad;
		this.arrivalTime = arrivalTime;
		this.turnTime = turnTime;
		this.departureTime = -1;
		ID = idGenerator++;
	}

	/**
	 * @param incomingRoad
	 * @param startTime
	 */
	public Vehicle(int incomingRoad, double startTime) {
		super();
		this.incomingRoad = incomingRoad;
		this.arrivalTime = startTime;
		ID = idGenerator++;
	}

	/**
	 * @return the incoming road
	 */
	public int getIncomingRoad() {
		return incomingRoad;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setIncomdingRoad(int incomingRoad) {
		this.incomingRoad = incomingRoad;
	}

	/**
	 * @return the startTime
	 */
	public double getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setArrivalTime(double startTime) {
		this.arrivalTime = startTime;
	}

	/**
	 * @return the turnTime
	 */
	public double getTurnTime() {
		return turnTime;
	}

	/**
	 * @param turnTime the turnTime to set
	 */
	public void setTurnTime(double turnTime) {
		this.turnTime = turnTime;
	}

	/**
	 * @return the departureTime
	 */
	public double getDepartureTime() {
		return departureTime;
	}

	/**
	 * @param departureTime the departureTime to set
	 */
	public void setDepartureTime(double departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	@Override
	public String toString() {
		return "Vehicle [direction=" + incomingRoad + ", turnTime=" + turnTime + ", arrivalTime=" + arrivalTime + ", departureTime=" + departureTime + ", ID=" + ID + "]";
	}
	

	
	
	
	

}
