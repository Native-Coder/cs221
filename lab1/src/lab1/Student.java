package lab1;


public class Student {
	
	private String name;		// Student name
	private double prereqsMet;	// fraction of prerequisites met
	
	// Constructors
	
	// Default
	Student(){
		this.name       = "";
		this.prereqsMet = 0;
	}
	
	// Copy constructor
	Student( Student Other ){
		this.name       = Other.name;
		this.prereqsMet = Other.prereqsMet;
	}
	
	// Instantiation
	Student( String name, double prereqsMet ){
		this.name       = name;
		this.prereqsMet = prereqsMet;
	}
	
	// Typecast
	Student( String name, int prereqsMet ) {
		this.name = name;
		this.prereqsMet = ( double ) prereqsMet;
	}
	
	// Setters
	public void setName( String name ) {
		this.name = name;
	}
	
	public void setPrereqsMet( double prereqsMet ) {
		this.prereqsMet = prereqsMet;
	}
	
	// Getters
	public String getName() {
		return this.name;
	}
	
	public double getPrereqsMet() {
		return this.prereqsMet;
	}
	
	// Overrides
	@Override
	public boolean equals( Object Other ) {
		if ( this == Other )
			return true;
		if ( Other == null )
			return false;
		if ( getClass() != Other.getClass() )
			return false;
		
		// If the other object is a student, make sure the values match
		Student other = ( Student ) Other;
		return this.name == other.name && this.prereqsMet == other.prereqsMet;
	}
	
	@Override
	public String toString() {
		return "Student [name=" + this.name + ", prereqsMet=" + this.prereqsMet + "]";
	}

}
