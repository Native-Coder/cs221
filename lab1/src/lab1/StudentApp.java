package lab1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import lab1.Student;

public class StudentApp {

	public static void main(String[] args) {
		
		// 3.1 create two different students - use a constructor for each
		Student Ty    = new Student( "Ty Meador", 2 );
		Student Megan = new Student( "Megan Meador", 3 );
		
		// 3.2 call the processStudent method below with these two students
		// as parameters
		processStudents( Ty, Megan );
		
		// 5.1 repeat 3.1 with to other students (use two more variables), which have the same 
		//   name and the same fraction of prerequisites met 
		Student Brendon1 = new Student( "Brendon Parker", 3 );
		Student Brendon2 = new Student( Brendon1 );
		
		// 5.2 call the processStudent method below with the second set
		//  of students as parameters
		processStudents( Brendon1, Brendon2 );
		
		// call the readFromFile method and print the array
		Student [] threeStudents = readFromFile("/home/ty/Downloads/studentfile.txt");
		for (Student next: threeStudents) {
			System.out.println(next);
		}


	}
	
	static void processStudents (Student Student1, Student Student2) {
		
		// 7.1  print out the information in both
		// student objects using the toString method
		System.out.println( "Student 1 " + Student1.toString() );
		System.out.println( "Student 2 " + Student2.toString() );
		System.out.println();
		
		// 3.3 get the name of both student objects and print a 
		//  message whether it is the same; include the names in the message
		String modifier = ( Student1.getName() == Student2.getName() ) ? "are" : "are NOT" ;
		System.out.println( Student1.getName() + " " + Student2.getName() );
		System.out.println( "Names " + modifier + " the same");
		System.out.println();
		
		// 3.4 get the fraction of required prerequisites met of both 
		// student objects and print a message whether it is the same;
		// include the fraction in the message
		modifier = ( Student1.getPrereqsMet() == Student2.getPrereqsMet() ) ? "are" : "are NOT" ;
		System.out.println( Student1.getPrereqsMet() + " " + Student2.getPrereqsMet() );
		System.out.println( "Fractions " + modifier + " the same" );
		
		// 9.1 print a message which say
		// whether student 1 is equal to student 2 based
		//  on the result of calling the equals method
		modifier = ( Student1.equals( Student2 ) ) ? "equals" : "does NOT equal";
		System.out.println( "Equals says student 1 " + modifier + " student 2." );
		
		// 9.2 for through testing, now do the same, except determine
		// whether student 2 is equal to student 1 and print an
		// appropriate message
		modifier = ( Student2.equals( Student1 ) ) ? "equals" : "does NOT equal";
		System.out.println( "Equals says student 2 " + modifier + " student 1." );
		
	}
	
	static Student []  readFromFile ( String filename ) {
		Scanner dataIn = null;
		try {
			 dataIn = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		// 10.a.1 declare an array of students which can hold 3 students
		Student[] students = new Student[ 3 ];
		
		
	/*
	 * Why do we still have to manually load one student, 
	 * instead of just doing it all in the loop?
	 * (this comment is intentionally in a weird spot so it will stand out)
	 */
		// 10.a.2 read the information about a student from a line in the file and
		// create the student object and store it in the array of students
		double prereqsMet = dataIn.nextDouble();
		String name       = dataIn.next();
		students[ 0 ]     = new Student( name, prereqsMet );
		
		// 10.a.3 repeat the above a total three times (to read 3 students from the file)
	    //  It would be best if you use a loop....
		// (You may assume the file contains information about at least 3 students)
		for( int i = 1; i < 3 && dataIn.hasNext(); i++ ) { 
			prereqsMet = dataIn.nextDouble();
			name       = dataIn.next();
			students[ i ] = new Student( name, prereqsMet );
		}
		
		// 10.a.4 return the array of students
		return students;
	}

}

