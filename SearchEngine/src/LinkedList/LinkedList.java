
package LinkedList;

import java.util.NoSuchElementException;
import java.util.Iterator;

public class LinkedList< T > implements ListInterface< T > {

	protected LinkedNode< T > head;
	protected int numOfElements;

	/**
	 * add the LinkedNode< T > to to the head.
	 */
	@Override
	public void add( T element ) {
		if ( element == null )
			return;

		head = new LinkedNode< T >( element, head );
		numOfElements++;
	}

	public T removeFirst() {
		T value = head.getVal();
		head    = head.getNext();
		return value;
	}
	
	@Override
	public T remove( T element ) {
		LinkedNode< T > prev = head, it = head;

		// find the element's prev
		// so that we can bypass the LinkedNode< T >
		// from prev to the next LinkedNode< T >
		while ( it != null ) {
			if ( it.getVal().equals( element ) )
				break;
			prev = it;
			it = it.getNext();
		}

		if ( it != null ) {
			// this means that the head is the LinkedNode< T > that contains the element to
			// remove
			if ( prev == it )
				head = it.getNext();
			else
				prev.setNext( it.getNext() );

			numOfElements--;

			return it.getVal();
		}

		// we didn't find the element to remove
		return null;
	}

	@Override
	public boolean contains( T element ) {
		return findAux( element ) != null;
	}

	/**
	 * call the recursive findAux method.
	 */
	@Override
	public T find( T element ) {
		LinkedNode< T > res = findAux( element );

		if ( res != null )
			return res.getVal();

		return null;
	}

	protected LinkedNode< T > findAux( T element ) {
		LinkedNode< T > it = head;

		while ( it != null ) {
			if ( it.getVal().equals( element ) )
				return it;

			it = it.getNext();
		}

		return it;

	}

	@Override
	public boolean isEmpty() {
		return numOfElements == 0;
	}

	@Override
	public int size() {
		return numOfElements;
	}

	@Override
	public Iterator< T > iterator() {
		return new Iterator< T >() {

			private LinkedNode< T > currentNode = null;
			private LinkedNode< T > prevNode = null;
			private LinkedNode< T > nextNode = head;

			@Override
			public boolean hasNext() {
				return nextNode != null;
			}

			@Override
			public T next() {
				if ( hasNext() ) {
					prevNode = currentNode;
					currentNode = nextNode;
					nextNode = nextNode.getNext();
					return currentNode.getVal();
				} else
					throw new NoSuchElementException();
			}

			@Override
			public void remove() {
				if ( currentNode == null ) {
					throw new IllegalStateException( "no current value" );
				} else {
					// currentNode is the head, so set head to null
					if ( prevNode == null ) {
						head = nextNode;
					} else {
						prevNode.setNext( nextNode );
					}

					currentNode = null;
				}
			}
		};
	}

}
