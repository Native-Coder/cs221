package LinkedList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedNode< T > {
	private T val; // information stored in each node
	private LinkedNode< T > next; // the link to the next node



	/**
	 * Default constructor
	 */
	public LinkedNode() {
		this( null, null );
	}

	/**
	 * Initialization constructor, stores the object in the node 
	 * @param info
	 */
	public LinkedNode( T Obj ) {
		this( Obj, null );
	}
	
	/**
	 * Instantiation constructor, stores the Object in the node
	 * and links to the next node
	 * @param val
	 * @param next
	 */
	public LinkedNode( T Obj, LinkedNode< T > next ) {
		super();
		this.val  = Obj;
		this.next = next;
		next      = null;
	}

	/**
	 * Stores a reference to the given Object in the node
	 * @param Obj
	 */
	public void setVal( T Obj ) {
		this.val = Obj;
	}

	/**
	 * Returns the Object that this node references
	 * @return
	 */
	public T getVal() {
		return val;
	}

	/**
	 * Forward-Links this node to the given node
	 * @param link
	 */
	public void setNext( LinkedNode< T > link ) {
		this.next = link;
	}

	/**
	 * Returns a reference to the forward-linked node, null if none exists
	 * @return
	 */
	public LinkedNode< T > getNext(){
		return next;
	}
}