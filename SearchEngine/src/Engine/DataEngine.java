package Engine;

import java.io.*;
import java.util.*;

public class DataEngine {

	/**
	 * Maps each fileName to a Document object
	 */
	HashMap< String, Document > docs;
	/**
	 * HashSet.contains( value ) runs in O(1) expected time. Since every word needs
	 * to be checked against the blacklist, this is an essential design choice
	 * Current blacklist derived from
	 * https://www.ef.edu/english-resources/english-grammar/determiners/
	 */
	HashSet< String > blacklist;
	// Sorts the results! :)
	ResultComparator resultComp = new ResultComparator();

	/**
	 * Represents a single search results. 
	 * 
	 * @author Ty Meador ( Native-Coder )
	 *
	 */
	public class Result {
		private String fileName;
		private double rawScore, score, cost;

		Result( String name, double scr, double cst ) {
			fileName = name; // The name of the file
			rawScore = scr;  // Number of points this file generated
			score    = 0.0;  // Score relative to other results
			cost     = cst;  // How long this result took to generate
		}

		/**
		 * Returns the score of this result relative to others
		 * @return
		 */
		public double getScore() {
			return score;
		}
		
		/**
		 * Returns the name of the file
		 * @return
		 */
		public String getFileName() {
			return fileName;
		}

		/**
		 * Returns the amount of time this result took to generate
		 * @return
		 */
		public double getCost() {
			return this.cost;
		}
		
		@Override
		public String toString() {
			return "Result: [ fileName: " + fileName +
					", score: " + score +
					", cost : " + cost + " ]";
		}
	}

	/**
	 * Default constructor
	 */
	public DataEngine() {
		blacklist = new HashSet< String >();
		docs = new HashMap< String, Document >();
		// Attempt to load the blacklist
		try {
			Scanner scanner = new Scanner( new File( "blacklist.csv" ) );
			scanner.useDelimiter( "," );
			while ( scanner.hasNext() )
				blacklist.add( scanner.next() );

			blacklist.add( "" ); // No blanks!
			scanner.close();
		} catch ( FileNotFoundException e ) {
			System.out.println( "WARN: blacklist failed to load " );
			e.printStackTrace();
		}

	}

	public Iterator< Result > search( String query ) {
		ArrayList< Result > results = new ArrayList< Result >();
		double totalPoints = 0.0, score;

		for ( String fileName : docs.keySet() ) { // Search every document
			double start = System.nanoTime();
			score = score( fileName, query );     // Calculate documents raw score
			double end = System.nanoTime();
			totalPoints += score;                 // keep track of total points earned
			if ( score > 0.0 )                    // Add result to set
				results.add( new Result( fileName, score, ( ( end - start ) / 1000000 ) ) );
		}

		// Calculate relational scores
		for ( Result result : results )
			result.score = ( result.rawScore / totalPoints ) * 100;

		// Sort the results
		Collections.sort( results, resultComp );

		// Return a string iterator of results
		return results.iterator();
	}

	/**
	 * 
	 * @param query
	 * @param fileName
	 * @return
	 */
	public double score( String fileName, String query ) {
		if ( !docs.containsKey( fileName ) )
			return 0.0;
		
		double score = 0.0;
		Document doc = docs.get( fileName );
		HashSet< String > // adding search terms and matches to hash sets ensures that we ignore duplicates
			searchTerms = new HashSet< String >(),  // Exclusive list of words we want to find
			matches     = new HashSet< String >(); // Exclusive list of words we actually found

		/**
		 * Splits the search query using a single space as the delimeter. For each
		 * string returned, counts the number of instances in each document
		 */
		for ( String word : query.split( " " ) ) {
			word = sanitize( word );
			
			// Ignore blacklisted words and duplicate search terms
			if ( !blacklist.contains( word ) && !searchTerms.contains( word ) ) {
				searchTerms.add( word );         // Keep track of distinct search terms (ignores duplicates)
				score += doc.wordCount( word );  // Give one point for each instance of the word in the document
				if ( doc.contains( word ) )      // If the word matches, add it to the set of matches
					matches.add( word );
			}
		}

		// Give the document 2 points for every back-link it has
		score += 2.0 * doc.getBackLinkCount();

		// Score multiplier is equal to the number of words in the search that were contained in the document
		score *= matches.size();

		// Double the score if document contains all search terms
		if ( searchTerms.size() == matches.size() )
			score *= 2.0;
		
		// Avoid div/0
		return ( doc.size() > 0 ) ? score / doc.size() : 0.0;
	}

	/**
	 * Loads the document from the provided scanner
	 * 
	 * @param reader
	 * @throws IOException
	 */
	public void loadFile( String fileName ) throws IOException {
		Scanner reader = new Scanner( new FileReader( fileName ) );
		reader.useDelimiter( "[^a-zA-z]" );

		// Updates the link counts
		loadLinks( reader );

		// Create the document that we are about to load. NOTE: it's possible that the
		// document already exists, but hasn't been loaded. This happens when one file
		// links to this file before this file has been parsed. The simple fix is to
		// just check if the document exists before we create it.
		if ( !docs.containsKey( fileName ) )
			docs.put( fileName, new Document() );

		// Finally, index the file
		loadIndexes( fileName, reader );
		reader.close();
	}

	/**
	 * Auxillary function to load links from a given BufferedReader
	 * 
	 * @param reader
	 * @throws IOException
	 */
	private void loadLinks( Scanner reader ) throws IOException {
		String fileName = reader.nextLine();
		// There is a blank line that denotes the end of the "links" section
		while ( !fileName.replaceAll( "[^a-zA-z]", "" ).equals( "" ) ) {
			// If this file links to a file that we havn't yet loaded, create an empty
			// document for it
			if ( !docs.containsKey( fileName ) )
				docs.put( fileName, new Document( fileName ) );

			// Increment this documents link count
			docs.get( fileName ).incrementLinkCount();

			fileName = reader.nextLine();
		}
	}

	/**
	 * Auxillary function that indexes the body of the given document with the given
	 * reader
	 * 
	 * @param fileName
	 * @param reader
	 * @throws IOException
	 */
	private void loadIndexes( String fileName, Scanner reader ) throws IOException {
		Document doc = docs.containsKey( fileName ) ? docs.get( fileName ) : new Document( fileName );
		// Read one word at a time
		String word;
		while ( reader.hasNext() ) {
			word = sanitize( reader.next() );    // Remove unwanted characters,
			if ( !blacklist.contains( word ) ) { // Don't index blacklisted words,
				doc.addIndex( word );            // Add the index to the proper document,
			}
		}
	}

	/**
	 * Removes punctuation and returns the word as lower-case
	 * 
	 * @param word
	 * @return
	 */
	private static String sanitize( String word ) {
		// The regex removes any non-alphabet character
		return word.replaceAll( "[^a-zA-Z ]", "" ).toLowerCase();
	}

	/**
	 * Use to sort search results by their score
	 * 
	 * @author Ty Meador ( Native-Coder )
	 *
	 */
	private class ResultComparator implements Comparator< Result > {

		@Override
		public int compare( Result arg0, Result arg1 ) {
			// Checking if doubles are equal
			if ( Math.abs( arg0.score - arg1.score ) < 0.01 )
				return 0;
			else
				return ( arg0.score > arg1.score ) ? -1 : 1;
		}

	}
}
