package Engine;

import java.util.HashMap;

import LinkedList.LinkedList;

/**
 * Container for a documents meta-data such as indexes, link count, etc
 * 
 * @author Ty Meador ( Native-Coder )
 *
 */
public class Document {
	// The number of other files that link to this one
	private int backLinkCount, currentIndex;
	private HashMap< String, Integer > indexes;
	//private HashMap< String, LinkedList< Integer > > indexes;
	private String name;

	/**
	 * Instantiation constructor
	 */
	public Document() {
		this( null, 0 );
	}

	/**
	 * Instantiation constructor
	 * 
	 * @param name
	 */
	public Document( String name ) {
		this( name, 0 );
	}

	/**
	 * Initialization constructor
	 * 
	 * @param name
	 * @param linkCount
	 */
	public Document( String name, int backLinkCount ) {
		this.currentIndex  = 0;
		this.backLinkCount = backLinkCount;
		this.name          = name;
		indexes            = new HashMap< String, Integer >();
	}
	
	/**
	 * Actually storing the indexes isn't necessary.
	 * @param word
	 */
	public void addIndex( String word ) {
		currentIndex++;
		// Add the word to the index if it isn't already there
		if( !indexes.containsKey( word ) )
			indexes.put( word, 0 );
		// Increase the count
		else
			indexes.put( word, indexes.get( word ) + 1 );
	}
	
	/**
	 * Returns the number of times the given word occurred in the document
	 * @param word
	 * @return
	 */
	public int wordCount( String word ) {
		return contains( word ) ? indexes.get( word ) : 0;
	}
	
	/**
	 * Returns true if the word exists in the document
	 * @param word
	 * @return
	 */
	public boolean contains( String word ) {
		return indexes.containsKey( word );
	}
	
	/**
	 * Returns the number of files that link to this file
	 * 
	 * @return
	 */
	public int getBackLinkCount() {
		return backLinkCount;
	}

	/**
	 * Returns the number of indexes this document holds
	 * 
	 * @return
	 */
	public int size() {
		return currentIndex;
	}

	/**
	 * The number of unique words this document has indexed.
	 * 
	 * @return
	 */
	public int indexLength() {
		return indexes.size();
	}

	/**
	 * Increments the link count
	 */
	public void incrementLinkCount() {
		this.backLinkCount++;
	}
	
	
/** These methods have been fully tested. this actually keeps an index of every word in teh document. ended up not being necessary
	public void addIndex( String word ) {
		// If the word isn't already indexed
		if ( !indexes.containsKey( word ) )
			// Create a new ( and empty ) list of indexes for this word
			indexes.put( word, new LinkedList< Integer >() );

		// Add an index for this word
		indexes.get( word ).add( currentIndex );
		// Cache the size of the document
		currentIndex++;
	}
	
	/**
	 * 
	 * @param word
	 * @return
	 *
	public int countWord( String word ) {
		return indexes.containsKey( word ) ? indexes.get( word ).size() : 0;
	}

	*/
	
	
}
