import java.util.Arrays;

public class HashFunction {
	String[] theArray;
	int arraySize;
	int itemsInArray = 0;

	HashFunction( int size ) {

		arraySize = size;
		theArray = new String[ size ];
		Arrays.fill( theArray, "-1" );

	}

	public static void main( String[] args ) {
		HashFunction hf = new HashFunction( 30 );
		String[] elementsToAdd = {
				"100", "510", "170", "214", "268", "398", "235", "802", "900", "723", "699", "1", "16", "999", "890",
				"725", "998", "978", "988", "990", "989", "984", "320", "321", "400", "415", "450", "50", "660", "624"
		};

		hf.hashFunc2( elementsToAdd, hf.theArray );

		hf.findKey( "400" );
		
	}

	public void poorHash( String[] stringsForArray, String[] theArray ) {
		for ( int n = 0; n < stringsForArray.length; n++ ) {
			String newElementValue = stringsForArray[ n ];
			theArray[ Integer.parseInt( newElementValue ) ] = newElementValue;
		}
	}

	public void hashFunc2( String[] stringsForArray, String[] theArray ) {
		for ( int i = 0; i < stringsForArray.length; i++ ) {
			String newElement = stringsForArray[ i ];
			int arrayIndex = Integer.parseInt( newElement ) % 29;

			System.out.println( "Modulus Index= " + arrayIndex + " for value " + newElement );

			while ( theArray[ arrayIndex ] != "-1" ) {
				++arrayIndex;
				System.out.println( "Collistion try " + arrayIndex + " instead" );
				arrayIndex %= arraySize;

			}

			theArray[ arrayIndex ] = newElement;
		}
	}

	public String findKey( String key ) {
		int arrayIndexHash = Integer.parseInt( key ) % 29;

		while ( theArray[ arrayIndexHash ] != "-1" ) {
			if ( theArray[ arrayIndexHash ] == key ) {
				System.out.println( key + " was found in index " + arrayIndexHash );
				return theArray[ arrayIndexHash ];
			}
			++arrayIndexHash;
			arrayIndexHash %= arraySize;
		}
		return null;
	}
}
