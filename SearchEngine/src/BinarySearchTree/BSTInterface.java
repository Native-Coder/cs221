package BinarySearchTree;

/**
 * 
 * @author Guangming Xing, updated Uta Ziegler
 *
 * @param <K>  the unique key use to store and find elements in the binary search tree
 * @param <T>  the elements stored in the binary search tree
 */
public interface BSTInterface<K,T> extends Iterable<T> {
   /**
     * Inserts the  element with the specified key in the tree if the binary search 
     * tree does not already contains an entry with the specified key
     *
     * @param  key  the unique identifier for the element
     * @param  element the element
     * @throws IllegalArgumentException key or element is null.
     */
	void insert(K key, T element);
	
	/**
	 * Removes the element with the specified key from the binary search tree.
	 * 
	 * @param  the unique key of the element to be deleted
    * @throws IllegalArgumentException key is null.
	 */ 
	T remove(K key);
	
	
    /**
	 * Find the element with the specified key in the binary search tree and return it
	 * 
	 * @param  the unique key of the element to be deleted
    * @throws IllegalArgumentException key is null.
	 */ 
	T find(K key);
	
	/**
	 * Determine whether the specified key is in the binary search tree and return 
	 * tru or false
	 * 
	 * @param  the unique key of the element to be looked for
    * @throws IllegalArgumentException key is null.
	 */ 
	boolean contains (K key);
	

	/**
	 * Returns the number of elements in the binary search tree.
	 */
	int size();
}
