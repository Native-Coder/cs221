import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Iterator;

import Engine.DataEngine;
import Engine.DataEngine.Result;

public class Main {
	private static enum MODE {
		LOAD, SEARCH, EXIT, MENU
	};
	private static MODE Mode; //
	private static String input;
	private static DecimalFormat dec = new DecimalFormat( "#0.00" );
	private static BufferedReader reader = new BufferedReader( new InputStreamReader( System.in ) );;
	private static DataEngine searchEngine = new DataEngine();
	private static HashSet< String > loadedFiles = new HashSet <String >();
	
	public static void main( String[] args ) {
		HashSet< String > loadedFiles = new HashSet< String >();
		Mode = MODE.MENU;
		
		// Allows user to load document after performing a search
		while( Mode == MODE.MENU ) {
			System.out.println( "[S]earch, [L]oad file, [Q]uit: " );
			input = getInput();
			switch( input.toLowerCase().charAt( 0 ) ) {
				case 's':
					Mode = MODE.SEARCH;
					searchPhase();
				break;
					
				case'l':
					Mode = MODE.LOAD;
					loadPhase();
				break;
					
				case 'q':
					Mode = MODE.EXIT;
				break;
				
				default:
					System.out.println( "Unknown option" );
				break;
			}
		}
	}

	/**
	 * Contains the logic for the load phase of the project
	 */
	private static void loadPhase() {
		// Load phase
		while ( Mode == MODE.LOAD ) {
			System.out.println( "Enter file path, [q] to quit: " );
			input = getInput();			
			try {
				// If the user is ready to quit loading files
				if ( input.equals( "q" ) ) {
					Mode = MODE.MENU;
					continue;
				}
				
				// Otherwise, as long as we havn'et loaded the file before
				if( !loadedFiles.contains( input ) ){
					double start, end;
					System.out.print( "loading " + input + "..." );
					start = System.nanoTime();
					searchEngine.loadFile( input ); // Load file into search engine
					end = System.nanoTime();
					loadedFiles.add( input );       // Add to loaded files set
					System.out.print( "done! (" + dec.format( ( end - start ) / 1000000 ) + "ms)\n" );
				} else {
					System.out.println( input + " already loaded");
				}

			} catch ( IOException e ) {
				System.out.println( "ERROR: Someting when wrong while trying to load " + input );
				System.out.println( "\t" + e.getMessage() );
			}
		}
	}
	
	/**
	 * Contains the logic for the search phase of the engine
	 */
	private static void searchPhase() {
		// Search Phase
		while ( Mode == MODE.SEARCH ) {
			System.out.println( "Enter search query, [q] to quit" );
			input = getInput();
			// If the user is ready to quit searching
			if ( input.equals( "q" ) )
				Mode = MODE.MENU;
			else
				printResults( searchEngine.search( input ) );
		}
	}
	
	/**
	 * Iterates over the given results a formats them into a nice table
	 * 
	 * @param resultItr
	 */
	private static void printResults( Iterator< Result > resultItr ) {
		Result next;
		String format = "|%0$-20s|%0$-20s|%n";
		
		System.out.printf( "|-----------------------------------------|\n" );
		System.out.printf( "|                 Results                 |\n" );
		System.out.printf( "|-----------------------------------------|\n" );
		System.out.printf( "|      File Name     |       Score        |\n" );
		System.out.printf( "|-----------------------------------------|\n" );
		while ( resultItr.hasNext() ) {
			next = resultItr.next();			
			System.out.printf( format, next.getFileName(), dec.format( next.getScore() ) + "(" + next.getCost() + " ms)" );
		}
		System.out.printf( "|-----------------------------------------|\n" );
	}
	
	private static String getInput() {
		String input = null;
		try {
			input = reader.readLine();
		} catch( IOException e ) {
			System.out.println( "You borked it, couldn't get input" );
		}
		return input;
	}
}
