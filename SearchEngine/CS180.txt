
A study of the algorithmic approach to the analysis of problems and their computational solutions, using a high-level structured language. Labs are included in the course.